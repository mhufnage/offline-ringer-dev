import ROOT
from ROOT import TFile
from ROOT import TCanvas, TH1F,TH2F, TH1I, TFile,TLine,TGraph, TGraph2D, TGraphErrors,TMultiGraph, THStack
from ROOT import TLatex, gPad, TLegend
from ROOT import kRed, kBlue, kBlack,TLine,kBird, kOrange,kGray, kYellow, kViolet, kGreen, kAzure
from ROOT import gROOT
from array import array
import numpy as np
import os

def norm1( data ):
      norms = np.abs( data.sum(axis=1) )
      norms[norms==0] = 1
      return data/norms[:,None]

def ATLASLabel(canvas, x,y,text,color=1):
    l = TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextSize(0.06)
    l.SetTextColor(color)
    delx = 0.28*696*gPad.GetWh()/(472*gPad.GetWw())
    l.DrawLatex(x,y,"ATLAS Internal")
    p = TLatex()
    p.SetNDC()
    p.SetTextFont(4)
    p.SetTextColor(color)
    p.DrawLatex(x+delx,y,text)

    
def AddTexLabel(canvas, x,y,text,color=1, textfont=42, textsize=0.1):
    from ROOT import TLatex
    tex = TLatex()
    tex.SetNDC()
    tex.SetTextFont(textfont)
    tex.SetTextColor(color)
    tex.SetTextSize(textsize)
    tex.DrawLatex(x,y,text)
    canvas.Modified()
    canvas.Update()


# offline electron binning
etBin  = [[15 , 20] ,[20, 30]  ,[30, 40]  ,[40, 50],[50,1000000]]
etaBin = [[0.00, 0.80], [0.80, 1.37], [1.37,1.54],[1.54,2.50]]

path = '/eos/user/e/eegidiop/worktransf/offlineRinger/dataset/'
file = 'user.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.e8392_e7400_s3775_r13614.et%i_eta%i.npz' 
inputFile = path+file

os.path.join(path)
os.chdir(path)
os.mkdir('./plotsTopologies')
os.chdir('./plotsTopologies')


# for strips - basic configuration for plots

x_naxis = 170
logSc = False #log scale ?

for i,etBins in enumerate(etBin):
    
    for j,etaBins in enumerate(etaBin):
        
        data = np.load(inputFile%(i,j))
        collection = data['strips']
        etIdx  = etBin[i]
        etaIdx = etaBin[j]
    
        x_axis = np.arange(x_naxis)+1
        signal = norm1(collection)
        meanSignal = np.mean(signal,axis=0)
        sSignal = np.std(signal,axis=0)

        xS  = array( 'f', x_axis )
        exS = 0
        yS  = array( 'f', meanSignal)
        eyS = array( 'f', sSignal)
        ymax = np.max(eyS)+np.max(yS)

        c1 = TCanvas( 'c1', '', 200, 10, 1600, 900 )
        if logSc:
            c1.SetLogy()

        c1.GetFrame().SetFillColor( 21 )
        c1.GetFrame().SetBorderSize( 12 )

        gr = TGraphErrors( x_naxis, xS, yS, exS, eyS )

        gr.SetLineColor( 4 )
        gr.SetLineWidth( 2 )
        gr.SetMarkerColor( 1 )
        gr.SetMarkerStyle( 21 )
        gr.SetMarkerSize( 1 )
        gr.SetTitle()
        gr.GetXaxis().SetTitle( 'Ring' )
        gr.GetXaxis().SetTitleSize(.04)
        gr.GetXaxis().SetLimits( 0,169 )
        gr.GetXaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitle( 'Normalized Energy' )
        gr.GetYaxis().SetTitleSize(.05)
        gr.GetYaxis().SetRangeUser(0.001, ymax)
        gr.GetYaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitleOffset(1)
        gr.SetFillColor(kAzure)
        gr.Draw()

        #Layer lines
        lineEM1 = TLine(14,1e-3,14,ymax)
        lineEM1.SetLineStyle(5)
        lineEM1.Draw()
        lineEM2 = TLine(139,1e-3,139,ymax)
        lineEM2.SetLineStyle(5)
        lineEM2.Draw()
        lineEM3 = TLine(153,1e-3,153,ymax)
        lineEM3.SetLineStyle(5)
        lineEM3.Draw()
        lineH1 = TLine(160,1e-3,160,ymax)
        lineH1.SetLineStyle(5)
        lineH1.Draw()
        
        # Legends
        legend=TLegend(0.25,0.85)
        legend.AddEntry ( gr , " Z#rightarrowee (Signal) ","l" )
        legend.Draw()
        legend.SetBorderSize(0)
        legend.SetTextSize(0.06)
        legend.SetFillStyle(0)

        ATLASLabel(c1, 0.1, 0.92, '')
        AddTexLabel(c1, 0.2, 0.75, str(etIdx[0])+""+' < E_{T} #leq '+str(etIdx[1])+' GeV', textsize=0.05)
        AddTexLabel(c1, 0.2, 0.68, str(etaIdx[0])+""+' < |#eta| #leq '+str(etaIdx[1]), textsize=0.05)

        # Legend Layers
        AddTexLabel(c1, 0.11, 0.85, 'PS', textsize=0.03)
        AddTexLabel(c1, 0.17, 0.85, 'EM1', textsize=0.03)
        AddTexLabel(c1, 0.760, 0.85, 'EM2', textsize=0.03)
        AddTexLabel(c1, 0.82, 0.85, 'EM3', textsize=0.03)
        AddTexLabel(c1, 0.855, 0.85, 'Had.', textsize=0.03)
        AddTexLabel(c1, 0.855, 0.805, 'Layers', textsize=0.03)
        
        c1.Draw()

        name_plot = "stripsMeanProfile.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.e8392_e7400_s3775_r13614.et%i_eta%i.pdf"%(i,j)
        c1.SaveAs(name_plot)


# for rings - basic configuration for plots
x_naxis = 100
logSc = False #log scale ?

for i,etBins in enumerate(etBin):
    
    for j,etaBins in enumerate(etaBin):
        
        data = np.load(inputFile%(i,j))
        collection = data['rings']
        etIdx  = etBin[i]
        etaIdx = etaBin[j]
    
        x_axis = np.arange(x_naxis)+1
        signal = norm1(collection)
        meanSignal = np.mean(signal,axis=0)
        sSignal = np.std(signal,axis=0)

        xS  = array( 'f', x_axis )
        exS = 0
        yS  = array( 'f', meanSignal)
        eyS = array( 'f', sSignal)
        ymax = np.max(eyS)+np.max(yS)

        c1 = TCanvas( 'c1', '', 200, 10, 1600, 900 )
        if logSc:
            c1.SetLogy()

        c1.GetFrame().SetFillColor( 21 )
        c1.GetFrame().SetBorderSize( 12 )

        gr = TGraphErrors( x_naxis, xS, yS, exS, eyS )

        gr.SetLineColor( 4 )
        gr.SetLineWidth( 2 )
        gr.SetMarkerColor( 1 )
        gr.SetMarkerStyle( 21 )
        gr.SetMarkerSize( 1 )
        gr.SetTitle()
        gr.GetXaxis().SetTitle( 'Ring' )
        gr.GetXaxis().SetTitleSize(.04)
        gr.GetXaxis().SetLimits( 0,x_naxis )
        gr.GetXaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitle( 'Normalized Energy' )
        gr.GetYaxis().SetTitleSize(.05)
        gr.GetYaxis().SetRangeUser(0.001, ymax)
        gr.GetYaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitleOffset(1)
        gr.SetFillColor(kAzure)
        gr.Draw()

        #Layer lines
        lineEM1 = TLine(8,1e-3,8,ymax)
        lineEM1.SetLineStyle(5)
        lineEM1.Draw()
        lineEM2 = TLine(72,1e-3,72,ymax)
        lineEM2.SetLineStyle(5)
        lineEM2.Draw()
        lineEM3 = TLine(80,1e-3,80,ymax)
        lineEM3.SetLineStyle(5)
        lineEM3.Draw()
        lineH1 = TLine(88,1e-3,88,ymax)
        lineH1.SetLineStyle(5)
        lineH1.Draw()
        
        # Legends
        legend=TLegend(0.25,0.85)
        legend.AddEntry ( gr , " Z#rightarrowee (Signal) ","l" )
        legend.Draw()
        legend.SetBorderSize(0)
        legend.SetTextSize(0.06)
        legend.SetFillStyle(0)

        ATLASLabel(c1, 0.1, 0.92, '')
        AddTexLabel(c1, 0.2, 0.75, str(etIdx[0])+""+' < E_{T} #leq '+str(etIdx[1])+' GeV', textsize=0.05)
        AddTexLabel(c1, 0.2, 0.68, str(etaIdx[0])+""+' < |#eta| #leq '+str(etaIdx[1]), textsize=0.05)

        # Legend Layers
        AddTexLabel(c1, 0.11, 0.85, 'PS', textsize=0.03)
        AddTexLabel(c1, 0.17, 0.85, 'EM1', textsize=0.03)
        AddTexLabel(c1, 0.70, 0.85, 'EM2', textsize=0.03)
        AddTexLabel(c1, 0.75, 0.85, 'EM3', textsize=0.03)
        AddTexLabel(c1, 0.83, 0.85, 'Had.', textsize=0.03)
        AddTexLabel(c1, 0.83, 0.805, 'Layers', textsize=0.03)
        
        c1.Draw()

        name_plot = "ringsMeanProfile.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.e8392_e7400_s3775_r13614.et%i_eta%i.pdf"%(i,j)
        c1.SaveAs(name_plot)



# for quarters - basic configuration for plots
x_naxis = 379
logSc = False #log scale ?

for i,etBins in enumerate(etBin):
    
    for j,etaBins in enumerate(etaBin):
        
        data = np.load(inputFile%(i,j))
        collection = data['quarter']
        etIdx  = etBin[i]
        etaIdx = etaBin[j]
    
        x_axis = np.arange(x_naxis)+1
        signal = norm1(collection)
        meanSignal = np.mean(signal,axis=0)
        sSignal = np.std(signal,axis=0)

        xS  = array( 'f', x_axis )
        exS = 0
        yS  = array( 'f', meanSignal)
        eyS = array( 'f', sSignal)
        ymax = np.max(eyS)+np.max(yS)

        c1 = TCanvas( 'c1', '', 200, 10, 1600, 900 )
        if logSc:
            c1.SetLogy()

        c1.GetFrame().SetFillColor( 21 )
        c1.GetFrame().SetBorderSize( 12 )

        gr = TGraphErrors( x_naxis, xS, yS, exS, eyS )

        gr.SetLineColor( 4 )
        gr.SetLineWidth( 2 )
        gr.SetMarkerColor( 1 )
        gr.SetMarkerStyle( 21 )
        gr.SetMarkerSize( 1 )
        gr.SetTitle()
        gr.GetXaxis().SetTitle( 'Ring' )
        gr.GetXaxis().SetTitleSize(.04)
        gr.GetXaxis().SetLimits( 0,x_naxis )
        gr.GetXaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitle( 'Normalized Energy' )
        gr.GetYaxis().SetTitleSize(.05)
        gr.GetYaxis().SetRangeUser(0.001, ymax)
        gr.GetYaxis().SetLabelSize( 0.05 )
        gr.GetYaxis().SetTitleOffset(1)
        gr.SetFillColor(kAzure)
        gr.Draw()

        #Layer lines

        lineEM1 = TLine(29,1e-3,29,ymax)
        lineEM1.SetLineStyle(5)
        lineEM1.Draw()

        lineEM2 = TLine(282,1e-3,282,ymax)
        lineEM2.SetLineStyle(5)
        lineEM2.Draw()


        lineEM3 = TLine(311,1e-3,311,ymax)
        lineEM3.SetLineStyle(5)
        lineEM3.Draw()

        lineH1 = TLine(340,1e-3,340,ymax)
        lineH1.SetLineStyle(5)
        lineH1.Draw()
        
        # Legends
        legend=TLegend(0.25,0.85)
        legend.AddEntry ( gr , " Z#rightarrowee (Signal) ","l" )
        legend.Draw()
        legend.SetBorderSize(0)
        legend.SetTextSize(0.06)
        legend.SetFillStyle(0)

        ATLASLabel(c1, 0.1, 0.92, '')
        AddTexLabel(c1, 0.2, 0.75, str(etIdx[0])+""+' < E_{T} #leq '+str(etIdx[1])+' GeV', textsize=0.05)
        AddTexLabel(c1, 0.2, 0.68, str(etaIdx[0])+""+' < |#eta| #leq '+str(etaIdx[1]), textsize=0.05)

        # Legend Layers
        AddTexLabel(c1, 0.11, 0.85, 'PS', textsize=0.03)
        AddTexLabel(c1, 0.17, 0.85, 'EM1', textsize=0.03)
        AddTexLabel(c1, 0.70, 0.85, 'EM2', textsize=0.03)
        AddTexLabel(c1, 0.75, 0.85, 'EM3', textsize=0.03)
        AddTexLabel(c1, 0.83, 0.85, 'Had.', textsize=0.03)
        AddTexLabel(c1, 0.83, 0.805, 'Layers', textsize=0.03)
        
        c1.Draw()

        name_plot = "quarterMeanProfile.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.e8392_e7400_s3775_r13614.et%i_eta%i.pdf"%(i,j)
        c1.SaveAs(name_plot)


print("PLOTS ---- SUCESS")