# Ringer Dumper package:

This is a simple package to extract the FastCaloRinger (symmetric and asymetric) from a AOD file.
This package is developed in the Athena,latest,master

# Compiling the package
To run this package, in a lxplus enviroment, start in a clean path (no others package in the folder), downloading the package and setup the ATLAS enviroment:

```
git clone https://gitlab.cern.ch/jlieberm/ringerdumper.git
setupATLAS
asetup Athena,latest,master
```


Then, create a build folder and enter on it:

```
mkdir build
cd build
```


And then, compile and publish de package:

```
cmake ../ringerdumper/
make
source x86*/setup.sh
```
# Run the code
There is two packages available: RingerDumper and ForwardRingerDumper. They extract barrel and forward information, respectivelly.

If everything goes well, you can run the script on the share folder:

`
athena.py RingerDumperOptions.py
`

NOTE: Inside this script, you can change the configuration by setting True/False the keys in the beginning of the file. Also, you can change the input and output file name/path.

