// PhotonTruthMatchingTool includes
#include "RingerDumper/PhotonRingerDumperAlg.h"




PhotonRingerDumperAlg::PhotonRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAlgorithm( name, pSvcLocator ), m_ntsvc("THistSvc/THistSvc", name){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


PhotonRingerDumperAlg::~PhotonRingerDumperAlg() {
 
}


StatusCode PhotonRingerDumperAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  m_Tree = new TTree("events", "events");
    bookBranches(m_Tree);
    if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [events]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

void PhotonRingerDumperAlg::bookBranches(TTree *tree){
  tree->Branch("avgmu", &m_pileup);
  if (m_dumpCells){
    tree->Branch("cells_eta", &m_cells_eta);
    tree->Branch("cells_phi", &m_cells_phi);
    tree->Branch("cells_et", &m_cells_et);
    tree->Branch("cells_sampling", &m_cells_sampling);
    tree->Branch("cells_size", &m_cells_size);
    tree->Branch("std_rings_sum", &m_rings_sum);
  }
  tree->Branch("online_rings", &m_rings);
  tree->Branch("cluster_et", &m_clusterEnergy);
  tree->Branch("cluster_eta", &m_clusterEta);
  tree->Branch("cluster_phi", &m_clusterPhi);
  tree->Branch("cluster_e237", &m_clustere237);
  tree->Branch("cluster_e277", &m_clustere277);
  tree->Branch("cluster_fracs1", &m_clusterfracs1);
  tree->Branch("cluster_weta2", & m_clusterweta2);
  tree->Branch("cluster_wstot", & m_clusterwstot);
  tree->Branch("cluster_ehad1", & m_clusterehad1);
  if (m_dumpOfflineRings) tree->Branch("offline_rings", &m_offlineRings);
  tree->Branch("ph_et", &ph_energy);
  tree->Branch("ph_eta", &ph_eta);
  tree->Branch("ph_phi", &ph_phi);
  tree->Branch("ph_f1",&ph_f1);
  tree->Branch("ph_f3",&ph_f3);
  tree->Branch("ph_eratio",&ph_eratio);
  tree->Branch("ph_weta1",&ph_weta1);
  tree->Branch("ph_weta2",&ph_weta2);
  tree->Branch("ph_fracs1",&ph_fracs1);
  tree->Branch("ph_wtots1",&ph_wtots1);
  tree->Branch("ph_deltae",&ph_deltae);
  tree->Branch("ph_e2777",&ph_e277);
  tree->Branch("ph_reta",&ph_reta);
  tree->Branch("ph_rphim",&ph_rphi);
  tree->Branch("ph_rhad",&ph_rhad);
  tree->Branch("ph_rhad1",&ph_rhad1);
  if (m_doMC){
    tree->Branch("mc_et", &mc_et);
    tree->Branch("mc_eta", &mc_eta);
    tree->Branch("mc_phi", &mc_phi);
    tree->Branch("mc_origin", &mc_origin);
    tree->Branch("mc_type", &mc_type);
  }
}

StatusCode PhotonRingerDumperAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  setFilterPassed(false); //optional: start with algorithm not passed
  if (!collectInfo()) ATH_MSG_DEBUG("Unable to collect info. Please check input file");
  m_Tree->Fill();
  return StatusCode::SUCCESS;
}


StatusCode PhotonRingerDumperAlg::selectElectrons(){
  return StatusCode::SUCCESS;
}

StatusCode PhotonRingerDumperAlg::selectMuons(){
  return StatusCode::SUCCESS;
}

StatusCode PhotonRingerDumperAlg::selectPhotons(){
  return StatusCode::SUCCESS;
}


StatusCode PhotonRingerDumperAlg::collectInfo(){
  clear();
  if(!collectEventInfo())   ATH_MSG_DEBUG("Event information cannot be collected!");
  if(!collectRingsInfo())   ATH_MSG_DEBUG("Ringer information cannot be collected!");
  if(!collectOfflineInfo()) ATH_MSG_DEBUG("Offline information cannot be collected!");
  return StatusCode::SUCCESS;
}

void PhotonRingerDumperAlg::clear(){
  m_pileup = -999;
  if(m_dumpCells){
    m_cells_eta->clear();
    m_cells_phi->clear();
    m_cells_et->clear();
    m_cells_sampling->clear();
    m_cells_size->clear();
    m_rings_sum->clear();
  }
  m_rings->clear();
  m_clusterEnergy->clear();
  m_clusterEta->clear();
  m_clusterPhi->clear();
  m_clustere237->clear();
  m_clustere277->clear();
  m_clusterfracs1->clear();
  m_clusterweta2->clear();
  m_clusterwstot->clear();
  m_clusterehad1->clear();
  if (m_dumpOfflineRings) m_offlineRings->clear();
  ph_energy->clear();
  ph_eta->clear();
  ph_phi->clear();
  ph_f1->clear();
  ph_f3->clear();
  ph_eratio->clear();
  ph_weta1->clear();
  ph_weta2->clear();
  ph_fracs1->clear();
  ph_wtots1->clear();
  ph_deltae->clear();
  ph_e277->clear();
  ph_reta->clear();
  ph_rphi->clear();
  ph_rhad->clear();
  ph_rhad1->clear();
  if(m_doMC){
    mc_et->clear();
    mc_eta->clear();
    mc_phi->clear();
    mc_origin->clear();
    mc_type->clear();
  }  
}

StatusCode PhotonRingerDumperAlg::collectRingsInfo(){
  const xAOD::TrigRingerRingsContainer *ringerContainer = nullptr;
  const xAOD::RingSetContainer *ph_ring_container = nullptr;
  ATH_CHECK( evtStore()->retrieve(ringerContainer,"HLT_FastCaloRinger"));
  if (m_dumpOfflineRings) ATH_CHECK(evtStore()->retrieve(ph_ring_container, "PhotonRingSets"));
  ATH_MSG_DEBUG("Rings Container Retrieved! That's good!");
  
  for (const xAOD::TrigRingerRings_v2 *ringer : *ringerContainer){
    ATH_MSG_DEBUG("Looping over Ringer Container");
    if(m_dumpCells){
      m_cells_eta->push_back(ringer->auxdata<std::vector<float>>("cells_eta"));
      m_cells_phi->push_back(ringer->auxdata<std::vector<float>>("cells_phi"));
      m_cells_et->push_back(ringer->auxdata<std::vector<float>>("cells_et"));
      m_cells_sampling->push_back(ringer->auxdata<std::vector<int>>("cells_sampling"));
      m_cells_size->push_back(ringer->auxdata<std::vector<int>>("cells_size"));
      m_rings_sum->push_back(ringer->auxdata<std::vector<double>>("rings_sum"));
    }
    m_rings->push_back(ringer->rings());
    m_clusterEnergy->push_back(ringer->emCluster()->et());
    m_clusterEta->push_back(ringer->emCluster()->eta());
    m_clusterPhi->push_back(ringer->emCluster()->phi());

    m_clustere237->push_back(ringer->emCluster()->e237());
    m_clustere277->push_back(ringer->emCluster()->e277());
    m_clusterfracs1->push_back(ringer->emCluster()->fracs1());
    m_clusterweta2->push_back(ringer->emCluster()->weta2());
    m_clusterwstot->push_back(ringer->emCluster()->wstot());
    m_clusterehad1->push_back(ringer->emCluster()->ehad1());
    }
  ATH_MSG_DEBUG("Online Standard Ringer Information Collected! That's good!");
  if (m_dumpOfflineRings){
    std::vector<float> single_ring;
    for (const xAOD::RingSet_v1 *ph_ring : *ph_ring_container) {
      for (auto iter=ph_ring->begin(); iter != ph_ring->end(); iter++){
        single_ring.push_back(*iter);
      }
      if (single_ring.size() == 100){
        m_offlineRings->push_back(single_ring);
        single_ring.clear();
      }
    }
    ATH_MSG_DEBUG("Offline Standard Ringer Information Collected! That's good!");
  }
  
  return StatusCode::SUCCESS;
}

StatusCode PhotonRingerDumperAlg::collectEventInfo(){
  const xAOD::EventInfo* ei = nullptr;
  ATH_CHECK( evtStore()->retrieve(ei,"EventInfo"));
  ATH_MSG_DEBUG("EventInfo Container Retrieved! That's good!");
  m_pileup =  ei->actualInteractionsPerCrossing();
  return StatusCode::SUCCESS;
}

StatusCode PhotonRingerDumperAlg::collectOfflineInfo(){
  const xAOD::PhotonContainer * offlinePhotons = nullptr;
  ATH_CHECK(evtStore()->retrieve(offlinePhotons, "Photons"));
  ATH_MSG_DEBUG("Offline Photon Container Retrieved! That's good!");
  for (auto *photon : *offlinePhotons){
    ph_energy->push_back ( photon->e() );
    ph_eta->push_back ( photon->eta() );
    ph_phi->push_back ( photon->phi() );
    if (!collectOfflineSS(photon)) ATH_MSG_DEBUG("Impossible to collect offline SS variables");
    if (m_doMC){
      const xAOD::TruthParticle* mc = xAOD::TruthHelpers::getTruthParticle(*photon);
      if(mc){
        mc_et->push_back(mc->pt());
        mc_eta->push_back(mc->eta());
        mc_phi->push_back(mc->phi());
        mc_origin->push_back(photon->auxdata<int>("truthOrigin"));
        mc_type->push_back(photon->auxdata<int>("truthType"));
      }
    }
  }
  ATH_MSG_DEBUG("Offline Electron Container Information Collected! That's good!");
  return StatusCode::SUCCESS;
}

StatusCode  PhotonRingerDumperAlg::collectOfflineSS(const xAOD::Photon *photon){
  ph_f1->push_back(photon->showerShapeValue(xAOD::EgammaParameters::f1));
  ph_f3->push_back(photon->showerShapeValue(xAOD::EgammaParameters::f3));
  ph_eratio->push_back(photon->showerShapeValue(xAOD::EgammaParameters::Eratio));
  ph_weta1->push_back(photon->showerShapeValue(xAOD::EgammaParameters::weta1));
  ph_weta2->push_back(photon->showerShapeValue(xAOD::EgammaParameters::weta2));
  ph_fracs1->push_back(photon->showerShapeValue(xAOD::EgammaParameters::fracs1));
  ph_wtots1->push_back(photon->showerShapeValue(xAOD::EgammaParameters::wtots1));
  ph_e277->push_back(photon->showerShapeValue(xAOD::EgammaParameters::e277));
  ph_reta->push_back(photon->showerShapeValue(xAOD::EgammaParameters::Reta));
  ph_rphi->push_back(photon->showerShapeValue(xAOD::EgammaParameters::Rphi));
  ph_deltae->push_back(photon->showerShapeValue(xAOD::EgammaParameters::DeltaE));
  ph_rhad->push_back(photon->showerShapeValue(xAOD::EgammaParameters::Rhad));
  ph_rhad1->push_back(photon->showerShapeValue(xAOD::EgammaParameters::Rhad1));
   
  return StatusCode::SUCCESS;
}

StatusCode PhotonRingerDumperAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}