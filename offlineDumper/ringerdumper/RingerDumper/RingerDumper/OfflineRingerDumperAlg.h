#ifndef OFFLINERINGERDUMPER_RINGERDUMPERALG_H
#define OFFLINERINGERDUMPER_RINGERDUMPERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
#include "xAODTrigRinger/TrigRingerRingsContainer.h"
#include "AthenaBaseComps/AthAlgorithm.h"
#include <TTree.h>
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include <xAODCaloRings/versions/RingSet_v1.h>
#include <xAODCaloRings/RingSetContainer.h>
#include <xAODEgamma/versions/Electron_v1.h>
#include <xAODEgamma/ElectronContainer.h>
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include <xAODCaloRings/versions/CaloRings_v1.h>
#include <xAODCaloRings/CaloRingsContainer.h>
#include "xAODCaloRings/tools/getCaloRingsDecorator.h" 
#include <iostream>
#include <vector>



using namespace std;

class OfflineRingerDumperAlg: public ::AthAlgorithm { 

 public: 
    OfflineRingerDumperAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~OfflineRingerDumperAlg(); 

    virtual StatusCode  initialize();     //once, before any input is loaded
    virtual StatusCode  execute();        //per event
    virtual StatusCode  finalize();       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    virtual StatusCode  collectInfo();
    virtual StatusCode  collectRingsInfo();
    virtual StatusCode  collectEventInfo();
    virtual StatusCode  collectOfflineInfo();
    virtual StatusCode  collectElectronInfo();
    virtual StatusCode  collectOfflineSS(const xAOD::Electron *electron);
    bool getCaloRings( const xAOD::Electron_v1 *, std::vector<float> & );


  private: 
    TTree *m_Tree;

    ServiceHandle<ITHistSvc> m_ntsvc;
    Gaudi::Property<bool>                          m_doMC       {this, "doMC"     , false ,  "Dump MC information"};
    Gaudi::Property<bool>                          m_dumpCells  {this, "dumpCells", false ,  "Dump Cell information"};
    Gaudi::Property<bool>                          m_doQuarters {this, "doQuarters", false ,  "Do quarter rings"};

    float                                 m_pileup          = -999;

    std::vector < float > *m_offlineDummy                       = nullptr;
    std::vector < std::vector < float > > *m_offlineStdRings    = nullptr;
    std::vector < std::vector < float > > *m_offlineAsymRings   = nullptr;
    std::vector < std::vector < float > > *m_offlineStripsRings = nullptr;

    // std::vector < float > *m_offlineStdRings      = nullptr;
    // std::vector < float > *m_offlineAsymRings     = nullptr;
    // std::vector < float > *m_offlineStripsRings   = nullptr;
    
    std::vector < float > *el_energy = nullptr;
    std::vector < float > *el_et = nullptr;
    std::vector < float > *el_eta = nullptr;
    std::vector < float > *el_phi = nullptr;
    std::vector < float > *el_f1  = nullptr;
    std::vector < float > *el_f3  = nullptr;
    std::vector < float > *el_eratio = nullptr;
    std::vector < float > *el_weta1 = nullptr;  
    std::vector < float > *el_weta2 = nullptr;  
    std::vector < float > *el_fracs1 = nullptr; 
    std::vector < float > *el_wtots1 = nullptr; 
    std::vector < float > *el_e277 = nullptr; 
    std::vector < float > *el_reta = nullptr; 
    std::vector < float > *el_rphi = nullptr; 
    std::vector < float > *el_deltae = nullptr; 
    std::vector < float > *el_rhad = nullptr;  
    std::vector < float > *el_rhad1 = nullptr; 

    //truth

    std::vector < float > *el_truth_pt = nullptr;
    std::vector < float > *el_truth_eta = nullptr;
    std::vector < float > *el_truth_phi = nullptr;
    std::vector < float > *el_truth_f1  = nullptr;
    std::vector < float > *el_truth_f3  = nullptr;
    std::vector < float > *el_truth_eratio = nullptr;
    std::vector < float > *el_truth_weta1 = nullptr;  
    std::vector < float > *el_truth_weta2 = nullptr;  
    std::vector < float > *el_truth_fracs1 = nullptr; 
    std::vector < float > *el_truth_wtots1 = nullptr; 
    std::vector < float > *el_truth_e277 = nullptr; 
    std::vector < float > *el_truth_reta = nullptr; 
    std::vector < float > *el_truth_rphi = nullptr; 
    std::vector < float > *el_truth_deltae = nullptr; 
    std::vector < float > *el_truth_rhad = nullptr;  
    std::vector < float > *el_truth_rhad1 = nullptr; 
    
    std::vector < float > *mc_eta = nullptr;
    std::vector < float > *mc_phi = nullptr;
    std::vector < float > *mc_et = nullptr;
    std::vector < int > *mc_origin = nullptr;
    std::vector < int > *mc_type = nullptr;


    std::vector < float > *mc_match_eta = nullptr;
    std::vector < float > *mc_match_phi = nullptr;
    std::vector < float > *mc_match_et = nullptr;
    std::vector < int > *mc_match_origin = nullptr;
    std::vector < int > *mc_match_type = nullptr;

    float dR(const float eta1, const float phi1, const float eta2, const float phi2) const
    {
      float deta = std::abs(eta1 - eta2);
      float dphi = std::abs(phi1 - phi2) < TMath::Pi() ? std::abs(phi1 - phi2) : 2*TMath::Pi() - std::abs(phi1 - phi2);
      return sqrt(deta*deta + dphi*dphi);
    };

}; 


#endif 