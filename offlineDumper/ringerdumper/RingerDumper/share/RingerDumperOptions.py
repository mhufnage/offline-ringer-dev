
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
doFwd = False
doMC = True
dumpCells = False
doPhoton = False
doOfflineRinger = True
from AthenaCommon.AppMgr import ToolSvc

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()
from RingerDumper.RingerDumperConf import ForwardRingerDumperAlg, RingerDumperAlg, PhotonRingerDumperAlg, OfflineRingerDumperAlg
if doPhoton:
    job+= PhotonRingerDumperAlg("RingerDumper")
    job.RingerDumper.dumpOfflineRings = False

elif doOfflineRinger:
    job += OfflineRingerDumperAlg( "RingerDumper" )  

elif doFwd:
    job += ForwardRingerDumperAlg( "RingerDumper" )   
    from ElectronPhotonSelectorTools.ElectronPhotonSelectorToolsConf import AsgForwardElectronIsEMSelector
    LooseElectronSelector             = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTLooseElectronSelector")
    MediumElectronSelector            = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTMediumElectronSelector")
    TightElectronSelector             = CfgMgr.AsgForwardElectronIsEMSelector("T0HLTTightElectronSelector")
    
    LooseElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMLooseSelectorCutDefs.conf"
    MediumElectronSelector.ConfigFile = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMMediumSelectorCutDefs.conf"
    TightElectronSelector.ConfigFile  = "ElectronPhotonSelectorTools/offline/mc15_20170711/ForwardElectronIsEMTightSelectorCutDefs.conf"

    ToolSvc+=LooseElectronSelector
    ToolSvc+=MediumElectronSelector
    ToolSvc+=TightElectronSelector

    job.RingerDumper.ForwardElectronIsEMSelectors = [TightElectronSelector, MediumElectronSelector, LooseElectronSelector]
else:
    job += RingerDumperAlg( "RingerDumper" )   


job.RingerDumper.doMC = doMC
job.RingerDumper.dumpCells = dumpCells
job.RingerDumper.OutputLevel = INFO

from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

path = '/afs/cern.ch/work/e/eegidiop/offlineRinger/dataset/AOD/user.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.AOD.e8392_e7400_s3775_r13614.v11_EXT0/'
path_files = [path+'user.eegidiop.29754225.EXT0._000001.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000002.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000003.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000004.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000005.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000006.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000007.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000008.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000009.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000010.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000011.AOD.pool.root',
              path+'user.eegidiop.29754225.EXT0._000012.AOD.pool.root']

ServiceMgr.EventSelector.InputCollections = path_files
ServiceMgr += CfgMgr.THistSvc()
hsvc = ServiceMgr.THistSvc
hsvc.Output += [ "rec DATAFILE='user.eegidiop.mc21.601189.PhPy8EG_AZNLO_Zee.recon.HIST.e8392_e7400_s3775_r13614.pool.root' OPT='RECREATE'" ]
theApp.EvtMax = -1

