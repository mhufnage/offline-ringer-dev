// PhotonTruthMatchingTool includes
#include "EventReader/EventReaderAlg.h"
#include <xAODEventInfo/EventInfo.h>

// CaloCell
#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloClusterCellLinkContainer.h"
// #include "LArRawChannelContainer/"
#include "CaloEvent/CaloCompactCell.h"
#include "CaloEvent/CaloCompactCellContainer.h"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

#include "CaloUtils/CaloCellList.h"
#include "CaloGeoHelpers/CaloSampling.h"

using CLHEP::GeV;
using CLHEP::pi;
using CLHEP::twopi;

// Readout lib's
// #include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"

// #include <xAODJet/JetContainer.h>

EventReaderAlg::EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ) : 
    AthAlgorithm(name, pSvcLocator)
    , m_ntsvc("THistSvc/THistSvc", name)
    , m_noiseCDOKey("totalNoise")
    , m_pedestalKey("LArPedestal")
    , m_adc2MeVKey("LArADC2MeV")
    , m_ofcKey("LArOFC")
    , m_shapeKey("LArShape")
    , m_onlineLArID(0)
    // , m_tileCabling("TileCablingSvc",name) //initialize Tile cabling service
    , m_larCablingKey("LArOnOffIdMap")
    // , larCabling(nullptr)    
    , m_tileHWID(nullptr)
    , m_tileID(nullptr)
    , m_tileCabling(nullptr)
    , m_calocell_id(nullptr)
    // , m_ntsvc("THistSvc/THistSvc", name)
    {

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  // declareProperty("LArOnOffMap",m_larCablingKey," LAr cabling map for online and offline.");

}

EventReaderAlg::~EventReaderAlg() {
  // delete m_runNumber;
  // delete m_bcid;
  // delete m_eventNumber;
}

StatusCode EventReaderAlg::initializeCalibConstantsTools(){

  ATH_CHECK(m_noiseCDOKey.initialize());  //---- retrieve the noise CDO  https://acode-browser.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloRec/src/CaloTopoClusterMaker.cxx#0378

  ATH_CHECK(m_run2DSPThresholdsKey.initialize(SG::AllowEmpty) );  // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArROD/src/LArRawChannelBuilderAlg.cxx
  ATH_MSG_INFO("(DSP) Energy cut for time and quality computation: taken from COOL folder " << m_run2DSPThresholdsKey.key() << " (run2) ");
  if (m_run2DSPThresholdsKey.empty()){
    ATH_MSG_ERROR ("useDB requested but Run2DSPThresholdsKey was not initialized.");
    return StatusCode::FAILURE;
  }

  ATH_CHECK(m_pedestalKey.initialize());     
  ATH_CHECK(m_adc2MeVKey.initialize());  
  ATH_CHECK(m_ofcKey.initialize());  
  ATH_CHECK(m_shapeKey.initialize());

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_CHECK( m_tileCablingSvc.retrieve() );
  m_tileCabling = m_tileCablingSvc->cablingService(); // initialize the cabling service to a pointer.
  ATH_MSG_INFO ("(EventReader) Initializing detector store tools...");
  ATH_CHECK( detStore()->retrieve(m_onlineLArID, "LArOnlineID") ); // get online ID info from LAr (online)  
  ATH_CHECK( detStore()->retrieve(m_tileHWID) ); // get hardware ID info from TileCal (online)
  ATH_CHECK( detStore()->retrieve(m_tileID) ); // get cell ID info from TileCal (offline)
  ATH_CHECK( detStore()->retrieve (m_calocell_id, "CaloCell_ID") );

  ATH_CHECK( m_larCablingKey.initialize());
  // ATH_CHECK( m_cablingKey.initialize());
  
  if(!initializeCalibConstantsTools()) ATH_MSG_INFO("Calibration constants cannot be initialized!");
  else ATH_MSG_INFO("initializeCalibConstantsTools executed!");
  
  // Book the variables to save in the *.root 
  m_Tree = new TTree("dumpedData", "dumpedData");
  ATH_MSG_DEBUG("Booking branches...");
  bookBranches(m_Tree); // book TTree with branches
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [dumpedData]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}


// StatusCode EventReaderAlg::execute(const EventContext& ctx) const
StatusCode EventReaderAlg::execute() 
{  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  ATH_MSG_DEBUG("Cleanning of event variables...");
  clear();

  SG::ReadCondHandle<LArOnOffIdMapping>   larCablingHdl   (m_larCablingKey);
  SG::ReadCondHandle<CaloNoise>           noiseHdl        (m_noiseCDOKey);
  SG::ReadCondHandle<AthenaAttributeList> dspThrshAttrHdl (m_run2DSPThresholdsKey);
  SG::ReadCondHandle<ILArPedestal>        pedHdl          (m_pedestalKey);
  SG::ReadCondHandle<LArADC2MeV>          adc2mevHdl      (m_adc2MeVKey);
  SG::ReadCondHandle<ILArOFC>             ofcHdl          (m_ofcKey);
  SG::ReadCondHandle<ILArShape>           shapeHdl        (m_shapeKey);
  
  // const LArOnOffIdMapping* larCabling=*larCablingHdl;
  larCabling    =*larCablingHdl;
  noiseCDO      =*noiseHdl;
  run2DSPThresh = std::unique_ptr<LArDSPThresholdsFlat>(new LArDSPThresholdsFlat(*dspThrshAttrHdl));
  peds          =*pedHdl;
  adc2MeVs      =*adc2mevHdl;
  ofcs          =*ofcHdl;
  shapes        =*shapeHdl;

  const xAOD::EventInfo *ei = nullptr;

  ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));
  
  // EventInfo
  if(!dumpEventInfo(ei)) ATH_MSG_INFO("Event information cannot be collected!");

  // Jets
  if (pr_doJetRoiDump){
    const xAOD::JetContainer* jets = nullptr;
    if(!dumpJetsInfo(jets, pr_jetName, larCabling)) ATH_MSG_INFO("Jets information cannot be collected!");
    if(!buildRingsJets(jets, pr_jetName)) ATH_MSG_INFO("Jets information cannot be collected!");
  }
  
  // Clusters  
  if (pr_doClusterDump){
    const xAOD::CaloClusterContainer* cls = nullptr;
    ATH_CHECK ( evtStore()->retrieve(cls, pr_clusterName) );
    // if(!dumpClusterInfo(cls, pr_clusterName, larCabling)) ATH_MSG_DEBUG("Cluster information cannot be collected!");
    for (auto cl = cls->begin(); cl != cls->end(); ++cl){
      ATH_MSG_DEBUG ("Cluster "<< c_clusterIndexCounter << ", Total size: " << (*cl)->numberCells() << "/  "<< (*cl)->clusterSize() << ", etaBE: " << (*cl)->etaBE(2) << ", numberCellsInSampling: " << (*cl)->numberCellsInSampling(CaloSampling::EMB2) <<", eta size: " << (*cl)->etasize(CaloSampling::EMB2) << ", phi size: " << (*cl)->phisize(CaloSampling::EMB2) );

      if (! dumpClusterCells((*cl), c_clusterIndexCounter ) )  ATH_MSG_WARNING ("CaloCluster Cells cannot be dumped for Zee.");

      c_clusterIndexCounter++;
    }
  }
  
  if (pr_isMC){
    // Events Truth
    if (pr_doTruthEventDump){
      if(!dumpTruthEvent()) ATH_MSG_DEBUG("Truth Event information cannot be collected!");
    }    
    // Particle Truth
    if (pr_doTruthPartDump){
      if(!dumpTruthParticle()) ATH_MSG_DEBUG("Truth Particle information cannot be collected!");
    }
    
  }
  
  // Photons
  if (pr_doPhotonDump){
    const xAOD::PhotonContainer* photonCnt = nullptr;
    if(!dumpPhotons(photonCnt)) ATH_MSG_DEBUG("Photon container information cannot be collected!");
  }

  // ***** DEBUG ******
  // bool bMyTestEvent = (ei->eventNumber()==380449669);

  // Apply the Zee cut
  if (pr_doTagAndProbe && !pr_doClusterDump){
  // if ( (pr_doTagAndProbe && !pr_doClusterDump) && bMyTestEvent ){
    if(!dumpZeeCut(ei)) ATH_MSG_DEBUG("Zee cut algorithm cannot be performed!");

    xAOD::ElectronContainer *elContainer = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    std::string eSelectionText = "";

    if (pr_doElecSelectByTrackOnly){ // if flag true, loop over the container of pre-selected electrons.
      elContainer     = electronsSelectedByTrack;
      eSelectionText  = "Number of electronsSelectedByTrack single electrons: ";
    }
    else{ // if false, loop over the T&P result container
      elContainer     = tAndPElectrons; 
      eSelectionText  = "Number of tagsAndProbe events: ";
    }
    // Dump Electrons for Track Selection only.
    ATH_MSG_DEBUG(eSelectionText << electronsSelectedByTrack->size() );
    int electronIndex = 0;
    for (auto el=elContainer->begin() ; el!=elContainer->end() ; ++el){ 
    // commented for testing
    // const xAOD::CaloClusterContainer* cls711 = nullptr;
    // ATH_CHECK ( evtStore()->retrieve(cls711, "egamma711Clusters") );
    // for (auto cl=cls711->begin(); cl!=cls711->end(); ++cl){

      const xAOD::Electron *elec = (*el);
      const auto& clusLinks = elec->caloClusterLinks(); // get the cluster links for the electron
      
      // loop over clusters and dump cells
      for (const auto& clusLink : clusLinks){

        // get associated topoclusters from superclusters        
        if (pr_getAssociatedTopoCluster){
          auto assocCls = xAOD::EgammaHelpers::getAssociatedTopoClusters( (*clusLink) ); // std::vector<const xAOD::CaloCluster*>

          for ( auto assocCl : assocCls){
            ATH_MSG_DEBUG ("AssociatedTopoCluster " << c_clusterIndexCounter << ": e: "<< assocCl->e() << ", eta: " << assocCl->eta() << ", phi: " << assocCl->phi() <<". Total size: " << assocCl->numberCells() << "/  "<< assocCl->clusterSize() << ", etaBE: " << assocCl->etaBE(2) << ", numberCellsInSampling: " << assocCl->numberCellsInSampling(CaloSampling::EMB2) <<", eta size: " << assocCl->etasize(CaloSampling::EMB2) << ", phi size: " << assocCl->phisize(CaloSampling::EMB2) );
            if (! dumpClusterCells(assocCl, c_clusterIndexCounter ) )  ATH_MSG_WARNING ("CaloCluster Cells cannot be dumped for Zee.");
            if (! dumpCellsInFixedSizeROI(assocCl, c_clusterIndexCounter ) ) ATH_MSG_WARNING ("Fixed size window cannot be dumped.");

            c_clusterIndexCounter++;
          }
        }
        else{ // dump only the superclusters
          if (! dumpClusterCells((*clusLink), c_clusterIndexCounter ) )  ATH_MSG_WARNING ("CaloCluster Cells cannot be dumped for Zee.");
          c_clusterIndexCounter++;
        }
        
        if (pr_testCode){
          if(!testCode((*clusLink))) ATH_MSG_DEBUG("Test code cannot run!");
          // if(!testCode((*cl))) ATH_MSG_DEBUG("Test code cannot run!");
        }
        c_electronIndex_clusterLvl->push_back(electronIndex); // electron indexes for each cluster
        // c_clusterIndexCounter++;
      } //end-for loop in clusLinks]

      // dump tag and probe electrons
      
      if (!dumpElectrons(elec)) ATH_MSG_WARNING ("Cannot dump ordinary electrons...");
      if (!dumpOfflineSS(elec)) ATH_MSG_WARNING ("Cannot dump offline shower shapes for electrons...");
      el_index->push_back(electronIndex);

      electronIndex++;

    } // end-for loop in electronsSelectedByTrack electrons
  } // end if-start TagAndProbe chain

  m_Tree->Fill();

  return StatusCode::SUCCESS;
}


StatusCode EventReaderAlg::dumpOfflineSS(const xAOD::Electron *electron){
    el_f1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f1));
    el_f3->push_back(electron->showerShapeValue(xAOD::EgammaParameters::f3));
    el_eratio->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Eratio));
    el_weta1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta1));
    el_weta2->push_back(electron->showerShapeValue(xAOD::EgammaParameters::weta2));
    el_fracs1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::fracs1));
    el_wtots1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::wtots1));
    el_e277->push_back(electron->showerShapeValue(xAOD::EgammaParameters::e277));
    el_reta->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Reta));
    el_rphi->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rphi));
    el_deltae->push_back(electron->showerShapeValue(xAOD::EgammaParameters::DeltaE));
    el_rhad->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad));
    el_rhad1->push_back(electron->showerShapeValue(xAOD::EgammaParameters::Rhad1));
   
  return StatusCode::SUCCESS;
}

StatusCode  EventReaderAlg::dumpElectrons(const xAOD::Electron* electron){
  // el_index->push_back(electron->());
  float electronEt = electron->e()/(cosh(electron->trackParticle()->eta()));
  float eoverp = 0.;
  float track_p = (electron->trackParticle())->pt()*std::cosh((electron->trackParticle())->eta());
  eoverp = (electron->caloCluster())->e()/track_p;
  el_eoverp->push_back(eoverp);
  el_Pt->push_back(electron->pt());
  el_et->push_back(electronEt);
  el_Eta->push_back(electron->eta());
  el_Phi->push_back(electron->phi());
  el_m->push_back(electron->m());

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpEventInfo(const xAOD::EventInfo *ei){
  // const xAOD::EventInfo *eventInfo = nullptr;
  // ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  e_runNumber       = ei->runNumber();
  e_eventNumber     = ei->eventNumber();
  e_bcid            = ei->bcid();
  // e_lumiBlock       = ei->lumiBlock(); // bugged !!
  e_inTimePileup    = ei->actualInteractionsPerCrossing();
  e_outOfTimePileUp = ei->averageInteractionsPerCrossing();

  ATH_MSG_INFO ("in execute, runNumber = " << e_runNumber << ", eventNumber = " << e_eventNumber << ", bcid: " << e_bcid );
  
  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpZeeCut(const xAOD::EventInfo *ei){
  ATH_MSG_INFO("Dumping Zee cut region...");

  const xAOD::ElectronContainer* electronsCnt   = nullptr;
  const xAOD::VertexContainer* primVertexCnt    = nullptr;

  ATH_CHECK (evtStore()->retrieve(electronsCnt,"Electrons"));
  ATH_CHECK (evtStore()->retrieve(primVertexCnt,"PrimaryVertices"));
 
  // check the eventInfo container
  if ( !ei ){
    ATH_MSG_WARNING("Failed to retrieve EventInfo");
    return StatusCode::SUCCESS;
  }
  // ATH_MSG_DEBUG("Event test error in LAR...");
  if (ei->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error) {
    ATH_MSG_WARNING("Event not passing LAr");
    return StatusCode::SUCCESS;
  }

  // check the electrons container
  if(!electronsCnt){
    ATH_MSG_WARNING("Failed to retrieve offline Electrons ");
    return StatusCode::SUCCESS;
  }

  // Pre-selection of electrons
  for (auto el : *electronsCnt){
    if (trackSelectionElectrons(el, primVertexCnt) == true){
      if (eOverPElectron(el) == true){ // if 0.7 < E/p < 1.4
        electronsSelectedByTrack->push_back( const_cast<xAOD::Electron*>(el));
      }
    }
    else continue;
  }

  if (pr_doElecSelectByTrackOnly){ // do not proceed to T&P
    return StatusCode::SUCCESS;
  }

  // if (electronsCnt->size() < 2){// Not enough events for T&P
  if (electronsSelectedByTrack->size() < 2){
    ATH_MSG_DEBUG("Not enough Electrons for T&P (nElectrons="<<electronsSelectedByTrack->size() << ").");
    return StatusCode::SUCCESS;
  }

  // ## Loop over electron container ##
  // for ( auto elTag : *electronsCnt){
  for (auto elTagIter=electronsSelectedByTrack->begin() ; elTagIter!=electronsSelectedByTrack->end() ; ++elTagIter){
    const xAOD::Electron *elTag = (*elTagIter);
      
    if (! isTagElectron(elTag)) continue;
    ATH_MSG_DEBUG("(TagAndProbe) Electron is a Tag!");


    for (auto elProbeIter=electronsSelectedByTrack->begin() ; elProbeIter!=electronsSelectedByTrack->end() ; ++elProbeIter){
      const xAOD::Electron *elProbe = (*elProbeIter);
      // probeIndex++;
      if (elTag==elProbe) continue;

      //check for charge (opposite charges for Zee)
      if ( elTag->charge() == elProbe->charge() ) continue;      
      ATH_MSG_DEBUG ("(TagAndProbe) Electron Pair Charges are opposite! Good.");

      //check for Zee mass    
      TLorentzVector el1;
      TLorentzVector el2;
      el1.SetPtEtaPhiE(elTag->pt(), elTag->trackParticle()->eta(), elTag->trackParticle()->phi(), elTag->e());
      el2.SetPtEtaPhiE(elProbe->pt(), elProbe->trackParticle()->eta(), elProbe->trackParticle()->phi(), elProbe->e());

      float tpPairMass = (el1 + el2).M(); // mass of the electron pair
      ATH_MSG_DEBUG ("(TagAndProbe) Electron Pair mass: " << tpPairMass << ". Min: "<< pr_minZeeMassTP*GeV << ". Max: " << pr_maxZeeMassTP*GeV);
      if (!((tpPairMass > pr_minZeeMassTP*GeV) && (tpPairMass < pr_maxZeeMassTP*GeV))){
        ATH_MSG_DEBUG ("(TagAndProbe) Electron pair not in Z mass window.");
        continue;
      }
      else{
        ATH_MSG_INFO ("(TagAndProbe) Electron-positron pair are in Z mass window.");
        // test for goodProbeElectron
        if (!isGoodProbeElectron(elProbe)){
          ATH_MSG_DEBUG (" Probe electron not passed in Goodnes of Probe test.");
          continue;
        }
        ATH_MSG_INFO("(TagAndProbe) Electron is a good probe.");
        // tp_probeIndex->push_back()
        ATH_MSG_INFO ("Tag and probe pair found.");

        ATH_MSG_INFO ("TAG: pt="<< elTag->pt() << " e="<< elTag->e());
        ATH_MSG_INFO ("PRO: pt="<< elProbe->pt() << " e="<< elProbe->e());
        ATH_MSG_INFO ("Zee: M="<< tpPairMass << " E="<< (el1 + el2).E());
        zee_M->push_back((el1 + el2).M());
        zee_E->push_back((el1 + el2).E());
        zee_pt->push_back((el1 + el2).Pt());
        zee_px->push_back((el1 + el2).Px());
        zee_py->push_back((el1 + el2).Py());
        zee_pz->push_back((el1 + el2).Pz());
        zee_T->push_back((el1 + el2).T());
        // zee_x->push_back((el1 + el2).X());
        // zee_y->push_back((el1 + el2).Y());
        // zee_z->push_back((el1 + el2).Z());
        zee_deltaR->push_back(el1.DeltaR(el2));


        // ****************************************************************
        // store electrons in a container to dump their clusters and cells.
        // myMuons->push_back( const_cast<xAOD::Muon*>(muon) );
        tAndPElectrons->push_back( const_cast<xAOD::Electron*>(elTag) );
        tAndPElectrons->push_back( const_cast<xAOD::Electron*>(elProbe) );
        // tagElectrons->push_back( const_cast<xAOD::Electron*>(elTag) );
        // probeElectrons->push_back( const_cast<xAOD::Electron*>(elProbe) );
        // tp_tagIndex->push_back(electronIndex);
        // tp_probeIndex->push_back(probeIndex);
        // ****************************************************************
        
        // if (! dumpClusterCells(elTag->caloCluster()))
      }      
    } //end-loop probe electrons    
  } //end-loop tag electrons

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpTruthEvent(){
  ATH_MSG_INFO("Dumping Truth Event Info...");
  const xAOD::TruthEventContainer* truthEventCnt = nullptr;

  ATH_CHECK (evtStore()->retrieve(truthEventCnt, "TruthEvents"));
  xAOD::TruthEventContainer::const_iterator itr; //iterator for truth event
  // for (itr =  truthEventCnt->begin(); itr != truthEventCnt->end(); ++itr ){
  for (const xAOD::TruthEvent* evt : *truthEventCnt){
    ATH_MSG_DEBUG ("Looping over evt");
    int nVert = evt->nTruthVertices(); // number of truth vertices
    int nPart = evt->nTruthParticles(); // number of truth particles

    ATH_MSG_DEBUG (" nVert: " << nVert <<", nPart: "<< nPart);

    // int nVert = (*itr)->nTruthVertices(); // number of truth vertices
    // int nPart = (*itr)->nTruthParticles(); // number of truth particles

    for (int iVtx = 0; iVtx < nVert; ++iVtx){
      
      const xAOD::TruthVertex* vertex = evt->truthVertex(iVtx); // get truth vertex
      // const xAOD::TruthVertex* vertex = (*itr)->truthVertex(iVtx); // get truth vertex

      ATH_MSG_DEBUG ("Loop in vertex #"<< iVtx << ". ");
      
      float vert_x      = vertex->x(); // vertex displacement
      float vert_y      = vertex->y(); // vertex displacement
      float vert_z      = vertex->z(); // vertex displacement in beam direction
      float vert_time   = vertex->t(); // vertex time
      float vert_perp   = vertex->perp(); // Vertex transverse distance from the beam line
      float vert_eta    = vertex->eta(); // Vertex pseudorapidity
      float vert_phi    = vertex->phi(); // Vertex azimuthal angle
      int vert_barcode  = vertex->barcode(); //barcode
      int vert_id       = vertex->id();

      mc_vert_x->push_back(vert_x);
      mc_vert_y->push_back(vert_y);
      mc_vert_z->push_back(vert_z);
      mc_vert_time->push_back(vert_time);
      mc_vert_perp->push_back(vert_perp);
      mc_vert_eta->push_back(vert_eta);
      mc_vert_phi->push_back(vert_phi);
      mc_vert_barcode->push_back(vert_barcode);
      mc_vert_id->push_back(vert_id);

    } //
    for (int iPart = 0; iPart < nPart; ++iPart){
      ATH_MSG_DEBUG ("Loop in particle #"<< iPart);
      const xAOD::TruthParticle* particle = evt->truthParticle(iPart); //get truth particle
      // const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart); //get truth particle

      mc_part_energy->push_back(particle->e());
      mc_part_pt->push_back(particle->pt());
      mc_part_pdgId->push_back(particle->pdgId());
      mc_part_m->push_back(particle->m());
      mc_part_phi->push_back(particle->phi());
      mc_part_eta->push_back(particle->eta());
      mc_part_status->push_back(particle->status());
      mc_part_barcode->push_back(particle->barcode());
    } // end loop particles in event
    
  } // end loop truth events container

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpTruthParticle(){
  ATH_MSG_INFO("Dumping Truth Particle...");
  const xAOD::TruthParticleContainer* truthParticleCnt = nullptr;

  ATH_CHECK (evtStore()->retrieve (truthParticleCnt, "TruthParticles")); // retrieve container

  if (truthParticleCnt->size() == 0) ATH_MSG_INFO("truthParticleCnt is empty!");
  else{
    for (auto* truthParticle : *truthParticleCnt){
      mc_part_energy->push_back(truthParticle->e());
      mc_part_pt->push_back(truthParticle->pt());
      mc_part_pdgId->push_back(truthParticle->pdgId());
      mc_part_m->push_back(truthParticle->m());
      mc_part_phi->push_back(truthParticle->phi());
      mc_part_eta->push_back(truthParticle->eta());
    }
  }
  return StatusCode::SUCCESS;
  
}

StatusCode EventReaderAlg::dumpPhotons(const xAOD::PhotonContainer* photonCnt){
  ATH_MSG_INFO("Dumping Photons...");
  ATH_CHECK (evtStore()->retrieve (photonCnt, "Photons")); // retrieve photons container

  if (photonCnt->size() == 0) ATH_MSG_INFO ("photonCnt is empty!");
  else{
    for (auto* photon : *photonCnt){
      // bool isGood;
      // if ( photon->passSelection(isGood, pr_offTagTightness) ){
      //   ATH_MSG_DEBUG("(DumpPhoton) Trigger " << pr_offTagTightness << " is OK");
      // }
      // else{
      //   ATH_MSG_DEBUG("(DumpPhoton) Misconfiguration: " << pr_offTagTightness << " is not a valid working point for photons");
      // }

      ph_energy->push_back(photon->e());
      ph_pt->push_back(photon->pt());
      ph_phi->push_back(photon->phi());
      ph_eta->push_back(photon->eta());
      ph_m->push_back(photon->m());
    }
  }

  return StatusCode::SUCCESS;
}

// StatusCode EventReaderAlg::dumpClusterCells(const xAOD::CaloCluster *cl, const LArOnOffIdMapping* larCabling, int clusIndex){
StatusCode EventReaderAlg::dumpClusterCells(const xAOD::CaloCluster *cl, int clusIndex){

  const LArDigitContainer* LarDigCnt = nullptr;
  const TileDigitsContainer* TileDigCnt = nullptr;
  const TileRawChannelContainer* TileRawCnt = nullptr;
  const LArRawChannelContainer* LArRawCnt = nullptr;

  // int clusterIndex = 0; //index of cluster in the same event.

  // calorimeter level
  std::bitset<200000>       larClusteredDigits;
  std::bitset<65536>        tileClusteredDigitsLG, tileClusteredDigitsHG;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInClusterMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  // std::vector<double>       cellClusDeltaEta;//unused
  // std::vector<double>       cellClusDeltaPhi; //unused
  // std::vector<double>       cellGranularityEtaInClusterMap; //unused
  // std::vector<double>       cellGranularityPhiInClusterMap; //unused
  // channel level
  std::vector<double>       channelEnergyInClusterMap;
  std::vector<double>       channelTimeInClusterMap;
  std::vector<double>       channelEtaInClusterMap;
  std::vector<double>       channelPhiInClusterMap;
  std::vector<float>        channelEffectiveSigmaClusterMap;
  std::vector<float>        channelNoiseClusterMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;

  // cluster
  double  clusEta           = cl->eta();
  double  clusPhi           = cl->phi();
  double  clusEt            = cl->et();
  double  clusPt            = cl->pt();
  double  clusTime          = cl->time();
  // double  clusCellSumE_Eta  = 0.0;
  // double  clusCellSumE_Phi  = 0.0;
  // double  clusCellSumE      = 0.0;
  // double  clusEtaBaryc      = 0.0;
  // double  clusPhiBaryc      = 0.0;

  ATH_MSG_DEBUG (" (Cluster) Cluster: "<< clusIndex <<", numberCells: " << cl->numberCells() << ", e = " << cl->e() << ", time = "<< clusTime << ", pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());

  // loop over cells in cluster
  auto itrCellsBegin = cl->cell_begin();
  auto itrCellsEnd = cl->cell_end();
  int cellNum = 0; //cell number just for printing out
  // int cellIndex = 0; //cell index inside cluster (ordered)
  // int readOutIndex = 0; // channel index inside cluster

  for (auto itCells=itrCellsBegin; itCells != itrCellsEnd; ++itCells){
    const CaloCell* cell = (*itCells); //get the caloCells
    if (cell){ // check for empty clusters
      Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

      int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

      double  eneCell   = cell->energy();
      double  timeCell  = cell->time();
      double  etaCell   = cell->eta();
      double  phiCell   = cell->phi();          
      int     gainCell  = cell->gain(); // CaloGain type
      // int     layerCell = cell->caloDDE()->getLayer(); //
      int     layerCell = m_calocell_id->calo_sample(cell->ID());
      bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
      bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
      bool    isLAr     = cell->caloDDE()->is_lar_em();
      bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
      bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
      double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
      double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity
      float   effSigma  = noiseCDO->getEffectiveSigma(cell->ID(), cell->gain(), cell->energy()); // effective sigma of cell related to its noise.
      float   cellNoise = noiseCDO->getNoise(cell->ID(), cell->gain()); // cell noise value

      IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
      IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
      IdentifierHash chidHash, adcidHash;
      HWIdentifier chhwid, adchwid;
      size_t index, indexPmt1, indexPmt2;

      // ========= TileCal ==========
      if (isTile){ //Tile
        const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
        // ATH_MSG_DEBUG ("tCell->quality() " << tCell->quality());

        Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

        // main TileCell readout properties:
        int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
        int gain2   = tCell->gain2();
        // int qual1   = tCell->qual1(); // quality factor (chi2)
        // int qual2   = tCell->qual2();
        bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
        bool badch2 = tCell->badch2();
        bool noch1  = (gain1<0 || gain1>1); // there is a ch?
        bool noch2  = (gain2<0 || gain2>1);
        float time1 = tCell->time1(); // reco time in both chs (OF2)
        float time2 = tCell->time2();
        float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
        float ene2  = tCell->ene2();

        // verify if it's a valid ch pmt 1
        if ( !noch1 && (!badch1 || !pr_noBadCells ) ){
          // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
          HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
          IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
          indexPmt1 = (size_t) (hashpmt1);

          int adc1     = m_tileHWID->adc(adchwid_pmt1);
          int channel1 = m_tileHWID->channel(adchwid_pmt1);
          int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
          int ros1     = m_tileHWID->ros(adchwid_pmt1);

          if (pr_printCellsClus){ //optional to help debugging
            ATH_MSG_INFO (" (Cluster) in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
          }
          // test if there is conflict in the map (only for debugging reason)
          if (tileClusteredDigitsLG.test(indexPmt1) && !gain1 ) { // if gain is LOW and its HASH was filled
            ATH_MSG_ERROR (" ##### (Cluster) Error Tile LG: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1 << ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E1/time1: "<< ene1 << "/" << time1);
            continue;
            }
          else if (tileClusteredDigitsHG.test(indexPmt1) && gain1) { // if gain is HIGH and its HASH was filled
            ATH_MSG_ERROR (" ##### (Cluster) Error Tile HG: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1 << ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E1/time1: "<< ene1 << "/" << time1);
            continue;
            }
          else { //If there wasn't an error, 
            if (!gain1) tileClusteredDigitsLG.set(indexPmt1); //fill the map in case PMT1 readout is LG
            if (gain1)  tileClusteredDigitsHG.set(indexPmt1); //fill the map, case PMT1 readout is HG            
            caloHashMap.push_back(indexPmt1);
            cellIndexMap.push_back(c_cellIndexCounter);
            channelIndexMap.push_back( (float) (round( (c_cellIndexCounter + 0.1)*10)/10) ); // adds 0.1 to the cell index, to indicate this is the PMT 1
            channelHwidInClusterMap.push_back(adchwid_pmt1);
            channelTimeInClusterMap.push_back(time1);
            channelEnergyInClusterMap.push_back(ene1);
            channelCaloRegionMap.push_back(caloRegionIndex);
            channelEffectiveSigmaClusterMap.push_back(effSigma);
            channelNoiseClusterMap.push_back(cellNoise);
            if (!pr_noBadCells) badChannelMap.push_back(badch1);

            // readOutIndex++;
          }
          
        } //end-if its a valid pmt1

        // verify if it's a valid ch pmt 2
        if ( !noch2 && (!badch2 || !pr_noBadCells) ){
          HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
          IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);
          indexPmt2 = (size_t) (hashpmt2);

          int adc2     = m_tileHWID->adc(adchwid_pmt2);
          int channel2 = m_tileHWID->channel(adchwid_pmt2);
          int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
          int ros2     = m_tileHWID->ros(adchwid_pmt2);

          if (pr_printCellsClus){ //optional to help debugging
            ATH_MSG_INFO (" (Cluster) in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
          }
          // test if there is conflict in the map (only for debugging reason)
          if (tileClusteredDigitsLG.test(indexPmt2) && !gain2 ) { // if gain is LOW and its HASH was filled
            ATH_MSG_ERROR (" ##### (Cluster) Error Tile LG: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2<< ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E2/time2: "<< ene2 << "/" << time2);
            // continue;
            }
          else if (tileClusteredDigitsHG.test(indexPmt2) && gain2 ) { // if gain is HIGH and its HASH was filled
            ATH_MSG_ERROR (" ##### (Cluster) Error Tile HG: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2<< ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E2/time2: "<< ene2 << "/" << time2);
            // continue;
            }
          else {//If there wasn't an error,
            if (!gain2) tileClusteredDigitsLG.set(indexPmt2); //fill the map, case PMT2 is LG
            if (gain2)  tileClusteredDigitsHG.set(indexPmt2); //fill the map, case PMT2 is HG
            caloHashMap.push_back(indexPmt2);
            cellIndexMap.push_back(c_cellIndexCounter);
            channelIndexMap.push_back( (float) (round( (c_cellIndexCounter + 0.2)*10)/10) ); // adds 0.2 to the cell index, to indicate this is the PMT2
            channelHwidInClusterMap.push_back(adchwid_pmt2);
            channelTimeInClusterMap.push_back(time2);
            channelEnergyInClusterMap.push_back(ene2);
            channelCaloRegionMap.push_back(caloRegionIndex);
            channelEffectiveSigmaClusterMap.push_back(effSigma);
            channelNoiseClusterMap.push_back(cellNoise);
            if (!pr_noBadCells) badChannelMap.push_back(badch2);

            // readOutIndex++;
          }
        }
        if (noch1)   ATH_MSG_INFO (" (Cluster)(tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
        if (noch2)   ATH_MSG_INFO (" (Cluster)(tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
        if ( (badch1 && badch2) && pr_noBadCells )  {
          ATH_MSG_INFO (" (Cluster)(tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and pmt2 channel! Skipping cell.");
          continue;
        }
        if ( (badch1 && noch2) && pr_noBadCells ) {
          ATH_MSG_INFO (" (Cluster)(tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and has no valid pmt2 channel! Skipping cell.");
          continue;
        }

      } // end if isTile

      // ========= LAr ==========
      else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
        chhwid = larCabling->createSignalChannelID(cellId);
        // std::string hwid_lar = chid.getString();
        // ATH_MSG_INFO (typeid(chhwid).name());
        chidHash =  m_onlineLArID->channel_Hash(chhwid);
        index = (size_t) (chidHash);

        // ATH_MSG_DEBUG (" LAr Cel index: " << index << ", hwid: " << chhwid << ", E/t="<<cell->energy() << "/"<< cell->time() << ", Eta/Phi: "<< cell->eta() << "/" << cell->phi() );

        if (larClusteredDigits.test(index)) {
          ATH_MSG_ERROR (" ##### (Cluster) Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of index: " << index << " and hwid: " << chhwid << ". Cell_E/t="<<cell->energy() << "/"<< cell->time() << ". Eta/Phi: "<< cell->eta() << "/" << cell->phi());
          continue;
          }
        // else if (badCell && pr_noBadCells){
        if (badCell && pr_noBadCells){
          ATH_MSG_INFO (" (Cluster)(LAr) Cell "<< cellNum <<" in cluster is a bad LAr channel! Skipping the cell.");
          continue;
        }
        // else{
          larClusteredDigits.set(index);
          caloHashMap.push_back(index);
          cellIndexMap.push_back(c_cellIndexCounter);
          channelIndexMap.push_back( (float) (round( (c_cellIndexCounter + 0.0)*10)/10) ); 
          channelHwidInClusterMap.push_back(chhwid);
          channelTimeInClusterMap.push_back(timeCell);
          channelEnergyInClusterMap.push_back(eneCell);
          channelCaloRegionMap.push_back(caloRegionIndex);
          channelEffectiveSigmaClusterMap.push_back(effSigma);
          channelNoiseClusterMap.push_back(cellNoise);
          if (!pr_noBadCells) badChannelMap.push_back(badCell);
        // }
        // readOutIndex++;
        if (pr_printCellsClus){ //optional to help debugging
          ATH_MSG_INFO (" (Cluster) in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<") ID: " << cellId << "). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):" << ". Eta/Phi: "<< cell->eta() << "/" << cell->phi());
        }
        // if 
      } //end else LAr

      else {
        ATH_MSG_ERROR (" ####### (Cluster) ERROR ! No CaloCell region was found!");
        continue;
      }

      // ##############################
      //  CELL INFO DUMP (CLUSTER)
      // ##############################
      // double cluster_dphi = fabs(clusPhi-phiCell)  < pi ? (clusPhi-phiCell) : twopi - fabs(clusPhi-phiCell); //wrap correction
      double cluster_dphi = (clusPhi-phiCell)  < -pi ? twopi + (clusPhi-phiCell) : ( clusPhi-phiCell  > pi ? (clusPhi-phiCell) - twopi : clusPhi-phiCell );

      // if (fabs(clusPhi-phiCell)  < pi) ATH_MSG_INFO ("abs deltaPhi = " << fabs(clusPhi-phiCell));
      // else ATH_MSG_INFO ("(cluster) deltaPhi = " << (clusPhi-phiCell) <<". Corrected value is: " << cluster_dphi );

      c_clusterIndex_cellLvl->push_back(clusIndex);
      c_clusterCellIndex->push_back(c_cellIndexCounter);
      c_cellGain->push_back(gainCell);
      c_cellLayer->push_back(layerCell);
      c_cellEta->push_back(etaCell);
      c_cellPhi->push_back(phiCell);
      c_cellDEta->push_back(detaCell);
      c_cellDPhi->push_back(dphiCell);
      c_cellToClusterDEta->push_back(clusEta - etaCell);
      c_cellToClusterDPhi->push_back(cluster_dphi);
      c_cellRegion->push_back(caloRegionIndex);
      
      c_cellIndexCounter++;
      cellNum++;
    } // end if-cell is empty verification
  } // end loop at cells inside cluster

  // ##############################
  //  CLUSTER INFO DUMP (CLUSTER)
  // // ##############################
  // clusEtaBaryc      = clusCellSumE_Eta / clusCellSumE;
  // clusPhiBaryc      = fixPhi( clusCellSumE_Phi / clusCellSumE ); // verify if phi value is inside -pi,pi
  // ATH_MSG_INFO ("clusEtaBaryc " << clusEtaBaryc << " clusPhiBaryc " << clusPhiBaryc);

  // TEST ********************************
  int nCellsPS = getNumberCellsInClusterSampling  (0, c_cellLayer);
  int nCellsEMB1 = getNumberCellsInClusterSampling(1, c_cellLayer);
  int nCellsEMB2 = getNumberCellsInClusterSampling(2, c_cellLayer);
  int nCellsEMB3 = getNumberCellsInClusterSampling(3, c_cellLayer);
  if (nCellsPS != 0) c_nCellsInPS->push_back(nCellsPS);
  if (nCellsEMB1 != 0) c_nCellsInEMB1->push_back(nCellsEMB1);
  if (nCellsEMB2 != 0) c_nCellsInEMB2->push_back(nCellsEMB2);
  if (nCellsEMB3 != 0) c_nCellsInEMB3->push_back(nCellsEMB3);
  //**************************************
  c_clusterIndex->push_back(clusIndex);
  c_clusterEnergy->push_back(clusEt);
  c_clusterTime->push_back(clusTime);
  c_clusterEta->push_back(clusEta);
  c_clusterPhi->push_back(clusPhi);
  c_clusterPt->push_back(clusPt);
  // c_clusterEta_calc->push_back(clusEtaBaryc);
  // c_clusterPhi_calc->push_back(clusPhiBaryc);

   // ##############################
  //  DIGITS CHANNEL INFO DUMP (CLUSTER)
  // ##############################
  // Loop over ALL channels in LAr Digit Container.
  // Dump only the channels inside the previous clusters

  // ========= LAr ==========
  if (larClusteredDigits.any()){
    ATH_MSG_DEBUG (" (Cluster) Dumping LAr Digits...");
    ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

    for (const LArDigit* dig : *LarDigCnt) {
      HWIdentifier channelID  = dig->channelID();
      IdentifierHash idHash   = m_onlineLArID->channel_Hash(channelID);      
      size_t index = (size_t) (idHash);

      if (larClusteredDigits.test(index)) {
        for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){
          if (channelHwidInClusterMap[k]==channelID){
            // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
            // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
            
            // digits
            std::vector<short> larDigitShort = dig->samples();
            std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
            // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
            // c_larSamples->push_back(dig->samples());
            c_channelDigits->push_back(larDigit);

            // // TESTING
            // ATH_MSG_INFO ("ecut (Thrs) = " << run2DSPThresh->tQThr(channelID));
            const HWIdentifier        digId         = dig->hardwareID();
            const int                 digGain       = dig->gain();
            auto                      ofc_a         = ofcs->OFC_a(digId, digGain);
            auto                      ofc_b         = ofcs->OFC_b(digId, digGain);
            auto                      sig_shape     = shapes->Shape(digId, digGain);
            auto                      sig_shapeDer  = shapes->ShapeDer(digId, digGain);

            std::vector<double>       ofca, ofcb, shape, shapeDer;

            for (size_t k=0; k < ofc_a.size(); k++) ofca.push_back((double) ofc_a[k]);
            for (size_t k=0; k < ofc_b.size(); k++) ofcb.push_back((double) ofc_b[k]);
            for (size_t k=0; k < sig_shape.size(); k++) shape.push_back((double) sig_shape[k]);
            for (size_t k=0; k < sig_shapeDer.size(); k++) shapeDer.push_back((double) sig_shapeDer[k]);

            const auto&               adc2mev = adc2MeVs->ADC2MEV(digId, digGain);

            // Cell / readout data
            c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
            c_channelTime->push_back(channelTimeInClusterMap[k]);
            c_channelEffectiveSigma->push_back(channelEffectiveSigmaClusterMap[k]);
            c_channelNoise->push_back(channelNoiseClusterMap[k]);
            if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);
            // Calibration constants
            c_channelDSPThreshold->push_back(run2DSPThresh->tQThr(digId));
            c_channelOFCa->push_back(ofca);
            c_channelOFCb->push_back(ofcb);
            c_channelShape->push_back(shape);
            c_channelShapeDer->push_back(shapeDer);
            c_channelOFCTimeOffset->push_back(ofcs->timeOffset(digId,digGain));
            c_channelADC2MEV0->push_back(adc2mev[0]);
            c_channelADC2MEV1->push_back(adc2mev[1]);

            // Channel info
            int barrelEc = m_onlineLArID->barrel_ec(channelID);
            int posNeg = m_onlineLArID->pos_neg(channelID);
            int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
            int slot = m_onlineLArID->slot(channelID);
            int chn = m_onlineLArID->channel(channelID);      
            std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
            c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

            // Identifier  softId      = larCabling->cnvToIdentifier(channelID); // cell id

            // important indexes
            c_clusterChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
            c_clusterIndex_chLvl->push_back(clusIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
            c_channelHashMap->push_back(index); // online id hash for channel
            c_channelChannelIdMap->push_back(channelID.get_identifier32().get_compact());

            ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  channelID);
            ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << " HwId.get_compact() " << channelID.get_compact());
            ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  <<  " HwId.get_identifier32().get_compact() " << channelID.get_identifier32().get_compact());

            ATH_MSG_DEBUG("(Cluster) LAr OFC timeOffset: "<< ofcs->timeOffset(digId, digGain)  <<", dig->hardwareID: " << digId << ", dig->channelID: "<< channelID);
            ATH_MSG_DEBUG("\tOFCa ("<< ofca.size()<<"): ["<< ofca[0] <<", " << ofca[1] <<", " << ofca[2]<<", "  << ofca[3] <<"]");
            ATH_MSG_DEBUG("\tOFCb ("<< ofcb.size()<<"): ["<< ofcb[0] <<", " << ofcb[1] <<", " << ofcb[2]<<", "  << ofcb[3] <<"]");
            ATH_MSG_DEBUG("\tADC2MeV ("<< adc2mev.size() <<"): ["<< adc2mev[0] <<", " << adc2mev[1] <<"]");
            
            if (pr_printCellsClus){
              ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
            }
          } //end-if caloHashMap matches
          // else{
          //   if (caloHashMap[k] > m_onlineLArID->channelHashMax() ) ATH_MSG_ERROR ("###### HASH INDEX OUT OF BONDS FOR LAR !...");
          // }
        } //end for loop over caloHashMaps
      } // end-if clustBits Test
    } //end loop over LAr digits container
  } //end-if larClustBits is not empty


    // ========= TileCal LG+HG ==========
  if (tileClusteredDigitsLG.any() || tileClusteredDigitsHG.any()) {
    ATH_MSG_INFO ("(Cluster) Dumping tile PMT's Digits...");
    // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
    ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));

    for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
      if (TileDigColl->empty() ) continue;

      HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
      // uint32_t rodBCID = TileDigColl->getRODBCID();

      for (const TileDigits* dig : *TileDigColl){
        HWIdentifier adc_id = dig->adc_HWID();

        IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
        // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
        
        size_t index = (size_t) (adcIdHash);

        if (index <= m_tileHWID->adc_hash_max()){ //safety verification
          if (tileClusteredDigitsLG.test(index) || tileClusteredDigitsHG.test(index)){ //if this channel index is inside cluster map of LG or HG cells
            // Im aware this may not be the most optimized way to dump the containers info, but the solution below
            // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
            // because the order here is very important to keep the data links.
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
              if (channelHwidInClusterMap[k] == adc_id){ //find the index from pmt index to proceed.
                // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << adc_id );
                // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc

                // fake mask to tile calib. constants (temporary !!!!) it is done this way to maintain order to the data in the ntuple.
                std::vector<double> maskOfc{-999.0,-999.0,-999.0,-999.0};
                
                // Digits     
                // c_tileSamples->push_back(dig->samples()); //temporarily maintained
                c_channelDigits->push_back(dig->samples());

                // Channel / readout data
                c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
                c_channelTime->push_back(channelTimeInClusterMap[k]);
                c_channelEffectiveSigma->push_back(channelEffectiveSigmaClusterMap[k]);
                c_channelNoise->push_back(channelNoiseClusterMap[k]);
                if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);
                // Calibration constants
                // c_channelDSPThreshold->push_back(run2DSPThresh->tQThr(adc_id));
                c_channelDSPThreshold->push_back(-999); //masked values
                c_channelOFCa->push_back(maskOfc);
                c_channelOFCb->push_back(maskOfc);
                c_channelShape->push_back(maskOfc);
                c_channelShapeDer->push_back(maskOfc);
                c_channelOFCTimeOffset->push_back(-999.0);
                c_channelADC2MEV0->push_back(-999.0);
                c_channelADC2MEV1->push_back(-999.0);

                //channelInfo
                int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                int partition = ros - 1; //partition (0-3)
                int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                // important indexes
                c_clusterChannelIndex->push_back(channelIndexMap[k]);
                c_clusterIndex_chLvl->push_back(clusIndex); // what roi this ch belongs
                c_channelHashMap->push_back(index); // online id hash for channel
                // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                c_channelChannelIdMap->push_back(adc_id.get_identifier32().get_compact());

                ATH_MSG_DEBUG ("(Cluster) In DumpTile Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  adc_id << " HwId.get_compact() " << adc_id.get_compact() << " HwId.get_identifier32().get_compact() " << adc_id.get_identifier32().get_compact());
                  
                if (pr_printCellsClus){ //option for debugging
                  ATH_MSG_INFO ("(Cluster) In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   
                }
                // readOutIndex++;
                // cellIndex++;
              } // end if-pmt index comparison
              // if (caloHashMap[k] > m_tileHWID->adc_hash_max()){
              //   ATH_MSG_ERROR (" ###### HASH INDEX OUT OF BONDS FOR TILE !...");
              // } //end else
            }// end loop over pmt-indexes
          } // end if digit index exist on clusteredDigits
        } //end if hash exists
      } // end loop TileDigitsCollection
    } //end loop TileDigitsContainer
  } // end DumpTileDigits

  // ##############################
  //  RAW CHANNEL INFO DUMP (CLUSTER)
  // ##############################

  // ========= LAr ==========
  if (larClusteredDigits.any()) {
    ATH_MSG_INFO ("(Cluster) Dumping LAr Raw Channel...");
    ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

    for (const LArRawChannel& LArChannel : *LArRawCnt){
      // for (const TileRawChannel* TileChannel : *TileChannelColl){

      HWIdentifier    channelID   = LArChannel.channelID();
      IdentifierHash  chHwidHash  = m_onlineLArID->channel_Hash(channelID);
      Identifier      softID      = larCabling->cnvToIdentifier(channelID);
      size_t index = (size_t) (chHwidHash);

      if (larClusteredDigits.test(index)){ //if this channel index is inside 
        for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loop over the vector of hashIndex, and verify if the cell index match.
          if (channelHwidInClusterMap[k] == channelID){
            // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == channelID " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
            
            //RawCh info
            int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
            int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
            uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
            int provenance       = (int) LArChannel.provenance(); // its uint16_t
            float rawEnergyConv  = (float) (rawEnergy); 
            float rawTimeConv    = (float) (rawTime);
            float rawQualityConv = (float) (rawQuality);

            // Channel info
            int barrelEc = m_onlineLArID->barrel_ec(channelID);
            int posNeg = m_onlineLArID->pos_neg(channelID);
            int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
            int slot = m_onlineLArID->slot(channelID);
            int chn = m_onlineLArID->channel(channelID);      
            std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
            c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

            if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
              ATH_MSG_ERROR (" ###### (Cluster) LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
            }
            c_rawChannelAmplitude->push_back(rawEnergyConv);
            c_rawChannelTime->push_back(rawTimeConv);
            c_rawChannelQuality->push_back(rawQualityConv);
            c_rawChannelPedProv->push_back(provenance);
            c_rawChannelDSPThreshold->push_back(run2DSPThresh->tQThr(channelID));

            // important indexes
            c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
            c_clusterIndex_rawChLvl->push_back(clusIndex); // what roi this ch belongs
            c_rawChannelIdMap->push_back(channelID.get_identifier32().get_compact());

            if (pr_printCellsClus){ //optional to help debugging

              HWIdentifier hardwareID = LArChannel.hardwareID();
              HWIdentifier identifyID = LArChannel.identify();
              // uint16_t     provenance = LArChannel.provenance();

              ATH_MSG_INFO ("(Cluster) In DumpLAr Raw "<< channelIndexMap[k] <<": hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv <<" ecut (DSPThr) = " << run2DSPThresh->tQThr(channelID));
            } // end-if optional cell print
          } // end-if hwid match rawChID
        } // end loop over HWIDClusterMap
      } // end-if clust.test
    } //end loop LArRawChannelContainer
  } // end-if clustBits have Cells


  // ========= TileCal LG+HG ==========
  if (tileClusteredDigitsLG.any() || tileClusteredDigitsHG.any()) {
    ATH_MSG_INFO ("(Cluster) Dumping tile PMT's Raw Channel...");
    ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

    for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
      for (const TileRawChannel* TileChannel : *TileChannelColl){

        HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
        IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
        size_t index = (size_t) (chHwidHash);

        if (tileClusteredDigitsLG.test(index) || tileClusteredDigitsHG.test(index)){ //if this channel index is inside the bitmap of cells (LG+HG maps)
          for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
            if (channelHwidInClusterMap[k] == rawAdcHwid){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == rawAdcHwid " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
              
              // Channel info
              int tileAdc         = m_tileHWID->adc(rawAdcHwid);
              int tileCh          = m_tileHWID->channel(rawAdcHwid);
              int drawer          = m_tileHWID->drawer(rawAdcHwid);
              int ros             = m_tileHWID->ros(rawAdcHwid);
              int partition       = ros - 1;
              std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
              c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              //RawCh info
              float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
              float rawTime       = TileChannel->time();    // time relative to triggering bunch
              // quality is a number in [0,1] obtained by integrating the parent
              // distribution for the "goodness" from the DSP.  In hypothesis testing
              // terms, it is the significance of the hypothisis that the input
              // to the DSP matches the signal-model used to tune the DSP.                  
              float rawQuality    = TileChannel->quality();// quality of the sampling distribution
              float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
              c_rawChannelAmplitude->push_back(rawAmplitude);
              c_rawChannelTime->push_back(rawTime);
              c_rawChannelPedProv->push_back(rawPedestal);
              c_rawChannelQuality->push_back(rawQuality);
              c_rawChannelDSPThreshold->push_back(-999); // masked value
              // c_rawChannelDSPThreshold->push_back(run2DSPThresh->tQThr(rawAdcHwid));

              // important indexes
              c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
              c_clusterIndex_rawChLvl->push_back(clusIndex); // what roi this ch belongs
              c_rawChannelIdMap->push_back(rawAdcHwid.get_identifier32().get_compact());

              if (pr_printCellsClus){ //optional to help debugging
                int tileIndex, tilePmt;

                Identifier rawCellId = TileChannel->cell_ID();
                Identifier rawPmtId = TileChannel->pmt_ID();
                Identifier rawAdcId = TileChannel->adc_ID();

                TileChannel->cell_ID_index(tileIndex,tilePmt);

                ATH_MSG_INFO ("(Cluster) In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
              } // end-if optional cell print
            } // end-if hwid match rawChID
          } // end loop over HWIDClusterMap
        } // end-if clust.test
      } //end loop TileRawChannelCollection
    } //end loop TileRawChannelContainer
  } // end-if clustBits have Cells

  larClusteredDigits.reset();
  tileClusteredDigitsLG.reset();
  tileClusteredDigitsHG.reset();

  caloHashMap.clear();

  channelHwidInClusterMap.clear();
  cellIndexMap.clear();
  channelIndexMap.clear();
  channelEnergyInClusterMap.clear();
  channelTimeInClusterMap.clear();
  channelEtaInClusterMap.clear();
  channelPhiInClusterMap.clear();
  channelEffectiveSigmaClusterMap.clear();
  channelNoiseClusterMap.clear();
  // cellGranularityEtaInClusterMap.clear();
  // cellGranularityPhiInClusterMap.clear();
  channelCaloRegionMap.clear();
  if (!pr_noBadCells) badChannelMap.clear();
  // cellClusDeltaEta.clear();
  // cellClusDeltaPhi.clear();
  
  ATH_MSG_INFO ("  (Cluster) RawChannel: "<< c_rawChannelAmplitude->size() <<" channels dumped, from " << c_cellEta->size() <<" cluster cells. ");
  ATH_MSG_INFO ("  (Cluster) Digits: "<< c_channelDigits->size() <<" channels dumped, out of " << c_rawChannelAmplitude->size() <<" raw channel cluster channels. ");
  if (c_channelDigits->size() == c_rawChannelAmplitude->size()){
    ATH_MSG_INFO ("  (Cluster) ALL Digits from the cluster were dumped successfully!");}
  else{
    ATH_MSG_INFO ("  (Cluster) The digits from "<< (c_rawChannelAmplitude->size() - c_channelDigits->size()) <<" channels are missing!");}


  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpCellsInFixedSizeROI(const xAOD::CaloCluster *cl, int clusIndex){

  // Get a list of hot cells, one for each TopoCluster sampling
  std::vector<CaloCell* > hotCellList;
  std::vector<const CaloCell*> cellCollection;

  for (auto samp : sampRegions){
    CaloCell* hotCell = nullptr;
    findHottestCellInClusterSampling(cl, hotCell, samp); // point the hottest cell in samp for the cluster.
    // disclaimer: 
    // -> for this test, i am getting only the EMB2 hotcell to seed the new 7_11 cluster, only at the EMB2.
    // -> must check how to seed other samplings.
    // -> we can use the same eta-phi seed for each sampling
    // for each sampling layer
    // - convert the 7_11 window for EMB2 into delta_eta delta_phi window
    // - use this value for each layer, then calculate the number of cells.
    //    - the n_cells should be odd in eta and phi.
    if (hotCell){
      hotCellList.push_back(hotCell);
    }
    else{
      continue;
    }
    // if (! buildFixedSizeClusterInSampling(cellCollection, hotCell , samp , 7 , 11 ) ) ATH_MSG_WARNING ("buildFixedSizeClusterInSampling failed!");
    // 7x11 EMB2 Window
    if (! buildFixedSizeClusterInSampling(cellCollection, hotCell , samp , 0.175 , 0.269981 ) ) ATH_MSG_WARNING ("buildFixedSizeClusterInSampling failed!");
    ATH_MSG_DEBUG ("HotCell: "<< hotCell->eta() << " "<< hotCell->phi() << ". cellCollection size: " << cellCollection.size());

    if (cellCollection.size() == 0){
      ATH_MSG_INFO (" Fixed ROI window has 0 cells.");
      return StatusCode::SUCCESS;
    }

  }

  // for (auto hotCellInSampling : hotCellList){

  // }
  const LArDigitContainer* LarDigCnt = nullptr;
  const TileDigitsContainer* TileDigCnt = nullptr;
  const TileRawChannelContainer* TileRawCnt = nullptr;
  const LArRawChannelContainer* LArRawCnt = nullptr;

  // calorimeter level
  std::bitset<200000>       larClusteredDigits;
  std::bitset<65536>        tileClusteredDigitsLG, tileClusteredDigitsHG;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInClusterMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // channel level
  std::vector<double>       channelEnergyInClusterMap;
  std::vector<double>       channelTimeInClusterMap;
  std::vector<double>       channelEtaInClusterMap;
  std::vector<double>       channelPhiInClusterMap;
  std::vector<float>        channelEffectiveSigmaClusterMap;
  std::vector<float>        channelNoiseClusterMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;

    // cluster
  double  clusEta           = cl->eta();
  double  clusPhi           = cl->phi();
  double  clusEt            = cl->et();
  double  clusPt            = cl->pt();
  double  clusTime          = cl->time();
  // double  clusEt            = cl->et();
  // double  clusPt            = cl->pt(); 

  int cellNum = 0; //cell number just for printing out
  for (auto cell : cellCollection){
    if (cell){ // check for empty clusters
      Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

      int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

      double  eneCell   = cell->energy();
      double  timeCell  = cell->time();
      double  etaCell   = cell->eta();
      double  phiCell   = cell->phi();          
      int     gainCell  = cell->gain(); // CaloGain type
      // int     layerCell = cell->caloDDE()->getLayer(); //
      int     layerCell = m_calocell_id->calo_sample(cell->ID());
      bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
      bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
      bool    isLAr     = cell->caloDDE()->is_lar_em();
      bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
      bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
      double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
      double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity
      float   effSigma  = noiseCDO->getEffectiveSigma(cell->ID(), cell->gain(), cell->energy()); // effective sigma of cell related to its noise.
      float   cellNoise = noiseCDO->getNoise(cell->ID(), cell->gain()); // cell noise value


      IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
      IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
      IdentifierHash chidHash, adcidHash;
      HWIdentifier chhwid, adchwid;
      size_t index, indexPmt1, indexPmt2;

      // ========= TileCal ==========
      if (isTile){ //Tile
        const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
        // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

        Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

        // main TileCell readout properties:
        int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
        int gain2   = tCell->gain2();
        // int qual1   = tCell->qual1(); // quality factor (chi2)
        // int qual2   = tCell->qual2();
        bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
        bool badch2 = tCell->badch2();
        bool noch1  = (gain1<0 || gain1>1); // there is a ch?
        bool noch2  = (gain2<0 || gain2>1);
        float time1 = tCell->time1(); // reco time in both chs (OF2)
        float time2 = tCell->time2();
        float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
        float ene2  = tCell->ene2();

        // verify if it's a valid ch pmt 1
        if ( !noch1 && (!badch1 || !pr_noBadCells ) ){
          // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
          HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
          IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
          indexPmt1 = (size_t) (hashpmt1);

          int adc1     = m_tileHWID->adc(adchwid_pmt1);
          int channel1 = m_tileHWID->channel(adchwid_pmt1);
          int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
          int ros1     = m_tileHWID->ros(adchwid_pmt1);

          if (pr_printCellsClus){ //optional to help debugging
            ATH_MSG_INFO (" (7x11 ROI) in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
          }
          // test if there is conflict in the map (only for debugging reason)
          if (tileClusteredDigitsLG.test(indexPmt1) && !gain1) { // if PMT1 is LG and already filled in the LG map.
            ATH_MSG_ERROR (" ##### (7x11 ROI) Error Tile LG: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1 << ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E1/time1: "<< ene1 << "/" << time1);
            continue;
            }
          else if (tileClusteredDigitsHG.test(indexPmt1) && gain1) { // if PMT1 is HG and already filled in the HG map.
            ATH_MSG_ERROR (" ##### (7x11 ROI) Error Tile HG: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1 << ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E1/time1: "<< ene1 << "/" << time1);
            continue;
            }
          else { //If there wasn't an error, 
            if (!gain1) tileClusteredDigitsLG.set(indexPmt1); //fill the map in case PMT1 readout is LG
            if (gain1)  tileClusteredDigitsHG.set(indexPmt1); //fill the map, case PMT1 readout is HG
            caloHashMap.push_back(indexPmt1);
            cellIndexMap.push_back(c711_cellIndexCounter);
            channelIndexMap.push_back( (float) (round((c711_cellIndexCounter + 0.1)*10)/10) ); // adds 0.1 to the cell index, to indicate this is the PMT 1
            channelHwidInClusterMap.push_back(adchwid_pmt1);
            channelTimeInClusterMap.push_back(time1);
            channelEnergyInClusterMap.push_back(ene1);
            channelCaloRegionMap.push_back(caloRegionIndex);
            channelEffectiveSigmaClusterMap.push_back(effSigma);
            channelNoiseClusterMap.push_back(cellNoise);
            if (!pr_noBadCells) badChannelMap.push_back(badch1);

            // readOutIndex++;
          }
          
        } //end-if its a valid pmt1

        // verify if it's a valid ch pmt 2
        if ( !noch2 && (!badch2 || !pr_noBadCells) ){
          HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
          IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);            
          indexPmt2 = (size_t) (hashpmt2);

          int adc2     = m_tileHWID->adc(adchwid_pmt2);
          int channel2 = m_tileHWID->channel(adchwid_pmt2);
          int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
          int ros2     = m_tileHWID->ros(adchwid_pmt2);

          if (pr_printCellsClus){ //optional to help debugging
            ATH_MSG_INFO (" (7x11 ROI) in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
          }              
          // test if there is conflict in the map (only for debugging reason)
          if (tileClusteredDigitsLG.test(indexPmt2) && !gain2 ) { // if gain is LOW and its HASH was filled
            ATH_MSG_ERROR (" ##### (7x11 ROI) Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2<< ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E2/time2: "<< ene2 << "/" << time2);
            // continue;
            }
          else if (tileClusteredDigitsHG.test(indexPmt2) && gain2 ) { // if gain is HIGH and its HASH was filled
            ATH_MSG_ERROR (" ##### (7x11 ROI) Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2<< ". eta/phi: "<< tCell->eta() << "/" << tCell->phi() << ". E2/time2: "<< ene2 << "/" << time2);
            // continue;
            }
          else {//If there wasn't an error,
            if (!gain2) tileClusteredDigitsLG.set(indexPmt2); //fill the map, case PMT2 is LG
            if (gain2)  tileClusteredDigitsHG.set(indexPmt2); //fill the map, case PMT2 is HG
            caloHashMap.push_back(indexPmt2);
            cellIndexMap.push_back(c711_cellIndexCounter);
            channelIndexMap.push_back( (float) (round((c711_cellIndexCounter + 0.2)*10)/10) ); // adds 0.2 to the cell index, to indicate this is the PMT2
            channelHwidInClusterMap.push_back(adchwid_pmt2);
            channelTimeInClusterMap.push_back(time2);
            channelEnergyInClusterMap.push_back(ene2);
            channelCaloRegionMap.push_back(caloRegionIndex);
            channelEffectiveSigmaClusterMap.push_back(effSigma);
            channelNoiseClusterMap.push_back(cellNoise);
            if (!pr_noBadCells) badChannelMap.push_back(badch2);

            // readOutIndex++;
          }              
        }
        if (noch1)   ATH_MSG_DEBUG (" (7x11 ROI)(tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
        if (noch2)   ATH_MSG_DEBUG (" (7x11 ROI)(tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
        if ( (badch1 && badch2) && pr_noBadCells )  {
          ATH_MSG_DEBUG (" (7x11 ROI)(tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and pmt2 channel! Skipping cell.");
          continue;
        }
        if ( (badch1 && noch2) && pr_noBadCells ) {
          ATH_MSG_DEBUG (" (7x11 ROI)(tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and has no valid pmt2 channel! Skipping cell.");
          continue;
        }

      } // end if isTile

      // ========= LAr ==========
      else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
        chhwid = larCabling->createSignalChannelID(cellId);
        // std::string hwid_lar = chid.getString();
        // ATH_MSG_INFO (typeid(chhwid).name());
        chidHash =  m_onlineLArID->channel_Hash(chhwid);
        index = (size_t) (chidHash);

        // ATH_MSG_DEBUG (" LAr Cel index: " << index << ", hwid: " << chhwid << ", E/t="<<cell->energy() << "/"<< cell->time() << ", Eta/Phi: "<< cell->eta() << "/" << cell->phi() );

        if (larClusteredDigits.test(index)) {
          ATH_MSG_ERROR (" ##### (7x11 ROI) Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of index: " << index << " and hwid: " << chhwid << ". Cell_E/t="<<cell->energy() << "/"<< cell->time() << ". Eta/Phi: "<< cell->eta() << "/" << cell->phi());
          continue;
          }
        // else if (badCell && pr_noBadCells){
        if (badCell && pr_noBadCells){
          ATH_MSG_DEBUG (" (7x11 ROI)(LAr) Cell "<< cellNum <<" in cluster is a bad LAr channel! Skipping the cell.");
          continue;
        }
        // else{
          larClusteredDigits.set(index);
          caloHashMap.push_back(index);
          cellIndexMap.push_back(c711_cellIndexCounter);
          channelIndexMap.push_back( (float) (round((c711_cellIndexCounter + 0.0)*10)/10) );
          channelHwidInClusterMap.push_back(chhwid);
          channelTimeInClusterMap.push_back(timeCell);
          channelEnergyInClusterMap.push_back(eneCell);
          channelCaloRegionMap.push_back(caloRegionIndex);
          channelEffectiveSigmaClusterMap.push_back(effSigma);
          channelNoiseClusterMap.push_back(cellNoise);
          if (!pr_noBadCells) badChannelMap.push_back(badCell);
        // }
        // readOutIndex++;
        if (pr_printCellsClus){ //optional to help debugging
          ATH_MSG_INFO (" (7x11 ROI) in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<") ID: " << cellId << "). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):" << ". Eta/Phi: "<< cell->eta() << "/" << cell->phi());
        }
        // if 
      } //end else LAr

      else {
        ATH_MSG_ERROR (" ####### (7x11 ROI) ERROR ! No CaloCell region was found!");
        continue;
      }

      // ##############################
      //  CELL INFO DUMP (7x11 ROI)
      // ##############################
      // double cluster_dphi = fabs(clusPhi-phiCell)  < pi ? (clusPhi-phiCell) : twopi - fabs(clusPhi-phiCell); //wrap correction
      double cluster_dphi = (clusPhi-phiCell)  < -pi ? twopi + (clusPhi-phiCell) : ( clusPhi-phiCell  > pi ? (clusPhi-phiCell) - twopi : clusPhi-phiCell );

      // if (fabs(clusPhi-phiCell)  < pi) ATH_MSG_INFO ("abs deltaPhi = " << fabs(clusPhi-phiCell));
      // else ATH_MSG_INFO ("(7x11 ROI) deltaPhi = " << (clusPhi-phiCell) <<". Corrected value is: " << cluster_dphi );

      c711_clusterIndex_cellLvl->push_back(clusIndex);
      c711_clusterCellIndex->push_back(c711_cellIndexCounter);
      c711_cellGain->push_back(gainCell);
      c711_cellLayer->push_back(layerCell);
      c711_cellEta->push_back(etaCell);
      c711_cellPhi->push_back(phiCell);
      c711_cellDEta->push_back(detaCell);
      c711_cellDPhi->push_back(dphiCell);
      c711_cellToClusterDEta->push_back(clusEta - etaCell);
      c711_cellToClusterDPhi->push_back(cluster_dphi);
      c711_cellRegion->push_back(caloRegionIndex);
      
      c711_cellIndexCounter++;
      cellNum++;
    } // end if-cell is empty verification
  } // end loop at cells inside cluster

    // ##############################
  //  CLUSTER INFO DUMP (7x11 ROI)
  // // ##############################
  // clusEtaBaryc      = clusCellSumE_Eta / clusCellSumE;
  // clusPhiBaryc      = fixPhi( clusCellSumE_Phi / clusCellSumE ); // verify if phi value is inside -pi,pi
  // ATH_MSG_INFO ("clusEtaBaryc " << clusEtaBaryc << " clusPhiBaryc " << clusPhiBaryc);

  // TEST ********************************
  int nCellsPS = getNumberCellsInClusterSampling  (0, c711_cellLayer);
  int nCellsEMB1 = getNumberCellsInClusterSampling(1, c711_cellLayer);
  int nCellsEMB2 = getNumberCellsInClusterSampling(2, c711_cellLayer);
  int nCellsEMB3 = getNumberCellsInClusterSampling(3, c711_cellLayer);
  if (nCellsPS != 0) c711_nCellsInPS->push_back(nCellsPS);
  if (nCellsEMB1 != 0) c711_nCellsInEMB1->push_back(nCellsEMB1);
  if (nCellsEMB2 != 0) c711_nCellsInEMB2->push_back(nCellsEMB2);
  if (nCellsEMB3 != 0) c711_nCellsInEMB3->push_back(nCellsEMB3);
  //**************************************
  c711_clusterIndex->push_back(clusIndex);
  c711_clusterEnergy->push_back(clusEt);
  c711_clusterTime->push_back(clusTime);
  c711_clusterEta->push_back(clusEta);
  c711_clusterPhi->push_back(clusPhi);
  c711_clusterPt->push_back(clusPt);


   // ##############################
  //  DIGITS CHANNEL INFO DUMP (7x11 ROI)
  // ##############################
  // Loop over ALL channels in LAr Digit Container.
  // Dump only the channels inside the previous clusters

  // ========= LAr ==========
  if (larClusteredDigits.any()){
    ATH_MSG_INFO (" (7x11 ROI) Dumping LAr Digits...");
    ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

    for (const LArDigit* dig : *LarDigCnt) {
      HWIdentifier channelID = dig->channelID();
      IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
      size_t index = (size_t) (idHash);

      if (larClusteredDigits.test(index)) {
        for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){
          if (channelHwidInClusterMap[k]==channelID){
            // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
            // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
            
            // digits
            std::vector<short> larDigitShort = dig->samples();
            std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
            // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
            // c_larSamples->push_back(dig->samples());
            c711_channelDigits->push_back(larDigit);

            const HWIdentifier      digId           = dig->hardwareID();
            const int               digGain         = dig->gain();
            auto                    ofc_a           = ofcs->OFC_a(digId, digGain);
            auto                    ofc_b           = ofcs->OFC_b(digId, digGain);
            auto                      sig_shape     = shapes->Shape(digId, digGain);
            auto                      sig_shapeDer  = shapes->ShapeDer(digId, digGain);

            std::vector<double>       ofca, ofcb, shape, shapeDer;

            for (size_t k=0; k < ofc_a.size(); k++) ofca.push_back((double) ofc_a[k]);
            for (size_t k=0; k < ofc_b.size(); k++) ofcb.push_back((double) ofc_b[k]);
            for (size_t k=0; k < sig_shape.size(); k++) shape.push_back((double) sig_shape[k]);
            for (size_t k=0; k < sig_shapeDer.size(); k++) shapeDer.push_back((double) sig_shapeDer[k]);

            const auto&         adc2mev = adc2MeVs->ADC2MEV(digId, digGain);

            // Cell / readout data
            c711_channelEnergy->push_back(channelEnergyInClusterMap[k]);
            c711_channelTime->push_back(channelTimeInClusterMap[k]);
            c711_channelEffectiveSigma->push_back(channelEffectiveSigmaClusterMap[k]);
            c711_channelNoise->push_back(channelNoiseClusterMap[k]);
            if (!pr_noBadCells) c711_channelBad->push_back(badChannelMap[k]);
            // Calibration constants
            c711_channelDSPThreshold->push_back(run2DSPThresh->tQThr(digId));
            c711_channelOFCa->push_back(ofca);
            c711_channelOFCb->push_back(ofcb);
            c711_channelShape->push_back(shape);
            c711_channelShapeDer->push_back(shapeDer);
            c711_channelOFCTimeOffset->push_back(ofcs->timeOffset(digId,digGain));
            c711_channelADC2MEV0->push_back(adc2mev[0]);
            c711_channelADC2MEV1->push_back(adc2mev[1]);

            // Channel info
            int barrelEc = m_onlineLArID->barrel_ec(channelID);
            int posNeg = m_onlineLArID->pos_neg(channelID);
            int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
            int slot = m_onlineLArID->slot(channelID);
            int chn = m_onlineLArID->channel(channelID);      
            std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
            c711_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

            // important indexes
            c711_clusterChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
            c711_clusterIndex_chLvl->push_back(clusIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
            c711_channelHashMap->push_back(index); // online id hash for channel
            c711_channelChannelIdMap->push_back(channelID.get_identifier32().get_compact());

            ATH_MSG_DEBUG ("(7x11 ROI) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  channelID);
            ATH_MSG_DEBUG ("(7x11 ROI) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << " HwId.get_compact() " << channelID.get_compact());
            ATH_MSG_DEBUG ("(7x11 ROI) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  <<  " HwId.get_identifier32().get_compact() " << channelID.get_identifier32().get_compact());

            ATH_MSG_DEBUG("(7x11 ROI) LAr OFC timeOffset: "<< ofcs->timeOffset(digId, digGain)  <<", dig->hardwareID: " << digId << ", dig->channelID: "<< channelID);
            ATH_MSG_DEBUG("\tOFCa ("<< ofca.size()<<"): ["<< ofca[0] <<", " << ofca[1] <<", " << ofca[2]<<", "  << ofca[3] <<"]");
            ATH_MSG_DEBUG("\tOFCb ("<< ofcb.size()<<"): ["<< ofcb[0] <<", " << ofcb[1] <<", " << ofcb[2]<<", "  << ofcb[3] <<"]");
            ATH_MSG_DEBUG("\tADC2MeV ("<< adc2mev.size() <<"): ["<< adc2mev[0] <<", " << adc2mev[1] <<"]");
            
            if (pr_printCellsClus){
              ATH_MSG_INFO ("(7x11 ROI) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
            }
          } //end-if caloHashMap matches
          // else{
          //   if (caloHashMap[k] > m_onlineLArID->channelHashMax() ) ATH_MSG_ERROR ("###### HASH INDEX OUT OF BONDS FOR LAR !...");
          // }
        } //end for loop over caloHashMaps
      } // end-if clustBits Test
    } //end loop over LAr digits container
  } //end-if larClustBits is not empty


    // ========= TileCal LG+HG==========
  if (tileClusteredDigitsLG.any() || tileClusteredDigitsHG.any()) {
    ATH_MSG_INFO ("(7x11 ROI) Dumping tile PMT's Digits...");
    // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
    ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));

    for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
      if (TileDigColl->empty() ) continue;

      HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
      // uint32_t rodBCID = TileDigColl->getRODBCID();

      for (const TileDigits* dig : *TileDigColl){
        HWIdentifier adc_id = dig->adc_HWID();

        IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
        // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
        
        size_t index = (size_t) (adcIdHash);

        if (index <= m_tileHWID->adc_hash_max()){ //safety verification
          if (tileClusteredDigitsLG.test(index) || tileClusteredDigitsHG.test(index)){ //if this channel index is inside cluster map of cells
            // Im aware this may not be the most optimized way to dump the containers info, but the solution below
            // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
            // because the order here is very important to keep the data links.
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
              if (channelHwidInClusterMap[k] == adc_id){ //find the index from pmt index to proceed.
                // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << adc_id );
                // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                
                // fake mask to tile calib. constants (temporary !!!!) it is done this way to maintain order to the data in the ntuple.
                std::vector<double> maskOfc{-999.0,-999.0,-999.0,-999.0};

                // Digits     
                // c_tileSamples->push_back(dig->samples()); //temporarily maintained
                c711_channelDigits->push_back(dig->samples());

                // Channel / readout data
                c711_channelEnergy->push_back(channelEnergyInClusterMap[k]);
                c711_channelTime->push_back(channelTimeInClusterMap[k]);
                c711_channelEffectiveSigma->push_back(channelEffectiveSigmaClusterMap[k]);
                c711_channelNoise->push_back(channelNoiseClusterMap[k]);
                if (!pr_noBadCells) c711_channelBad->push_back(badChannelMap[k]);
                // Calibration constants
                c711_channelDSPThreshold->push_back(-999.0); // masked values
                c711_channelOFCa->push_back(maskOfc);
                c711_channelOFCb->push_back(maskOfc);
                c711_channelShape->push_back(maskOfc);
                c711_channelShapeDer->push_back(maskOfc);
                c711_channelOFCTimeOffset->push_back(-999.0);
                c711_channelADC2MEV0->push_back(-999.0);
                c711_channelADC2MEV1->push_back(-999.0);

                //channelInfo
                int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                int partition = ros - 1; //partition (0-3)
                int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                c711_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                // important indexes
                c711_clusterChannelIndex->push_back(channelIndexMap[k]);
                c711_clusterIndex_chLvl->push_back(clusIndex); // what roi this ch belongs
                c711_channelHashMap->push_back(index); // online id hash for channel
                // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                c711_channelChannelIdMap->push_back(adc_id.get_identifier32().get_compact());

                ATH_MSG_DEBUG ("(7x11 ROI) In DumpTile Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  adc_id);
                ATH_MSG_DEBUG ("(7x11 ROI) In DumpTile Digits: ReadOutIndex "<< channelIndexMap[k]  << " HwId.get_compact() " << adc_id.get_compact());
                ATH_MSG_DEBUG ("(7x11 ROI) In DumpTile Digits: ReadOutIndex "<< channelIndexMap[k]  <<  " HwId.get_identifier32().get_compact() " << adc_id.get_identifier32().get_compact());
                 
                
                if (pr_printCellsClus){ //option for debugging
                  ATH_MSG_INFO ("(7x11 ROI) In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   
                }
                // readOutIndex++;
                // cellIndex++;
              } // end if-pmt index comparison
              // if (caloHashMap[k] > m_tileHWID->adc_hash_max()){
              //   ATH_MSG_ERROR (" ###### HASH INDEX OUT OF BONDS FOR TILE !...");
              // } //end else
            }// end loop over pmt-indexes
          } // end if digit index exist on clusteredDigits
        } //end if hash exists
      } // end loop TileDigitsCollection
    } //end loop TileDigitsContainer
  } // end DumpTileDigits

  // ##############################
  //  RAW CHANNEL INFO DUMP (7x11 ROI)
  // ##############################

  // ========= LAr ==========
  if (larClusteredDigits.any()) {
    ATH_MSG_INFO ("(7x11 ROI) Dumping LAr Raw Channel...");
    ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

    for (const LArRawChannel& LArChannel : *LArRawCnt){
      // for (const TileRawChannel* TileChannel : *TileChannelColl){

      HWIdentifier channelID = LArChannel.channelID();
      IdentifierHash chHwidHash = m_onlineLArID->channel_Hash(channelID);
      size_t index = (size_t) (chHwidHash);

      if (larClusteredDigits.test(index)){ //if this channel index is inside 
        for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loop over the vector of hashIndex, and verify if the cell index match.
          if (channelHwidInClusterMap[k] == channelID){
            // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == channelID " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
            
            //RawCh info
            int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
            int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
            uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
            int provenance       = (int) LArChannel.provenance(); // its uint16_t
            float rawEnergyConv  = (float) (rawEnergy); 
            float rawTimeConv    = (float) (rawTime);
            float rawQualityConv = (float) (rawQuality);

            // Channel info
            int barrelEc = m_onlineLArID->barrel_ec(channelID);
            int posNeg = m_onlineLArID->pos_neg(channelID);
            int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
            int slot = m_onlineLArID->slot(channelID);
            int chn = m_onlineLArID->channel(channelID);      
            std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
            c711_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

            if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
              ATH_MSG_ERROR (" ###### (7x11 ROI) LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
            }
            c711_rawChannelAmplitude->push_back(rawEnergyConv);
            c711_rawChannelTime->push_back(rawTimeConv);
            c711_rawChannelQuality->push_back(rawQualityConv);
            c711_rawChannelPedProv->push_back(provenance);
            c711_rawChannelDSPThreshold->push_back(run2DSPThresh->tQThr(channelID));

            // important indexes
            c711_clusterRawChannelIndex->push_back(channelIndexMap[k]);
            c711_clusterIndex_rawChLvl->push_back(clusIndex); // what roi this ch belongs
            c711_rawChannelIdMap->push_back(channelID.get_identifier32().get_compact());

            if (pr_printCellsClus){ //optional to help debugging

              HWIdentifier hardwareID = LArChannel.hardwareID();
              HWIdentifier identifyID = LArChannel.identify();
              // uint16_t     provenance = LArChannel.provenance();

              ATH_MSG_INFO ("(7x11 ROI) In DumpLAr Raw "<< channelIndexMap[k] <<": hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv);
            } // end-if optional cell print
          } // end-if hwid match rawChID
        } // end loop over HWIDClusterMap
      } // end-if clust.test
    } //end loop LArRawChannelContainer
  } // end-if clustBits have Cells


  // ========= TileCal LG+HG ==========
  if (tileClusteredDigitsLG.any() || tileClusteredDigitsHG.any()) {
    ATH_MSG_INFO ("(7x11 ROI) Dumping tile PMT's Raw Channel...");
    ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

    for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
      for (const TileRawChannel* TileChannel : *TileChannelColl){

        HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
        IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
        size_t index = (size_t) (chHwidHash);

        if (tileClusteredDigitsLG.test(index) || tileClusteredDigitsHG.test(index)){ //if this channel index is inside the bitmap of cells (LG+HG maps)
          for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
            if (channelHwidInClusterMap[k] == rawAdcHwid){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == rawAdcHwid " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
              
              // Channel info
              int tileAdc         = m_tileHWID->adc(rawAdcHwid);
              int tileCh          = m_tileHWID->channel(rawAdcHwid);
              int drawer          = m_tileHWID->drawer(rawAdcHwid);
              int ros             = m_tileHWID->ros(rawAdcHwid);
              int partition       = ros - 1;
              std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
              c711_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              //RawCh info
              float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
              float rawTime       = TileChannel->time();    // time relative to triggering bunch
              // quality is a number in [0,1] obtained by integrating the parent
              // distribution for the "goodness" from the DSP.  In hypothesis testing
              // terms, it is the significance of the hypothisis that the input
              // to the DSP matches the signal-model used to tune the DSP.                  
              float rawQuality    = TileChannel->quality();// quality of the sampling distribution
              float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
              c711_rawChannelAmplitude->push_back(rawAmplitude);
              c711_rawChannelTime->push_back(rawTime);
              c711_rawChannelPedProv->push_back(rawPedestal);
              c711_rawChannelQuality->push_back(rawQuality);
              // c711_rawChannelDSPThreshold->push_back(run2DSPThresh->tQThr(rawAdcHwid));
              c711_rawChannelDSPThreshold->push_back(-999); //masked value

              // important indexes
              c711_clusterRawChannelIndex->push_back(channelIndexMap[k]);
              c711_clusterIndex_rawChLvl->push_back(clusIndex); // what roi this ch belongs
              c711_rawChannelIdMap->push_back(rawAdcHwid.get_identifier32().get_compact());

              if (pr_printCellsClus){ //optional to help debugging
                int tileIndex, tilePmt;

                Identifier rawCellId = TileChannel->cell_ID();
                Identifier rawPmtId = TileChannel->pmt_ID();
                Identifier rawAdcId = TileChannel->adc_ID();

                TileChannel->cell_ID_index(tileIndex,tilePmt);

                ATH_MSG_INFO ("(7x11 ROI) In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
              } // end-if optional cell print
            } // end-if hwid match rawChID
          } // end loop over HWIDClusterMap
        } // end-if clust.test
      } //end loop TileRawChannelCollection
    } //end loop TileRawChannelContainer
  } // end-if clustBits have Cells
  
  larClusteredDigits.reset();
  tileClusteredDigitsLG.reset();
  tileClusteredDigitsHG.reset();

  caloHashMap.clear();

  channelHwidInClusterMap.clear();
  cellIndexMap.clear();
  channelIndexMap.clear();
  channelEnergyInClusterMap.clear();
  channelTimeInClusterMap.clear();
  channelEtaInClusterMap.clear();
  channelPhiInClusterMap.clear();
  // cellGranularityEtaInClusterMap.clear();
  // cellGranularityPhiInClusterMap.clear();
  // channelCaloRegionMap.clear();
  if (!pr_noBadCells) badChannelMap.clear();
  // cellClusDeltaEta.clear();
  // cellClusDeltaPhi.clear();
  
  ATH_MSG_DEBUG ("  (7x11 ROI) RawChannel: "<< c711_rawChannelAmplitude->size() <<" channels dumped, from " << c711_cellEta->size() <<" cluster cells. ");
  ATH_MSG_DEBUG ("  (7x11 ROI) Digits: "<< c711_channelDigits->size() <<" channels dumped, out of " << c711_rawChannelAmplitude->size() <<" raw channel cluster channels. ");
  if (c711_channelDigits->size() == c711_rawChannelAmplitude->size()){
    ATH_MSG_DEBUG ("  (7x11 ROI) ALL Digits from the cluster were dumped successfully!");}
  else{
    ATH_MSG_DEBUG ("  (7x11 ROI) The digits from "<< (c711_rawChannelAmplitude->size() - c711_channelDigits->size()) <<" channels are missing!");}


  return StatusCode::SUCCESS;
}


/// OLDER CLUSTER DUMPER CODE SNIPPET ( TO BE REMOVED SOON )
StatusCode EventReaderAlg::dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling ){
  //Get cable map via read conditions handle
  // SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  // const LArOnOffIdMapping* larCabling=*larCablingHdl;
  const LArDigitContainer* LarDigCnt = nullptr;
  const TileDigitsContainer* TileDigCnt = nullptr;
  const TileRawChannelContainer* TileRawCnt = nullptr;
  const LArRawChannelContainer* LArRawCnt = nullptr;

  int clusterIndex = 0; //index of cluster in the same event.

  // calorimeter level
  std::bitset<200000>       larClusteredDigits;
  std::bitset<65536>        tileClusteredDigits;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInClusterMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  std::vector<double>       cellClusDeltaEta;//unused
  std::vector<double>       cellClusDeltaPhi; //unused
  std::vector<double>       cellGranularityEtaInClusterMap; //unused
  std::vector<double>       cellGranularityPhiInClusterMap; //unused
  // channel level
  std::vector<double>       channelEnergyInClusterMap;
  std::vector<double>       channelTimeInClusterMap;
  std::vector<double>       channelEtaInClusterMap;
  std::vector<double>       channelPhiInClusterMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;
  
  //*****************************************
  
  ATH_CHECK (evtStore()->retrieve ( cls, clsName )); //reading from an ESD file.
  for (const xAOD::CaloCluster* cl : *cls) {

    double  clusEta           = cl->eta();
    double  clusPhi           = cl->phi();
    double  clusEt            = cl->et();
    double  clusPt            = cl->pt();
    double  clusTime          = cl->time();
    // double  clusCellSumE_Eta  = 0.0;
    // double  clusCellSumE_Phi  = 0.0;
    // double  clusCellSumE      = 0.0;
    // double  clusEtaBaryc      = 0.0;
    // double  clusPhiBaryc      = 0.0;
    // double cellClusDistPhi = 0.0;    
    
    ATH_MSG_DEBUG ("Cluster: "<< clusterIndex <<", numberCells: " << cl->numberCells() << ", e = " << cl->et() << " , pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());

    // loop over cells in cluster
    auto itrCells = cl->cell_begin();       
    auto itrCellsEnd = cl->cell_end(); 
    int cellNum = 0; //cell number just for printing out
    int cellIndex = 0; //cell index inside cluster (ordered)
    // int readOutIndex = 0; // channel index inside cluster

      for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){ 
      
        //loop over the cells in a cluster and do stuff...
        const CaloCell* cell = (*itCells);
 
        if (cell) { // check for empty clusters
          Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

          int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

          double  eneCell   = cell->energy();
          double  timeCell  = cell->time();
          double  etaCell   = cell->eta();
          double  phiCell   = cell->phi();          
          int     gainCell  = cell->gain(); // CaloGain type
          // int     layerCell = cell->caloDDE()->getLayer(); //
          int     layerCell = m_calocell_id->calo_sample(cell->ID());
          bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
          bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
          bool    isLAr     = cell->caloDDE()->is_lar_em();
          bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
          bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
          double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
          double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity

          // get cluster barycenter:
          // clusCellSumE      = clusCellSumE + fabs(eneCell);
          // clusCellSumE_Eta  = clusCellSumE_Eta + ( fabs(eneCell) * etaCell);
          // clusCellSumE_Phi  = clusCellSumE_Phi + ( fabs(eneCell) * phiCell );
          
          // double cellClusterDEta = etaCell

          // cell->caloDDE()->print(); //print out eta, phi and r.
              
          IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
          IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
          IdentifierHash chidHash, adcidHash;
          HWIdentifier chhwid, adchwid;
          size_t index, indexPmt1, indexPmt2;

          // ========= TileCal ==========
          if (isTile){ //Tile
            const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
            // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

            Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

            // main TileCell readout properties:
            int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
            int gain2   = tCell->gain2();
            // int qual1   = tCell->qual1(); // quality factor (chi2)
            // int qual2   = tCell->qual2();
            bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
            bool badch2 = tCell->badch2();
            bool noch1  = (gain1<0 || gain1>1); // there is a ch?
            bool noch2  = (gain2<0 || gain2>1);
            float time1 = tCell->time1(); // reco time in both chs (OF2)
            float time2 = tCell->time2();
            float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
            float ene2  = tCell->ene2();

            // verify if it's a valid ch pmt 1
            if ( !noch1 && (!badch1 || !pr_noBadCells ) ){
              // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
              HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
              IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
              indexPmt1 = (size_t) (hashpmt1);

              int adc1     = m_tileHWID->adc(adchwid_pmt1);
              int channel1 = m_tileHWID->channel(adchwid_pmt1);
              int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
              int ros1     = m_tileHWID->ros(adchwid_pmt1);

              if (pr_printCellsClus){ //optional to help debugging
                ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
              }
              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt1)) {
                ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1);
                continue;
                }
              else { //If there wasn't an error, 
                tileClusteredDigits.set(indexPmt1); //fill the map.
                caloHashMap.push_back(indexPmt1);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.1); // adds 0.1 to the cell index, to indicate this is the PMT 1
                channelHwidInClusterMap.push_back(adchwid_pmt1);
                channelTimeInClusterMap.push_back(time1);
                channelEnergyInClusterMap.push_back(ene1);
                channelCaloRegionMap.push_back(caloRegionIndex);
                if (!pr_noBadCells) badChannelMap.push_back(badch1);

                // readOutIndex++;
              }
              
            } //end-if its a valid pmt1
            // else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
            // else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 channel!");
            // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt1 channel skipped for no known reason.");
            
            // verify if it's a valid ch pmt 2
            if ( !noch2 && (!badch2 || !pr_noBadCells) ){
              HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
              IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);            
              indexPmt2 = (size_t) (hashpmt2);

              int adc2     = m_tileHWID->adc(adchwid_pmt2);
              int channel2 = m_tileHWID->channel(adchwid_pmt2);
              int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
              int ros2     = m_tileHWID->ros(adchwid_pmt2);

              if (pr_printCellsClus){ //optional to help debugging
                ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
              }              
              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt2)) {
                ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2);
                // continue;
                }
              else {//If there wasn't an error,
                tileClusteredDigits.set(indexPmt2); //fill the map.
                caloHashMap.push_back(indexPmt2);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.2); // adds 0.2 to the cell index, to indicate this is the PMT2
                channelHwidInClusterMap.push_back(adchwid_pmt2);
                channelTimeInClusterMap.push_back(time2);
                channelEnergyInClusterMap.push_back(ene2);
                channelCaloRegionMap.push_back(caloRegionIndex);
                if (!pr_noBadCells) badChannelMap.push_back(badch2);

                // readOutIndex++;
              }              
            }
            if (noch1)   ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
            if (noch2)   ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
            if ( (badch1 && badch2) && pr_noBadCells )  {
              ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and pmt2 channel! Skipping cell.");
              continue;
            }
            if ( (badch1 && noch2) && pr_noBadCells ) {
              ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and has no valid pmt2 channel! Skipping cell.");
              continue;
            }
            // else if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
            // else if (badch1 && badch2)  {
            //   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt2 channel!");
            //   continue;
            // }
            // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt2 channel skipped for no known reason.");
            // } //end if id
          } // end if isTile

          // ========= LAr ==========
          else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
            chhwid = larCabling->createSignalChannelID(cellId);
            // std::string hwid_lar = chid.getString();
            // ATH_MSG_INFO (typeid(chhwid).name());
            chidHash =  m_onlineLArID->channel_Hash(chhwid);
            index = (size_t) (chidHash);

            if (larClusteredDigits.test(index)) {
              ATH_MSG_ERROR (" ##### Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of index: " << index << " and hwid: " << chhwid);
              continue;
              }
            else if (badCell && pr_noBadCells){
              ATH_MSG_DEBUG (" (LAr) Cell "<< cellNum <<" in cluster is a bad LAr channel! Skipping the cell.");
            continue;
            }
            else{
              larClusteredDigits.set(index);
              caloHashMap.push_back(index);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.0);
              channelHwidInClusterMap.push_back(chhwid);
              channelTimeInClusterMap.push_back(timeCell);
              channelEnergyInClusterMap.push_back(eneCell);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badCell);
            }
            // readOutIndex++;
            if (pr_printCellsClus){ //optional to help debugging
              ATH_MSG_INFO (" in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<") ID: " << cellId << "). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):");
            }

            // if 
          } //end else LAr

          else {
            ATH_MSG_ERROR (" ####### ERROR ! No CaloCell region was found!");
            continue;
            }

          // ##############################
          //  CELL INFO DUMP (CLUSTER)
          // ##############################
          // double cluster_dphi = fabs(clusPhi-phiCell)  < pi ? (clusPhi-phiCell) : twopi - fabs(clusPhi-phiCell); //wrap correction
          double cluster_dphi = (clusPhi-phiCell)  < -pi ? twopi + (clusPhi-phiCell) : ( clusPhi-phiCell  > pi ? (clusPhi-phiCell) - twopi : clusPhi-phiCell );

          // if (fabs(clusPhi-phiCell)  < pi) ATH_MSG_INFO ("abs deltaPhi = " << fabs(clusPhi-phiCell));
          // else ATH_MSG_INFO ("(cluster) deltaPhi = " << (clusPhi-phiCell) <<". Corrected value is: " << cluster_dphi );

          c_clusterIndex_cellLvl->push_back(clusterIndex);
          c_clusterCellIndex->push_back(cellIndex);
          c_cellGain->push_back(gainCell);
          c_cellLayer->push_back(layerCell);
          c_cellEta->push_back(etaCell);
          c_cellPhi->push_back(phiCell);
          c_cellDEta->push_back(detaCell);
          c_cellDPhi->push_back(dphiCell);
          c_cellToClusterDEta->push_back(clusEta - etaCell);
          c_cellToClusterDPhi->push_back(cluster_dphi);
          c_cellRegion->push_back(caloRegionIndex);
          

          // ##############################
          cellIndex++;
          cellNum++;
        } //end if-cell is empty verification
      } // end loop at cells inside cluster

    // ##############################
    //  CLUSTER INFO DUMP
    // ##############################
    // clusEtaBaryc      = clusCellSumE_Eta / clusCellSumE;
    // clusPhiBaryc      = fixPhi( clusCellSumE_Phi / clusCellSumE ); // verify if phi value is inside -pi,pi
    // ATH_MSG_INFO ("clusEtaBaryc " << clusEtaBaryc << " clusPhiBaryc " << clusPhiBaryc);

    c_clusterIndex->push_back(clusterIndex);
    c_clusterEnergy->push_back(clusEt);
    c_clusterTime->push_back(clusTime);
    c_clusterEta->push_back(clusEta);
    c_clusterPhi->push_back(clusPhi);
    c_clusterPt->push_back(clusPt);
    // c_clusterEta_calc->push_back(clusEtaBaryc);
    // c_clusterPhi_calc->push_back(clusPhiBaryc);

    // ##############################
    //  DIGITS CHANNEL INFO DUMP (CLUSTER)
    // ##############################
    // Loop over ALL channels in LAr Digit Container.
    // Dump only the channels inside the previous clusters

    // ========= LAr ==========
    if (larClusteredDigits.any()){
      ATH_MSG_INFO (" (Cluster) Dumping LAr Digits...");
      ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

      for (const LArDigit* dig : *LarDigCnt) {
        HWIdentifier channelID = dig->channelID();
        IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
        size_t index = (size_t) (idHash);

        if (larClusteredDigits.test(index)) {
          for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){
            if (channelHwidInClusterMap[k]==channelID){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
              // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
              
              // digits
              std::vector<short> larDigitShort = dig->samples();
              std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
              // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
              // c_larSamples->push_back(dig->samples());
              c_channelDigits->push_back(larDigit);

              // Cell / readout data
              c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
              c_channelTime->push_back(channelTimeInClusterMap[k]);
              if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);

              // DataDescriptorElements

              // Channel info
              int barrelEc = m_onlineLArID->barrel_ec(channelID);
              int posNeg = m_onlineLArID->pos_neg(channelID);
              int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
              int slot = m_onlineLArID->slot(channelID);
              int chn = m_onlineLArID->channel(channelID);      
              std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
              c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              // important indexes
              c_clusterChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
              c_clusterIndex_chLvl->push_back(clusterIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
              c_channelHashMap->push_back(index); // online id hash for channel
              c_channelChannelIdMap->push_back(channelID.get_identifier32().get_compact());

              ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  channelID);
              ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << " HwId.get_compact() " << channelID.get_compact());
              ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  <<  " HwId.get_identifier32().get_compact() " << channelID.get_identifier32().get_compact());

              if (pr_printCellsClus){
                ATH_MSG_INFO ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
              }
            } //end-if caloHashMap matches
            // else{
            //   if (caloHashMap[k] > m_onlineLArID->channelHashMax() ) ATH_MSG_ERROR ("###### HASH INDEX OUT OF BONDS FOR LAR !...");
            // }
          } //end for loop over caloHashMaps
        } // end-if clustBits Test
      } //end loop over LAr digits container
    } //end-if larClustBits is not empty


      // ========= TileCal ==========
    if (tileClusteredDigits.any()) {
      ATH_MSG_INFO ("(Cluster) Dumping tile PMT's Digits...");
      // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
      ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));
  
      for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
        if (TileDigColl->empty() ) continue;

        HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
        // uint32_t rodBCID = TileDigColl->getRODBCID();

        for (const TileDigits* dig : *TileDigColl){
          HWIdentifier adc_id = dig->adc_HWID();

          IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
          // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
          
          size_t index = (size_t) (adcIdHash);

          if (index <= m_tileHWID->adc_hash_max()){ //safety verification
            if (tileClusteredDigits.test(index)){ //if this channel index is inside cluster map of cells
              // Im aware this may not be the most optimized way to dump the containers info, but the solution below
              // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
              // because the order here is very important to keep the data links.
              for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == adc_id){ //find the index from pmt index to proceed.
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << adc_id );
                  // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                  Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                  
                  // Digits     
                  // c_tileSamples->push_back(dig->samples()); //temporarily maintained
                  c_channelDigits->push_back(dig->samples());

                  // Channel / readout data
                  c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
                  c_channelTime->push_back(channelTimeInClusterMap[k]);
                  if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);

                  //channelInfo
                  int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                  int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                  int partition = ros - 1; //partition (0-3)
                  int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                  int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                  std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                  c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  // important indexes
                  c_clusterChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_chLvl->push_back(clusterIndex); // what roi this ch belongs
                  c_channelHashMap->push_back(index); // online id hash for channel
                  c_channelChannelIdMap->push_back(adc_id.get_identifier32().get_compact());
                  // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                  // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING

                  ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId " <<  adc_id);
                  ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << " HwId.get_compact() " << adc_id.get_compact());
                  ATH_MSG_DEBUG ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  <<  " HwId.get_identifier32().get_compact() " << adc_id.get_identifier32().get_compact());
                  
                  if (pr_printCellsClus){ //option for debugging
                    ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   
                  }
                  // readOutIndex++;
                  // cellIndex++;
                } // end if-pmt index comparison
                // if (caloHashMap[k] > m_tileHWID->adc_hash_max()){
                //   ATH_MSG_ERROR (" ###### HASH INDEX OUT OF BONDS FOR TILE !...");
                // } //end else
              }// end loop over pmt-indexes
            } // end if digit index exist on clusteredDigits
          } //end if hash exists
        } // end loop TileDigitsCollection
      } //end loop TileDigitsContainer
    } // end DumpTileDigits

    // ##############################
    //  RAW CHANNEL INFO DUMP (CLUSTER)
    // ##############################

    // ========= LAr ==========
    if (larClusteredDigits.any()) {
      ATH_MSG_INFO ("(Cluster) Dumping LAr Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

      for (const LArRawChannel& LArChannel : *LArRawCnt){
        // for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier channelID = LArChannel.channelID();
          IdentifierHash chHwidHash = m_onlineLArID->channel_Hash(channelID);
          size_t index = (size_t) (chHwidHash);

          if (larClusteredDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == channelID){
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == channelID " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
                  
                  //RawCh info
                  int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
                  int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
                  uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
                  int provenance       = (int) LArChannel.provenance(); // its uint16_t
                  float rawEnergyConv  = (float) (rawEnergy); 
                  float rawTimeConv    = (float) (rawTime);
                  float rawQualityConv = (float) (rawQuality);

                  // Channel info
                  int barrelEc = m_onlineLArID->barrel_ec(channelID);
                  int posNeg = m_onlineLArID->pos_neg(channelID);
                  int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
                  int slot = m_onlineLArID->slot(channelID);
                  int chn = m_onlineLArID->channel(channelID);      
                  std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
                  c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  

                  if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
                    ATH_MSG_ERROR (" ###### LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
                  }
                  c_rawChannelAmplitude->push_back(rawEnergyConv);
                  c_rawChannelTime->push_back(rawTimeConv);
                  c_rawChannelQuality->push_back(rawQualityConv);
                  c_rawChannelPedProv->push_back(provenance);

                  // important indexes
                  c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_rawChLvl->push_back(clusterIndex); // what roi this ch belongs

                  if (pr_printCellsClus){ //optional to help debugging

                    HWIdentifier hardwareID = LArChannel.hardwareID();
                    HWIdentifier identifyID = LArChannel.identify();
                    // uint16_t     provenance = LArChannel.provenance();

                    ATH_MSG_INFO ("In DumpLAr Raw "<< channelIndexMap[k] <<": hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        // } //end loop LArRawChannelCollection
      } //end loop LArRawChannelContainer
    } // end-if clustBits have Cells


    // ========= TileCal ==========
    if (tileClusteredDigits.any()) {
      ATH_MSG_INFO ("Dumping tile PMT's Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

      for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
        for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
          IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
          size_t index = (size_t) (chHwidHash);


          if (tileClusteredDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == rawAdcHwid){
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == rawAdcHwid " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
                  
                  // Channel info
                  int tileAdc         = m_tileHWID->adc(rawAdcHwid);
                  int tileCh          = m_tileHWID->channel(rawAdcHwid);
                  int drawer          = m_tileHWID->drawer(rawAdcHwid);
                  int ros             = m_tileHWID->ros(rawAdcHwid);
                  int partition       = ros - 1;
                  std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
                  c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  //RawCh info
                  float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
                  float rawTime       = TileChannel->time();    // time relative to triggering bunch
                  // quality is a number in [0,1] obtained by integrating the parent
                  // distribution for the "goodness" from the DSP.  In hypothesis testing
                  // terms, it is the significance of the hypothisis that the input
                  // to the DSP matches the signal-model used to tune the DSP.                  
                  float rawQuality    = TileChannel->quality();// quality of the sampling distribution
                  float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
                  c_rawChannelAmplitude->push_back(rawAmplitude);
                  c_rawChannelTime->push_back(rawTime);
                  c_rawChannelPedProv->push_back(rawPedestal);
                  c_rawChannelQuality->push_back(rawQuality);

                  // important indexes
                  c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_rawChLvl->push_back(clusterIndex); // what roi this ch belongs

                  if (pr_printCellsClus){ //optional to help debugging
                    int tileIndex, tilePmt;

                    Identifier rawCellId = TileChannel->cell_ID();
                    Identifier rawPmtId = TileChannel->pmt_ID();
                    Identifier rawAdcId = TileChannel->adc_ID();

                    TileChannel->cell_ID_index(tileIndex,tilePmt);

                    ATH_MSG_INFO ("In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        } //end loop TileRawChannelCollection
      } //end loop TileRawChannelContainer
    } // end-if clustBits have Cells
    
    clusterIndex++;

    larClusteredDigits.reset();
    tileClusteredDigits.reset();

    caloHashMap.clear();
  
    channelHwidInClusterMap.clear();
    cellIndexMap.clear();
    channelIndexMap.clear();
    channelEnergyInClusterMap.clear();
    channelTimeInClusterMap.clear();
    channelEtaInClusterMap.clear();
    channelPhiInClusterMap.clear();
    cellGranularityEtaInClusterMap.clear();
    cellGranularityPhiInClusterMap.clear();
    channelCaloRegionMap.clear();
    if (!pr_noBadCells) badChannelMap.clear();
    cellClusDeltaEta.clear();
    cellClusDeltaPhi.clear();
  } // end loop at clusters inside container

  ATH_MSG_DEBUG ("   RawChannel: "<< c_rawChannelAmplitude->size() <<" channels dumped, from " << c_cellEta->size() <<" cluster cells. ");
  ATH_MSG_DEBUG ("   Digits: "<< c_channelDigits->size() <<" channels dumped, out of " << c_rawChannelAmplitude->size() <<" raw channel cluster channels. ");
  if (c_channelDigits->size() == c_rawChannelAmplitude->size()){
    ATH_MSG_DEBUG ("   ALL Digits from the cluster were dumped successfully!");}
  else{
    ATH_MSG_DEBUG ("   The digits from "<< (c_rawChannelAmplitude->size() - c_channelDigits->size()) <<" channels are missing!");}

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::buildRingsJets(const xAOD::JetContainer *jets, const std::string& jetAlg){

  const CaloCellContainer*        CaloCnt      = nullptr;
  ATH_CHECK (evtStore()->retrieve (jets, jetAlg )); //get the jet container from file in evt store

  // ringer building definitions
  float window = 0.2;
  std::vector<int> nRings = {8, 64, 8, 8, 4, 4, 4}; // number of rings per layer (squared rings)
  std::vector<int> idxRings = {0, 8, 72, 80, 88, 92, 96}; // index for firt ring for each layer
  std::vector<double> m_etaWidth = { 0.025, 0.003125, 0.025, 0.05, 0.1, 0.1, 0.1 }; // eta granularity reference for each layer
  std::vector<double> m_phiWidth = { 0.098174770424681, 0.098174770424681, 0.024543692606170, 0.024543692606170,0.098174770424681, 0.098174770424681, 0.098174770424681 }; // phi granularity reference for each layer
  std::vector<float> m_tempRings(100,0.0);
  
  for (const xAOD::Jet* jet : *jets){

    ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo")); // point to all calo cell container
    double jetEta_center = jet->eta(); // eta coordinate of the jet centroid
    double jetPhi_center = jet->phi(); // phi coordinate of the jet centroid

    CaloCellList cells(CaloCnt);

    for ( const int layer : sampRegions) {
      cells.select(jet->eta(), jet->phi(), window, window, layer ); // selecting cells for a window
      int m_layerID = setLayerID(layer);

      for ( const CaloCell *cell : cells ) {
          int ringNumber(0);
          double  etaCell   = cell->eta();
          double  phiCell   = cell->phi();
        
          // Measure delta eta and delta phi to find out on which ring we are
          const double deltaEta = fabs(etaCell - jetEta_center)/m_etaWidth[m_layerID];
          const double deltaPhi = fabs(phiCell - jetPhi_center)/m_phiWidth[m_layerID];

          // ring index
          const double deltaGreater = std::max(std::abs(deltaEta), std::abs(deltaPhi));
          ringNumber = static_cast<unsigned int>(std::floor(deltaGreater + .5));
          int idxRing = ringNumber+idxRings[m_layerID];

          if ( ringNumber < nRings[m_layerID] ){
            m_tempRings.at(idxRing) += cell->energy(); 
            ATH_MSG_DEBUG("Add energy: " <<  cell->energy() << " to the Ring: "  << idxRing << " Layer: " << m_layerID << " Partial Energy for this Ring: " <<  m_tempRings[idxRing] ); 
          }
        } 
      }
      j_std_rings->push_back(m_tempRings); // Dump Standard Rings
    }
    
  return StatusCode::SUCCESS;
}

int EventReaderAlg::setLayerID(int samp){

// need to improve this in the near future
  int m_layerID(0);
  if (samp == CaloSampling::PreSamplerE){ 
    m_layerID = 0;
  }else if(samp == CaloSampling::PreSamplerB){
    m_layerID = 0;
  }else if(samp == CaloSampling::EMB1){
    m_layerID = 1;
  }else if(samp == CaloSampling::EME1){
    m_layerID = 1;
  }else if(samp == CaloSampling::EMB2){
    m_layerID = 2;
  }else if(samp == CaloSampling::EME2){
    m_layerID = 2;
  }else if(samp == CaloSampling::EMB3){
    m_layerID = 3;
  }else if(samp == CaloSampling::EME3){
    m_layerID = 3;
  }else if(samp == CaloSampling::HEC0){
    m_layerID = 4;
  }else if(samp == CaloSampling::TileBar0){
    m_layerID = 4;
  }else if(samp == CaloSampling::TileGap2){
    m_layerID = 4;
  }else if(samp == CaloSampling::TileExt0){
    m_layerID = 4;
  }else if(samp == CaloSampling::HEC1){
    m_layerID = 5;
  }else if(samp == CaloSampling::HEC2){
    m_layerID = 5;
  }else if(samp == CaloSampling::TileBar1){
    m_layerID = 5;
  }else if(samp == CaloSampling::TileExt1){
    m_layerID = 5;
  }else if(samp == CaloSampling::HEC3){
    m_layerID = 6;
  }else if(samp == CaloSampling::TileBar2){
    m_layerID = 6;
  }else if(samp == CaloSampling::TileGap1){
    m_layerID = 6;
  }else if(samp == CaloSampling::TileExt2){
    m_layerID = 6;
  }
  return m_layerID;
}

StatusCode EventReaderAlg::dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg, const LArOnOffIdMapping* larCabling ){

  const CaloCellContainer*        CaloCnt      = nullptr; // for loop inside 'AllCalo'
  const LArDigitContainer*        LarDigCnt    = nullptr;
  const TileDigitsContainer*      TileDigCnt   = nullptr;
  const TileRawChannelContainer*  TileRawCnt   = nullptr;
  const LArRawChannelContainer*   LArRawCnt    = nullptr;

  float r = pr_roiRadius; //jet ROI radius
  int jetRoiIndex = 0;  //index of jet in the same event.

  // calorimeter level
  std::bitset<200000>       larJetRoiDigits;
  std::bitset<65536>        tileJetRoiDigits;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInJetRoiMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  // std::vector<double>       cellClusDeltaEta;
  // std::vector<double>       cellClusDeltaPhi;
  std::vector<double>       cellGranularityEtaInJetRoiMap;
  std::vector<double>       cellGranularityPhiInJetRoiMap;
  // channel level
  std::vector<double>       channelEnergyInJetRoiMap;
  std::vector<double>       channelTimeInJetRoiMap;
  std::vector<double>       channelEtaInJetRoiMap;
  std::vector<double>       channelPhiInJetRoiMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;

  //*****************************************
  
  ATH_CHECK (evtStore()->retrieve (jets, jetAlg )); //get the jet container from file in evt store
  ATH_MSG_INFO("Number of jets: " <<  jets->size());
  
  for (const xAOD::Jet* jet : *jets){ //loop over jets in container

    double jetPt   = jet->pt();
    double jetEta  = jet->eta();
    double jetPhi  = jet->phi();

    ATH_MSG_INFO ("Jet:   pt: " << jet->pt() << " MeV" << ". Eta; Phi: " << jet->eta() << "; " << jet->phi());

    int cellNum = 0; //cell number just for printing out
    int cellIndex = 0; //cell index inside cluster (ordered)

    ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo")); // point to all calo cell container
    
    for (const CaloCell* cell : *CaloCnt ){ //loop over cells and verify if its inside jet ROI

      double  etaCell   = cell->eta();
      double  phiCell   = cell->phi();

      //checking if cell is in jet's ROI
      double jetRoi_deta = (jetEta-etaCell);
      double jetRoi_dphi = deltaPhi( jetPhi , phiCell );
      // double jetRoi_dR   = deltaR( jetRoi_deta, jetRoi_dphi);
      // bool  insideRoi = jetRoi_dR < r; // circle of radius r around the jet
      bool  insideRoi = (fabs(jetRoi_deta) < r) && (fabs(jetRoi_dphi) < r); // square window of 2*r

      if (insideRoi){
       
        Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

        int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

        double  eneCell   = cell->energy();
        double  timeCell  = cell->time();
                  
        int     gainCell  = cell->gain(); // CaloGain type
        // int     layerCell = cell->caloDDE()->getLayer(); //
        int     layerCell = m_calocell_id->calo_sample(cell->ID());
        bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
        bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
        bool    isLAr     = cell->caloDDE()->is_lar_em();
        bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
        bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
        double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
        double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity

        // ATH_MSG_INFO (" Cell ID " << cellId << "and Eta/Phi " << etaCell << "/" << phiCell << " from region " << caloRegionIndex << " is inside Jet radius r=" << r << " and Eta/Phi " << jetEta << "/" << jetPhi );

        IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
        IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
        IdentifierHash chidHash, adcidHash;
        HWIdentifier chhwid, adchwid;
        size_t index, indexPmt1, indexPmt2;

        // ========= TileCal ==========
        if (isTile){ //Tile
          const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
          // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

          Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

          // main TileCell readout properties:
          int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
          int gain2   = tCell->gain2();
          // int qual1   = tCell->qual1(); // quality factor (chi2)
          // int qual2   = tCell->qual2();
          bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
          bool badch2 = tCell->badch2();
          bool noch1  = (gain1<0 || gain1>1); // there is a ch?
          bool noch2  = (gain2<0 || gain2>1);
          float time1 = tCell->time1(); // reco time in both chs (OF2)
          float time2 = tCell->time2();
          float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
          float ene2  = tCell->ene2();

          // verify if it's a valid ch pmt 1
          if (!noch1 && (!badch1 || !pr_noBadCells ) ){
            // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
            HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
            IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
            indexPmt1 = (size_t) (hashpmt1);

            int adc1     = m_tileHWID->adc(adchwid_pmt1);
            int channel1 = m_tileHWID->channel(adchwid_pmt1);
            int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
            int ros1     = m_tileHWID->ros(adchwid_pmt1);

            if (pr_printCellsJet){ //optional to help debugging
              ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum << " (" << layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
            }
            // test if there is conflict in the map
            if (tileJetRoiDigits.test(indexPmt1)) {
              ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of Index: " << indexPmt1 << " and hwid: " << adchwid_pmt1);
              continue;
              }
            else { //If there wasn't an error, 
              tileJetRoiDigits.set(indexPmt1); //fill the map.
              caloHashMap.push_back(indexPmt1);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.1); // adds 0.1 to the cell index, to indicate this is the PMT 1
              channelHwidInJetRoiMap.push_back(adchwid_pmt1);
              channelTimeInJetRoiMap.push_back(time1);
              channelEnergyInJetRoiMap.push_back(ene1);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badch1);

              // readOutIndex++;
            }
          } //end-if its a valid pmt1
          // else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt1 channel!");
          // else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pmt1 channel!");
          // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI, had its pmt1 channel skipped for no known reason.");
          
          // verify if it's a valid ch pmt 2
          if ( (!noch2 && (!badch2 || !pr_noBadCells) ) ){
            HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));
            IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);
            indexPmt2 = (size_t) (hashpmt2);

            int adc2     = m_tileHWID->adc(adchwid_pmt2);
            int channel2 = m_tileHWID->channel(adchwid_pmt2);
            int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
            int ros2     = m_tileHWID->ros(adchwid_pmt2);

            if (pr_printCellsJet){ //optional to help debugging
              ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum << " (" << layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
            }              
            // test if there is conflict in the map (only for debugging reason)
            if (tileJetRoiDigits.test(indexPmt2)) {
              ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of Index: " << indexPmt2 << " and hwid: " << adchwid_pmt2);
              continue;
              }
            else {//If there wasn't an error,
              tileJetRoiDigits.set(indexPmt2); //fill the map.
              caloHashMap.push_back(indexPmt2);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.2); // adds 0.2 to the cell index, to indicate this is the PMT2
              channelHwidInJetRoiMap.push_back(adchwid_pmt2);
              channelTimeInJetRoiMap.push_back(time2);
              channelEnergyInJetRoiMap.push_back(ene2);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badch2);
              // readOutIndex++;
            }
            
          }
          if (noch1)   ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt1 channel!");
          if (noch2)   ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt2 channel!");
          if ( (badch1 && badch2) && pr_noBadCells )  {
            ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pmt2 channel! Skipping cell.");
            continue;
            }
          if ( (badch1 && noch2) && pr_noBadCells){
            ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pm1 and has no valid pmt2 channel! Skipping cell.");
            continue;
            }
          // else        ATH_MSG_DEBUG (" (tile) Cell "<< cellNum <<" in jet ROI, had its pmt2 channel skipped for no known reason.");
          // } //end if id
        } // end if isTile

        // ========= LAr ==========
        else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
          chhwid = larCabling->createSignalChannelID(cellId);
          // std::string hwid_lar = chid.getString();
          // ATH_MSG_DEBUG (typeid(chhwid).name());
          chidHash =  m_onlineLArID->channel_Hash(chhwid);
          index = (size_t) (chidHash);
          if (larJetRoiDigits.test(index)) {
            ATH_MSG_ERROR (" ##### Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of Index: " << index << " and hwid: " << chhwid);
            continue;
            }
          else if (badCell && pr_noBadCells){
            ATH_MSG_DEBUG (" (LAr) Cell "<< cellNum <<" in jet ROI is a bad LAr channel! Skipping the cell.");
            continue;
          }
          else{
            larJetRoiDigits.set(index);
            caloHashMap.push_back(index);
            cellIndexMap.push_back(cellIndex);
            channelIndexMap.push_back(cellIndex + 0.0);
            channelHwidInJetRoiMap.push_back(chhwid);
            channelTimeInJetRoiMap.push_back(timeCell);
            channelEnergyInJetRoiMap.push_back(eneCell);
            channelCaloRegionMap.push_back(caloRegionIndex);
            if (!pr_noBadCells) badChannelMap.push_back(badCell);
          }
          // readOutIndex++;
          if (pr_printCellsJet){ //optional to help debugging
            ATH_MSG_INFO (" in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<"). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):");
          }
        } //end else LAr

        else {
          ATH_MSG_ERROR (" ####### ERROR ! No CaloCell region was found!");
          continue;
        }

        // ##############################
        //  CELL INFO DUMP (JET ROI)
        // ##############################
        j_jetRoiIndex_cellLvl->push_back(jetRoiIndex);
        j_jetRoiCellIndex->push_back(cellIndex);
        j_cellGain->push_back(gainCell);
        j_cellLayer->push_back(layerCell);
        j_cellRegion->push_back(caloRegionIndex);
        //j_cellEnergy->push_back(eneCell)
        j_cellEta->push_back(etaCell);
        j_cellPhi->push_back(phiCell);
        j_cellDEta->push_back(detaCell);
        j_cellDPhi->push_back(dphiCell);
        j_cellToJetDEta->push_back(jetRoi_deta);
        j_cellToJetDPhi->push_back(jetRoi_dphi);


        // ##############################
        cellIndex++;
        cellNum++;          
        // } // end-if verification of phi region
      } //  end-if verification of jet ROI region
    } // end loop over cells 'AllCalo' container

    // ##############################
    //  JET INFO DUMP
    // ##############################
    j_jetIndex->push_back(jetRoiIndex);
    j_jetEta->push_back(jetEta);
    j_jetPhi->push_back(jetPhi);
    j_jetPt->push_back(jetPt);

    // ##############################
    //  DIGITS CHANNEL INFO DUMP (JET)
    // ##############################
    // Loop over ALL channels in LAr Digit Container.
    // Dump only the channels inside the previous clusters

    // ========= LAr ==========
    if (larJetRoiDigits.any()){
      ATH_MSG_INFO ("(Jet ROI) Dumping LAr Digits...");
      ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

      for (const LArDigit* dig : *LarDigCnt) {
        HWIdentifier channelID = dig->channelID();
        IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
        size_t index = (size_t) (idHash);

        if (larJetRoiDigits.test(index)) {
          for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){
            if (channelHwidInJetRoiMap[k]==channelID){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
              // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
              
            
              // digits
              std::vector<short> larDigitShort = dig->samples();
              std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
              // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
              // j_larSamples->push_back(dig->samples());
              j_channelDigits->push_back(larDigit);

              // Cell / readout data
              j_channelEnergy->push_back(channelEnergyInJetRoiMap[k]);
              j_channelTime->push_back(channelTimeInJetRoiMap[k]);
              if (!pr_noBadCells) j_channelBad->push_back(badChannelMap[k]);

              // DataDescriptorElements

              // Channel info
              int barrelEc = m_onlineLArID->barrel_ec(channelID);
              int posNeg = m_onlineLArID->pos_neg(channelID);
              int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
              int slot = m_onlineLArID->slot(channelID);
              int chn = m_onlineLArID->channel(channelID);      
              std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
              j_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              // important indexes
              j_jetRoiChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
              j_jetRoiIndex_chLvl->push_back(jetRoiIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
              j_channelHashMap->push_back(index); // add the jet roi channel hash 
              
              if (pr_printCellsJet){
                ATH_MSG_INFO ("In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
              }
            } //end-if caloHashMap matches
          } //end for loop over caloHashMaps
        } // end-if clustBits Test
      } //end loop over LAr digits container
    } //end-if larClustBits is not empty


      // ========= TileCal ==========
    if (tileJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping Tile PMT's Digits...");
      // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
      ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));
  
      for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
        if (TileDigColl->empty() ) continue;

        HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
        // uint32_t rodBCID = TileDigColl->getRODBCID();

        for (const TileDigits* dig : *TileDigColl){
          HWIdentifier adc_id = dig->adc_HWID();

          IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
          // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
          
          size_t index = (size_t) (adcIdHash);

          if (index <= m_tileHWID->adc_hash_max()){ //safety verification
            if (tileJetRoiDigits.test(index)){ //if this channel index is inside cluster map of cells
              // Im aware this may not be the most optimized way to dump the containers info, but the solution below
              // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
              // because the order here is very important to keep the data links.
              for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == adc_id){ //find the index from pmt index to proceed.
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == adc_id " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << adc_id );
                  // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                  Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                  
                  // Digits     
                  // j_tileSamples->push_back(dig->samples()); //temporarily maintained
                  j_channelDigits->push_back(dig->samples());

                  // Channel / readout data
                  j_channelEnergy->push_back(channelEnergyInJetRoiMap[k]);
                  j_channelTime->push_back(channelTimeInJetRoiMap[k]);
                  if (!pr_noBadCells) j_channelBad->push_back(badChannelMap[k]);

                  //channelInfo
                  int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                  int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                  int partition = ros - 1; //partition (0-3)
                  int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                  int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                  std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                  j_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  // important indexes
                  j_jetRoiChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_chLvl->push_back(jetRoiIndex); // what roi this ch belongs
                  j_channelHashMap->push_back(index); // add the jet roi channel hash 
                  // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                  // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                  
                  if (pr_printCellsJet){ //option for debugging
                    ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());
                  }
                } // end if-pmt index comparison
              }// end loop over pmt-indexes
            } // end if digit index exist on clusteredDigits
          } //end if hash exists
        } // end loop TileDigitsCollection
      } //end loop TileDigitsContainer
    } // end DumpTileDigits
  
    // ##############################
    //  RAW CHANNEL INFO DUMP (JET)
    // ##############################

    // ========= LAr ==========
    if (larJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping LAr Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

      for (const LArRawChannel& LArChannel : *LArRawCnt){
        // for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier channelID = LArChannel.channelID();
          IdentifierHash chHwidHash = m_onlineLArID->channel_Hash(channelID);
          size_t index = (size_t) (chHwidHash);

          if (larJetRoiDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loop over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == channelID){
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == channelID " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << channelID );
                  
                  //RawCh info
                  int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
                  int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
                  uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
                  int provenance  = (int) LArChannel.provenance(); // its uint16_t
                  float rawEnergyConv  = (float) (rawEnergy); 
                  float rawTimeConv    = (float) (rawTime);
                  float rawQualityConv = (float) (rawQuality);

                  // Channel info
                  int barrelEc = m_onlineLArID->barrel_ec(channelID);
                  int posNeg = m_onlineLArID->pos_neg(channelID);
                  int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
                  int slot = m_onlineLArID->slot(channelID);
                  int chn = m_onlineLArID->channel(channelID);      
                  std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, chn } ; //unite info
                  j_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
                    ATH_MSG_ERROR (" ###### (Jet ROI) LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
                  }
                  j_rawChannelAmplitude->push_back(rawEnergyConv);
                  j_rawChannelTime->push_back(rawTimeConv);
                  j_rawChannelQuality->push_back(rawQualityConv);
                  j_rawChannelPedProv->push_back(provenance);

                  // important indexes
                  j_jetRoiRawChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_rawChLvl->push_back(jetRoiIndex); // what roi this ch belongs

                  if (pr_printCellsJet){ //optional to help debugging

                    HWIdentifier hardwareID = LArChannel.hardwareID();
                    HWIdentifier identifyID = LArChannel.identify();
                    uint16_t     provenance = LArChannel.provenance();

                    ATH_MSG_INFO ("In DumpLAr Raw ("<< channelIndexMap[k] <<"): hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        // } //end loop LArRawChannelCollection
      } //end loop LArRawChannelContainer
    } // end-if clustBits have Cells


    // ========= TileCal ==========
    if (tileJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping tile PMT's Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

      for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
        for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
          IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
          size_t index = (size_t) (chHwidHash);


          if (tileJetRoiDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == rawAdcHwid){
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == rawAdcHwid " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
                  
                  // Channel info
                  int tileAdc         = m_tileHWID->adc(rawAdcHwid);
                  int tileCh          = m_tileHWID->channel(rawAdcHwid);
                  int drawer          = m_tileHWID->drawer(rawAdcHwid);
                  int ros             = m_tileHWID->ros(rawAdcHwid);
                  int partition       = ros - 1;
                  std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
                  j_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  //RawCh info
                  float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
                  float rawTime       = TileChannel->time();    // time relative to triggering bunch
                  // quality is a number in [0,1] obtained by integrating the parent
                  // distribution for the "goodness" from the DSP.  In hypothesis testing
                  // terms, it is the significance of the hypothisis that the input
                  // to the DSP matches the signal-model used to tune the DSP.                  
                  float rawQuality    = TileChannel->quality();// quality of the sampling distribution
                  float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
                  j_rawChannelAmplitude->push_back(rawAmplitude);
                  j_rawChannelTime->push_back(rawTime);
                  j_rawChannelPedProv->push_back(rawPedestal);
                  j_rawChannelQuality->push_back(rawQuality);

                  // important indexes
                  j_jetRoiRawChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_rawChLvl->push_back(jetRoiIndex); // what roi this ch belongs

                  if (pr_printCellsJet){ //optional to help debugging
                    int tileIndex, tilePmt;

                    Identifier rawCellId = TileChannel->cell_ID();
                    Identifier rawPmtId = TileChannel->pmt_ID();
                    Identifier rawAdcId = TileChannel->adc_ID();

                    TileChannel->cell_ID_index(tileIndex,tilePmt);

                    ATH_MSG_INFO ("In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        } //end loop TileRawChannelCollection
      } //end loop TileRawChannelContainer
    } // end-if clustBits have Cells

    jetRoiIndex++;

    larJetRoiDigits.reset();
    tileJetRoiDigits.reset();

    caloHashMap.clear();
  
    channelHwidInJetRoiMap.clear();
    cellIndexMap.clear();
    channelIndexMap.clear();
    channelEnergyInJetRoiMap.clear();
    channelTimeInJetRoiMap.clear();
    channelEtaInJetRoiMap.clear();
    channelPhiInJetRoiMap.clear();
    cellGranularityEtaInJetRoiMap.clear();
    cellGranularityPhiInJetRoiMap.clear();
    channelCaloRegionMap.clear();
    if (!pr_noBadCells) badChannelMap.clear();
    // cellClusDeltaEta.clear();
    // cellClusDeltaPhi.clear();
  } //end loop over jets in container
  return StatusCode::SUCCESS;
}

void EventReaderAlg::findHottestCellInClusterSampling(const xAOD::CaloCluster* cl, CaloCell* &hotCell , CaloSampling::CaloSample samp){

  float hotCellEnergy = -9999.0; // MeV
  // CaloCell* hotCell = nullptr; // pointer to the hottest cell in sampling

 // loop over cells in cluster
  auto itrCells = cl->cell_begin();       
  auto itrCellsEnd = cl->cell_end(); 

  // ATH_MSG_INFO ("The Sampling is: "<< samp << ".");

  for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){ 

    const CaloCell* cell = (*itCells); // recover the CaloCell from iterator

    // ATH_MSG_INFO (" (CaloSampling::CaloSample) BEFORE");
    CaloSampling::CaloSample sampCell = (CaloSampling::CaloSample) m_calocell_id->calo_sample(cell->ID()); // get sampling
    // ATH_MSG_INFO (" (CaloSampling::CaloSample) AFTER. sampCell = " << sampCell <<". samp = " << samp << " / " << (sampCell == samp));

    if (sampCell == samp){ // verify the hottest cell only for chosen sampling
    // ATH_MSG_INFO ("The Sampling is matching. ");
      if (cell->energy() > hotCellEnergy){
        // ATH_MSG_INFO ("is the hottest cell. ");
        hotCellEnergy = cell->energy();
        hotCell = const_cast<CaloCell* >(cell); // point cell as the hottest
        // ATH_MSG_DEBUG( "Hotcell found: "<< hotCellEnergy << " MeV. " << hotCell->eta() << " " << hotCell->phi());
        // ATH_MSG_DEBUG( "Hotcell found: "<< hotCellEnergy << " MeV. ");
      }
      else{
        continue;
      }
    }
  }
  // ATH_MSG_DEBUG (" End of cluster cells. ");
  
  // return hotCell;
}

// StatusCode EventReaderAlg::buildFixedSizeClusterInSampling(std::vector<const CaloCell*> &cellCollectionROI, CaloCell* hotCell, CaloSampling::CaloSample samp, int nCellsEta, int nCellsPhi){
StatusCode EventReaderAlg::buildFixedSizeClusterInSampling(std::vector<const CaloCell*> &cellCollectionROI, CaloCell* hotCell, CaloSampling::CaloSample samp, double etaSize, double phiSize){
  // Window size: deta=0.175 dphi=0.269981 (EMB2 7x11 cells window)
  // SLOWEST but easiest way to built the clusters
  // - loop over the all cell collection
  // - verify if the actual cell is inside the N_eta x M_phi window
  // - add cell to the new cluster
  const CaloCellContainer*    CaloCnt     = nullptr;

  // float etaSize = nCellsEta * hotCell->caloDDE()->deta();
  // float phiSize = nCellsPhi * hotCell->caloDDE()->dphi();

  ATH_MSG_DEBUG ("(buildFixedSizeClusterInSampling) Window size: " << etaSize << " " << phiSize);
  ATH_MSG_DEBUG("(buildFixedSizeClusterInSampling) isInPhiWindowRange: " << hotCell->phi() - phiSize/2 << " fix:" << fixPhi(hotCell->phi() - phiSize/2) << " " << hotCell->phi() + phiSize/2 << " fix: " <<  fixPhi(hotCell->phi() + phiSize/2));

  ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo")); // point to all calo cell container: CaloCompactCellContainer_AllCalo

  for (const auto cell : *CaloCnt){
    bool cellBelongsToSampling = (m_calocell_id->calo_sample(cell->ID()) == (int) samp);

    if (!cell || !cell->caloDDE() || !cellBelongsToSampling ){
      continue;
    }
    // ATH_MSG_DEBUG ("(m_calocell_id->calo_sample(cell->ID()) != (int) samp): " <<  cellBelongsToSampling);

    //verify if the actual cell is inside the N_eta x M_phi window
    bool isInEtaWindowRange = (cell->eta() >= (hotCell->eta() - etaSize/2)) && (cell->eta() <= (hotCell->eta() + etaSize/2));


    // bool isInPhiWindowRange = (cell->phi() >= fixPhi(hotCell->phi() - phiSize/2)) && (cell->phi() <= fixPhi(hotCell->phi() + phiSize/2));
    bool isInPhiWindowRange = false;
    double lowerPhiRange    = hotCell->phi() - phiSize/2 ;
    double upperPhiRange    = hotCell->phi() + phiSize/2 ;
    // bool isNegOverflow_low  = lowerPhiRange < -1*pi;
    // bool isNegOverflow_up   = upperPhiRange < -1*pi;
    // bool isPosOverflow_low  = lowerPhiRange > 1*pi;
    // bool isPosOverflow_up   = upperPhiRange > 1*pi;
    bool isPhiOverFlow      = (lowerPhiRange < -1*pi) || (upperPhiRange < -1*pi) || (lowerPhiRange > 1*pi) || (upperPhiRange > 1*pi) ;

    if (isPhiOverFlow) { // if overflows, need to include the discontinuity in the middle of the interval.
      isInPhiWindowRange  = ((cell->phi() >= fixPhi(lowerPhiRange)) && (cell->phi() <= 1*pi )) || (( cell->phi() <= fixPhi(upperPhiRange)) && (cell->phi() >= -1*pi));
    }
    else{
      isInPhiWindowRange = (cell->phi() >= lowerPhiRange) && (cell->phi() <= upperPhiRange);
    } 

    if (isInEtaWindowRange && isInPhiWindowRange){
      cellCollectionROI.push_back(cell);
      // ATH_MSG_DEBUG("isInEtaWindowRange && isInPhiWindowRange: " << (isInEtaWindowRange && isInPhiWindowRange));


      // add cell to new cell collection
      // int index = CaloCnt->findIndex(cell->caloDDE()->calo_hash());
      // if (index == -1){
      //   ATH_MSG_DEBUG ("(buildFixedSizeClusterInSampling) Cell index = -1 (cell not found).");
      //   continue;
      // }
      // newCluster->addCell(index, 1.); // add cells with weight = 1.0
    } // end-if window Range
    
  }// end-if cell container

  ATH_MSG_DEBUG ("Number of cells in new collection: "<< cellCollectionROI.size());
  return StatusCode::SUCCESS;
}

bool EventReaderAlg::eOverPElectron(const xAOD::Electron* electron){
  // the E/p is calculated here based on Electron caloCluster and its associated trackParticle
  // E_cluster/p_track
  float eoverp = 0.;
  float track_p = (electron->trackParticle())->pt()*std::cosh((electron->trackParticle())->eta());
  if (track_p != 0.) eoverp = (electron->caloCluster())->e()/track_p;
  else{
    ATH_MSG_WARNING ("(eOverP) Track_p == 0");
    return false;
  }
  if ((fabs(eoverp) < 0.7) || (fabs(eoverp) > 1.5) ){
    ATH_MSG_DEBUG ("(eOverP) E/p is out of range! (eoverp="<<eoverp<<").");
    return false;
  }
  else{
    ATH_MSG_DEBUG ("(eOverP) E/p="<<eoverp);
    return true;
  }
}


StatusCode EventReaderAlg::testCode(const xAOD::CaloCluster *cl){


    // ----------------------------------------------------------------------  
  // ---------------------------START TEST---------------------------------
  // ----------------------------------------------------------------------  

  std::vector<size_t>       cellIndexVec; // TESTING

  const CaloClusterCellLink* cellLinks = cl->getCellLinks();
  CaloClusterCellLink::const_iterator it_cell=cellLinks->begin();
  CaloClusterCellLink::const_iterator it_cell_e=cellLinks->end();

  int cellLoopIndex = 0;
  for(;it_cell!=it_cell_e;++it_cell) {  
  // for (auto itCells=itrCellsBegin; itCells != itrCellsEnd; ++itCells){

    const CaloCell* cell  = (*it_cell); //get the caloCells
    double clusCellWeight = it_cell.weight(); // what is the cell weight in the cluster ( https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/Calorimeter/CaloCalibHitRec/src/CaloCalibClusterMomentsMaker2.cxx#0335 )

    // bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
    bool    isLAr     = cell->caloDDE()->is_lar_em();
    bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
    bool    isLArHEC  = cell->caloDDE()->is_lar_hec();

    IdentifierHash chidHash, adcidHash;
    HWIdentifier chhwid, adchwid;
    size_t index;

    if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
      chhwid = larCabling->createSignalChannelID(cell->ID());
      // std::string hwid_lar = chid.getString();
      // ATH_MSG_DEBUG (typeid(chhwid).name());
      chidHash =  m_onlineLArID->channel_Hash(chhwid);
      index = (size_t) (chidHash);

      // verify if a cell was stored more than once in the same cluster
      if(std::find(cellIndexVec.begin(), cellIndexVec.end(), index) != cellIndexVec.end()) {
        /* cellIndexVec contains index */
        ATH_MSG_DEBUG (" OOPS, Cell index/hwid "<< index << "/ "<< chhwid <<" was already looped in this event. Weight: "<< clusCellWeight << ", E/t="<<cell->energy() << "/"<< cell->time() << ", Eta/Phi: "<< cell->eta() << "/" << cell->phi() );
        // if cells are repeated, store them in specific variables
        c_cellEta_rep->push_back(cell->eta());
        c_cellPhi_rep->push_back(cell->phi());
        c_cellEnergy_rep->push_back(cell->energy());

        continue;
      }
      else {
        /* cellIndexVec does not contain index */
        // ATH_MSG_DEBUG (" New unique cell added.");
      }

      cellIndexVec.push_back(index);

      ATH_MSG_DEBUG (" LAr Cell ("<< cellLoopIndex <<") index: " << index << ", hwid: " << chhwid << ". Weight: "<< clusCellWeight << ", E/t="<<cell->energy() << "/"<< cell->time() << ", Eta/Phi: "<< cell->eta() << "/" << cell->phi() );
      ++cellLoopIndex;
    }
  }
  cellIndexVec.clear(); 
  // ----------------------------------------------------------------------  
  // ---------------------------END TEST-----------------------------------  
  // ---------------------------------------------------------------------- 

    // // TILEDIGITS AND TILERAWCHANNEL
    // const TileRawChannelContainer* TileRawCnt = nullptr;
    // const TileDigitsContainer* TileDigitsCnt = nullptr;

    // ATH_CHECK (evtStore()->retrieve (TileDigitsCnt, "TileDigitsCnt"));

    // // test max of all kinds of id's
    // unsigned int maxAdcHash = m_tileID->adc_hash_max();
    // std::cout << "SW MAX CELLS: " << m_tileID->cell_hash_max() << std::endl;
    // std::cout << "SW MAX PMT: " << m_tileID->pmt_hash_max() << std::endl;
    // std::cout << "SW MAX ADC: " << maxAdcHash << std::endl;

    // maxAdcHash = m_tileHWID->adc_hash_max();
    // std::cout << "HW MAX CHANNELS: " << m_tileHWID->channel_hash_max() << std::endl;
    // std::cout << "HW MAX ADC: " << maxAdcHash << std::endl;

    // int maxChannels = m_tileCabling->getMaxChannels();
    // int maxGains = m_tileCabling->getMaxGains();
    // std::cout << "MAX CHANNELS (in a drawer): " << maxChannels << std::endl;
    // std::cout << "MAX GAINS: " << maxGains << std::endl;
    

    // for (const TileDigitsCollection* TileDigColl : *TileDigitsCnt){
    //   if (TileDigColl->empty() ) continue;

    //   HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
    //   // int ros = m_tileHWID->ros(adc_id_collection);
    //   // int drawer = m_tileHWID->drawer(adc_id_collection);
    //   // int partition = ros - 1;
    //   // uint32_t rodBCID = TileDigColl->getRODBCID();

    //   for (const TileDigits* TileDig : *TileDigColl){
    //     // ATH_MSG_DEBUG( "  TileDig->samples(): " << TileDig->samples());
    //     HWIdentifier adc_id = TileDig->adc_HWID(); // adc id, related to hardware. is different from channel id.

    //     IdentifierHash adc_ch_hash = m_tileHWID->get_channel_hash(adc_id);
    //     IdentifierHash adc_hash = m_tileHWID->get_hash(adc_id);

    //     Identifier swcell_id = m_tileCabling->h2s_cell_id(adc_id); // cell id 
    //     Identifier swpmt_id = m_tileCabling->h2s_pmt_id(adc_id); // cell id 
    //     Identifier swadc_id = m_tileCabling->h2s_adc_id(adc_id); // cell id 

    //     // int tileCh = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
    //     // int tileAdc = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)

    //     // size_t index = (size_t) (adc_hash);
    //     // ATH_MSG_DEBUG ("   adc_id: " << adc_id << ". index: " << index <<". ros: " << ros << ". drawer: " << drawer << ". hw_ch_hash_max (hw_adc_hash_max): " << hw_ch_hash_max << "("<< hw_adc_hash_max << ")" ". rodBCID: " << rodBCID);
    //     // ATH_MSG_DEBUG ("   adc_id (ros/drawer/ch/adc): " << adc_id << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" << "index " << index << " adc_ch_hash " << adc_ch_hash << " adc_hash " << adc_hash << " swids (cell/pmt/adc): " << swcell_id << " / " << swpmt_id << " / " << swadc_id <<  ") rodBCID: " << rodBCID);

    //   } // end loop TileDigitsContainer    
    // } //end loop TileDigitsCollection


    // ATH_CHECK (evtStore()->retrieve( TileRawCnt , "TileRawChannelCnt"));
    // for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
    //   for (const TileRawChannel* TileChannel : *TileChannelColl){
    //     // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/TileCalorimeter/TileEvent/TileEvent/TileRawData.h
    //     int tileIndex, tilePmt = 0;
    //     Identifier rawCellId = TileChannel->cell_ID();
    //     Identifier rawPmtId = TileChannel->pmt_ID();
    //     Identifier rawAdcId = TileChannel->adc_ID();

    //     HWIdentifier rawAdcHwid = TileChannel->adc_HWID();

    //     int tileAdc       = m_tileHWID->adc(rawAdcHwid);
    //     int tileCh   = m_tileHWID->channel(rawAdcHwid);
    //     int drawer    = m_tileHWID->drawer(rawAdcHwid);
    //     int ros       = m_tileHWID->ros(rawAdcHwid);

    //     TileChannel->cell_ID_index(tileIndex,tilePmt);
 
    //     ATH_MSG_DEBUG ("TileRawID/rawPmtId/rawAdcId/rawAdcHwid (ros/drawer/ch/adc): " << rawCellId <<"/" << rawPmtId << "/" << rawAdcId << "/" << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << TileChannel->amplitude() << ". Time: " << TileChannel->time()); 

    //   } //end loop TileRawChannelContainer
    // } //end loop TileRawChannelCollection
      
  return StatusCode::SUCCESS;
}

// create a new electrons container with the electrons that fullfill the list of criteria below
bool EventReaderAlg::trackSelectionElectrons(const xAOD::Electron *electron, const xAOD::VertexContainer *primVertexCnt){
  
  const xAOD::TrackParticle *trackElectron =  electron->trackParticle();
  const xAOD::CaloCluster *elClus = electron->caloCluster();

  if (!trackElectron) { //is there track?
    ATH_MSG_DEBUG ("(trackSelectionElectrons) No track particle for Tag check."); 
    return false;
  }
  // caloCluster  
  if (!elClus){ //is there a calo cluster associated to this electron?
    ATH_MSG_DEBUG ("(trackSelectionElectrons) No caloCluster associated for Tag check.");
    return false;
  }

  // track_pt > 7 GeV
  if (! ( (trackElectron->pt() * GeV ) > 7) ){
    ATH_MSG_DEBUG ("(trackSelectionElectrons) Pt_Track of the electron is below 7 GeV. Electron rejected.");
    return false;
  }
  // |eta| < 2.47
  if (! (fabs(trackElectron->eta()) < 2.47 )){
    ATH_MSG_DEBUG ("(trackSelectionElectrons) Electron track_eta is above 2.47. Electron rejected.");
    return false;
  }

  // check for primary vertex in the event
  // *************** Primary vertex check ***************
  //loop over vertices and look for good primary vertex
  bool  isPrimVtx      = false;
  bool  passDeltaZ0sin = false;
  for (xAOD::VertexContainer::const_iterator vxIter = primVertexCnt->begin(); vxIter != primVertexCnt->end(); ++vxIter) {
    // Select good primary vertex          
    float delta_z0      = fabs(trackElectron->z0() + trackElectron->vz() - (*vxIter)->z()); //where trk.vz() represents the point of reference for the z0 calculation (in this case, the beamspot position along the z axis).
    float delta_z0_sin  = delta_z0 * sin(trackElectron->theta()); //where sin(trk.theta()) parameterises the uncertainty of the z0 measurement. 

    if (((*vxIter)->vertexType() == xAOD::VxType::PriVtx) && ( delta_z0_sin < pr_z0Tag)){
      isPrimVtx       = true; //check for primary vertex in each vertex of the event.
      passDeltaZ0sin  = true; // and check if the longitudinal impact parameter difference for electron-to-vertex association.

      // ATH_MSG_DEBUG ("(trackSelectionElectrons) delta_z0_sin = "<< delta_z0_sin << ", Vtx_xyz=" <<(*vxIter)->x() <<", " <<(*vxIter)->y() << ", " << (*vxIter)->z() << ", tag_z= "<< ((elTag->trackParticle())->z0() + (elTag->trackParticle())->vz()) <<", vtxType="<< (*vxIter)->vertexType() );
      ATH_MSG_DEBUG ("(trackSelectionElectrons) delta_z0_sin < 0.5 mm ("<< delta_z0_sin << ")");
      ATH_MSG_DEBUG ("(trackSelectionElectrons) There is a primary vertex in the event.");
      break;
    }
  }

  // delta_z0_sin <= 0.5 mm
  if (!( isPrimVtx && passDeltaZ0sin)){
    ATH_MSG_DEBUG ("(trackSelectionElectrons) For this Tag, delta_z0_sin > 0.5mm, and there is NO primary vertices in the event. Rejecting electron.");
    return false;
  }  
  
  // d0_sig - transverse impact parameter significance
  // double d0sig = xAOD::TrackingHelpers::d0significance( elTag->trackParticle(), ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY() ); // SEG-FAULT !
  float d0sig = fabs( trackElectron->d0()) / sqrt(trackElectron->definingParametersCovMatrix()(0,0));  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/InDetTrackingDC14#Impact_parameters_z0_d0_definiti
  if ( !( d0sig < pr_d0TagSig)){
    ATH_MSG_DEBUG("(trackSelectionElectrons) Electron rejected. d0sig > " << pr_d0TagSig << " ("<< d0sig <<")");
    return false;
  }

  ATH_MSG_DEBUG("(trackSelectionElectrons) Electron accepted.");
  return true; // if true, add electron to electronsSelectedByTrack container.
}

bool EventReaderAlg::isTagElectron(const xAOD::Electron *el){
  // check constituents
  // track
  // const xAOD::TrackParticle *trk = el->trackParticle();
  // if (!el->trackParticle()) { //is there track?
  //   ATH_MSG_DEBUG ("(isTagElectron) No track particle for Tag check."); 
  //   return false;
  // }
  // ATH_MSG_DEBUG ("(isTagElectron) Track pt: "<< trk->pt());

  // // caloCluster
  // const xAOD::CaloCluster *elClus = el->caloCluster();
  // if (!el->caloCluster()){ //is there a calo cluster associated to this electron?
  //   ATH_MSG_DEBUG ("(isTagElectron) No caloCluster associated for Tag check.");
  //   return false;
  // }
  // ATH_MSG_DEBUG ("(isTagElectron) Cluster energy: "<< elClus->e());

  // |eta| < 1.475
  if ( fabs(el->eta()) > 1.475 ){ // EM Barrel
    ATH_MSG_DEBUG ("(isTagElectron) Electron |eta| > 1.475 (" << fabs(el->eta()) << ").");
    return false;
  }

  // LAr crack region
  // ATH_MSG_DEBUG ("(isTagElectron) Selecting Tag Electron Eta outside crack region...");
  float absEta = fabs(el->caloCluster()->etaBE(2));
  if ( !isEtaOutsideLArCrack(absEta) ){
    ATH_MSG_DEBUG ("(isTagElectron) Selecting Tag Electron Eta is inside crack region.");
    return false;
  }
  // ATH_MSG_DEBUG ("(isTagElectron) Tag Electron absEta: "<< absEta);

  // Tag electron PID verify if is tight
  // ATH_MSG_DEBUG ("(isTagElectron) Testing offTagTightness...");
  bool isGood;
  if (! el->passSelection(isGood, pr_offTagTightness) ) {
    ATH_MSG_DEBUG("(isTagElectron) Misconfiguration: " << pr_offTagTightness << " is not a valid working point for electrons");
    return false; // no point in continuing
    }
  ATH_MSG_DEBUG("(isTagElectron) Trigger " << pr_offTagTightness << " is OK");

  // Et > 25 (or 15) GeV && Et < 180
  // ATH_MSG_DEBUG ("(isTagElectron) Selecting Tag Electron Et...");
  float elTagEt = el->e()/(cosh(el->trackParticle()->eta()));
  float elTagPt = el->pt();
  // ATH_MSG_DEBUG ("(isTagElectron) Tag Electron Et: "<< elTagEt << ", threshold =" << pr_etMinTag * GeV << ".");
  // if ( !( (elTagEt > (pr_etMinTag * GeV)) && (elTagEt < (pr_etMaxTag * GeV)) ) ){
  if ( elTagPt < (pr_etMinTag * GeV) ){
    ATH_MSG_DEBUG ("(isTagElectron) Tag Electron Et/pT: "<< elTagEt << "/"<< elTagPt << ", threshold =" << pr_etMinTag * GeV << ".");
    return false;
  }

  // electron object quality
  ATH_MSG_DEBUG ("(isTagElectron) Checking electron object quality...");
  if ( !el->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)){
    ATH_MSG_DEBUG ("(isTagElectron) \tTag Electron is a BADCLUSELECTRON.");
    return false;
  }

  return true;
}

bool EventReaderAlg::isGoodProbeElectron(const xAOD::Electron *el){
  // check constituents
  // track
  // const xAOD::TrackParticle *trk = el->trackParticle();
  // if (!el->trackParticle()) { //is there track?
  //   ATH_MSG_DEBUG ("(isGoodProbeElectron) No track particle for Probe check."); 
  //   return false;
  // }
  // ATH_MSG_DEBUG ("(isGoodProbeElectron) Probe Track pt: "<< trk->pt());

  // caloCluster
  // const xAOD::CaloCluster *elClus = el->caloCluster();
  // if (!el->caloCluster()){ //is there a calo cluster associated to this electron?
  //   ATH_MSG_DEBUG ("(isGoodProbeElectron) No caloCluster associated for Tag check.");
  //   return false;
  // }
  // ATH_MSG_DEBUG (" (isGoodProbeElectron) Probe Cluster energy: "<< elClus->e());

  // |eta| < 1.475
  if ( fabs(el->eta()) > 1.475 ){ // EM Barrel
    ATH_MSG_DEBUG ("(isGoodProbeElectron) Electron |eta| > 1.475 (" << fabs(el->eta()) << ").");
    return false;
  }

  // electron object quality
  ATH_MSG_DEBUG (" (isGoodProbeElectron) Checking Probe electron object quality...");
  if ( !el->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)){
    ATH_MSG_DEBUG (" (isGoodProbeElectron) \tProbe Electron is a BADCLUSELECTRON.");
    return false;
  }

  // Et > 15 GeV
  // float electronEt = el->e()/(cosh(el->trackParticle()->eta()));
  float electronPt = el->pt();
  if (electronPt * GeV < pr_etMinProbe ){
    ATH_MSG_DEBUG ("(isGoodProbeElectron) Electron Et/pT < " << pr_etMinProbe << " GeV (pT="<< electronPt * GeV<<")");
    return false;
  }

  // is outside the crack
  float absEta = fabs(el->caloCluster()->etaBE(2));
  if ( !isEtaOutsideLArCrack(absEta) ){
    ATH_MSG_DEBUG ("(isGoodProbeElectron) Electron Eta inside LAr crack region...");
    return false;
  }

  // loose ID Cut
  bool isGood;
  if (! el->passSelection(isGood, pr_offProbeTightness)){
    ATH_MSG_DEBUG("(isGoodProbeElectron) Misconfiguration: " << pr_offProbeTightness << " is not a valid working point for electrons");
    return false;
  }

  ATH_MSG_DEBUG ("(isGoodProbeElectron) Electron is a good probe!");
  return true;
}

bool EventReaderAlg::isEtaOutsideLArCrack(float absEta){
  if ( (absEta > 1.37 && absEta < 1.52) || (absEta > 2.47) ){
    return false;
  }
  else{
    return true;
  }  
}


int EventReaderAlg::getCaloRegionIndex(const CaloCell* cell){
  if (cell->caloDDE()->is_tile()) return 0; //belongs to Tile
  // else if (cell->caloDDE()->is_lar_em()) return 1; //belongs to EM calorimeter !!
  else if (cell->caloDDE()->is_lar_em_barrel()) return 1; //belongs to EM barrel
  // else if (cell->caloDDE()->is_lar_em_endcap()) return 3; //belongs to EM end cap MESMA COISA DA LAR_EM !!
  else if (cell->caloDDE()->is_lar_em_endcap_inner()) return 2; //belongs to the inner wheel of EM end cap
  else if (cell->caloDDE()->is_lar_em_endcap_outer()) return 3; //belongs to the outer wheel of EM end cap
  else if (cell->caloDDE()->is_lar_hec()) return 4; //belongs to HEC
  else if (cell->caloDDE()->is_lar_fcal()) return 5; //belongs to FCAL
    
  ATH_MSG_ERROR (" #### Region not found for cell offline ID "<< cell->ID() <<" ! Returning -999.");
  return -999; //region not found
}

int EventReaderAlg::getNumberCellsInClusterSampling(int CaloSamplingIndex, std::vector < int > * cellLayerVector){
  // CaloSamplingIndex is the sampling index chosen to count the number of cells in that  sampling layer.
  int nCellsInSampling = 0;

  for (auto samp=cellLayerVector->begin(); samp!=cellLayerVector->end(); ++samp){
    if ((*samp) == CaloSamplingIndex){
      ++nCellsInSampling;
    }
  }

  return nCellsInSampling;
}


double EventReaderAlg::fixPhi(double phi){
  // Verify the phi value, if its in -pi,pi interval, then shifts 2pi in the correct direction.
  if (phi < -1*pi) return (phi + 2*pi);
  if (phi >  1*pi) return (phi - 2*pi);
  return phi;
}


double EventReaderAlg::deltaPhi(double phi1, double phi2){
  // Fix phi value for delta_phi calculation, to -pi,+pi interval.
  double deltaPhi = fixPhi(phi1) - fixPhi(phi2);
  return fixPhi(deltaPhi);
}


double EventReaderAlg::deltaR( double deta, double dphi){
  return sqrt( deta*deta + fixPhi(dphi)*fixPhi(dphi) );
}


void EventReaderAlg::bookBranches(TTree *tree){
  // ## Event info
  tree->Branch ("RunNumber", &e_runNumber);
  tree->Branch ("EventNumber", &e_eventNumber);
  tree->Branch ("BCID", &e_bcid);
  // tree->Branch ("Lumiblock ",&e_lumiBlock);
  tree->Branch ("avg_mu_inTimePU", &e_inTimePileup);
  tree->Branch ("avg_mu_OOTimePU", &e_outOfTimePileUp);
  // #############

  // ## Cluster 
  tree->Branch ("cluster_index",&c_clusterIndex);
  tree->Branch ("c_electronIndex_clusterLvl",&c_electronIndex_clusterLvl);
  tree->Branch ("cluster_et",&c_clusterEnergy);
  tree->Branch ("cluster_time",&c_clusterTime);
  tree->Branch ("cluster_pt",&c_clusterPt);
  tree->Branch ("cluster_eta",&c_clusterEta);
  tree->Branch ("cluster_phi",&c_clusterPhi);
  // Cluster cell
  tree->Branch ("cluster_index_cellLvl",&c_clusterIndex_cellLvl);
  tree->Branch ("cluster_cell_index",&c_clusterCellIndex);
  tree->Branch ("cluster_cell_caloGain",&c_cellGain);
  tree->Branch ("cluster_cell_layer",&c_cellLayer);
  tree->Branch ("cluster_cell_region",&c_cellRegion);
  tree->Branch ("cluster_cell_energy",&c_cellEnergy);
  tree->Branch ("cluster_cell_eta",&c_cellEta);
  tree->Branch ("cluster_cell_phi",&c_cellPhi);
  tree->Branch ("cluster_cell_deta",&c_cellDEta);
  tree->Branch ("cluster_cell_dphi",&c_cellDPhi);
  tree->Branch ("cluster_cellsDist_dphi",&c_cellToClusterDPhi);
  tree->Branch ("cluster_cellsDist_deta",&c_cellToClusterDEta);
  // **** Testing Variables
  if (pr_testCode){
    tree->Branch ("cluster_cell_eta_repeated",&c_cellEta_rep);
    tree->Branch ("cluster_cell_phi_repeated",&c_cellPhi_rep);
    tree->Branch ("cluster_cell_energy_repeated",&c_cellEnergy_rep);
  }
  tree->Branch ("cluster_nCells_PS",&c_nCellsInPS);
  tree->Branch ("cluster_nCells_EMB1",&c_nCellsInEMB1);
  tree->Branch ("cluster_nCells_EMB2",&c_nCellsInEMB2);
  tree->Branch ("cluster_nCells_EMB3",&c_nCellsInEMB3);
  //****
  // Cluster channel (digits and cell ch)
  tree->Branch ("cluster_index_chLvl",&c_clusterIndex_chLvl);
  tree->Branch ("cluster_channel_index",&c_clusterChannelIndex);
  tree->Branch ("cluster_channel_digits",&c_channelDigits);
  tree->Branch ("cluster_channel_energy",&c_channelEnergy);
  tree->Branch ("cluster_channel_time",&c_channelTime);
  if (!pr_noBadCells) tree->Branch ("cluster_channel_bad",&c_channelBad);
  tree->Branch ("cluster_channel_chInfo", &c_channelChInfo);
  tree->Branch ("cluster_channel_hash",&c_channelHashMap); 
  tree->Branch ("cluster_channel_id",&c_channelChannelIdMap);
  if (pr_getLArCalibConstants){
    tree->Branch ("cluster_channel_effSigma",&c_channelEffectiveSigma);
    tree->Branch ("cluster_channel_noise",&c_channelNoise);
    tree->Branch ("cluster_channel_DSPThreshold",&c_channelDSPThreshold);
    tree->Branch ("cluster_channel_OFCTimeOffset",&c_channelOFCTimeOffset);
    tree->Branch ("cluster_channel_ADC2MeV0",&c_channelADC2MEV0);
    tree->Branch ("cluster_channel_ADC2MeV1",&c_channelADC2MEV1);
    tree->Branch ("cluster_channel_OFCa",&c_channelOFCa);
    tree->Branch ("cluster_channel_OFCb",&c_channelOFCb);
    tree->Branch ("cluster_channel_shape",&c_channelShape);
    tree->Branch ("cluster_channel_shapeDer",&c_channelShapeDer);

  }

  // Cluster raw channel
  tree->Branch ("cluster_index_rawChLvl",&c_clusterIndex_rawChLvl);
  tree->Branch ("cluster_rawChannel_index",&c_clusterRawChannelIndex);
  tree->Branch ("cluster_rawChannel_id",&c_rawChannelIdMap);
  tree->Branch ("cluster_rawChannel_amplitude",&c_rawChannelAmplitude);
  tree->Branch ("cluster_rawChannel_time",&c_rawChannelTime);
  tree->Branch ("cluster_rawChannel_pedProv",&c_rawChannelPedProv);
  tree->Branch ("cluster_rawChannel_qual",&c_rawChannelQuality);  
  tree->Branch ("cluster_rawChannel_chInfo",&c_rawChannelChInfo); // tree->Branch ("cluster_cell_caloGain",&c_cellGain); //modify
  tree->Branch ("cluster_rawChannel_DSPThreshold",&c_rawChannelDSPThreshold);
  // tree->Branch ("cluster_rawChannel_DSPThreshold",&c_rawChannelDSPThreshold);

  // ## Cluster EMB2 7_11
  tree->Branch ("cluster711_index",&c711_clusterIndex);
  tree->Branch ("c711_electronIndex_clusterLvl",&c711_electronIndex_clusterLvl);
  tree->Branch ("cluster711_et",&c711_clusterEnergy);
  tree->Branch ("cluster711_pt",&c711_clusterPt);
  tree->Branch ("cluster711_time",&c711_clusterTime);
  tree->Branch ("cluster711_eta",&c711_clusterEta);
  tree->Branch ("cluster711_phi",&c711_clusterPhi);
  // Cluster cell
  tree->Branch ("cluster711_index_cellLvl",&c711_clusterIndex_cellLvl);
  tree->Branch ("cluster711_cell_index",&c711_clusterCellIndex);
  tree->Branch ("cluster711_cell_caloGain",&c711_cellGain);
  tree->Branch ("cluster711_cell_layer",&c711_cellLayer);
  tree->Branch ("cluster711_cell_region",&c711_cellRegion);
  tree->Branch ("cluster711_cell_energy",&c711_cellEnergy);
  tree->Branch ("cluster711_cell_eta",&c711_cellEta);
  tree->Branch ("cluster711_cell_phi",&c711_cellPhi);
  tree->Branch ("cluster711_cell_deta",&c711_cellDEta);
  tree->Branch ("cluster711_cell_dphi",&c711_cellDPhi);
  tree->Branch ("cluster711_cellsDist_dphi",&c711_cellToClusterDPhi);
  tree->Branch ("cluster711_cellsDist_deta",&c711_cellToClusterDEta);
  // **** Testing Variables
  tree->Branch ("cluster711_nCells_PS",&c711_nCellsInPS);
  tree->Branch ("cluster711_nCells_EMB1",&c711_nCellsInEMB1);
  tree->Branch ("cluster711_nCells_EMB2",&c711_nCellsInEMB2);
  tree->Branch ("cluster711_nCells_EMB3",&c711_nCellsInEMB3);
  //****
  // Cluster channel (digits and cell ch)
  tree->Branch ("cluster711_index_chLvl",&c711_clusterIndex_chLvl);
  tree->Branch ("cluster711_channel_index",&c711_clusterChannelIndex);
  tree->Branch ("cluster711_channel_digits",&c711_channelDigits);
  tree->Branch ("cluster711_channel_energy",&c711_channelEnergy);
  tree->Branch ("cluster711_channel_time",&c711_channelTime);
  if (!pr_noBadCells) tree->Branch ("cluster_channel_bad",&c711_channelBad);
  tree->Branch ("cluster711_channel_chInfo", &c711_channelChInfo);
  tree->Branch ("cluster711_channel_hash",&c711_channelHashMap); 
  tree->Branch ("cluster711_channel_id",&c711_channelChannelIdMap);
  if (pr_getLArCalibConstants){
    tree->Branch ("cluster711_channel_effSigma",&c711_channelEffectiveSigma);
    tree->Branch ("cluster711_channel_noise",&c711_channelNoise);
    tree->Branch ("cluster711_channel_DSPThreshold",&c711_channelDSPThreshold);
    tree->Branch ("cluster711_channel_OFCTimeOffset",&c711_channelOFCTimeOffset);
    tree->Branch ("cluster711_channel_ADC2MeV0",&c711_channelADC2MEV0);
    tree->Branch ("cluster711_channel_ADC2MeV1",&c711_channelADC2MEV1);
    tree->Branch ("cluster711_channel_OFCa",&c711_channelOFCa);
    tree->Branch ("cluster711_channel_OFCb",&c711_channelOFCb);
    tree->Branch ("cluster711_channel_shape",&c711_channelShape);
    tree->Branch ("cluster711_channel_shapeDer",&c711_channelShapeDer);

  }
  // Cluster raw channel
  tree->Branch ("cluster711_index_rawChLvl",&c711_clusterIndex_rawChLvl);
  tree->Branch ("cluster711_rawChannel_index",&c711_clusterRawChannelIndex);
  tree->Branch ("cluster711_rawChannel_id",&c711_rawChannelIdMap);
  tree->Branch ("cluster711_rawChannel_amplitude",&c711_rawChannelAmplitude);
  tree->Branch ("cluster711_rawChannel_time",&c711_rawChannelTime);
  tree->Branch ("cluster711_rawChannel_pedProv",&c711_rawChannelPedProv);
  tree->Branch ("cluster711_rawChannel_qual",&c711_rawChannelQuality);  
  tree->Branch ("cluster711_rawChannel_chInfo",&c711_rawChannelChInfo); // tree->Branch ("cluster_cell_caloGain",&c_cellGain); //modify
  tree->Branch ("cluster711_rawChannel_DSPThreshold",&c711_rawChannelDSPThreshold);
  // #############

  // ## Jets
  if (pr_doJetRoiDump){
    tree->Branch ("jet_index",&j_jetIndex);
    tree->Branch ("jet_pt",&j_jetPt);
    tree->Branch ("jet_eta",&j_jetEta);
    tree->Branch ("jet_phi",&j_jetPhi);
    tree->Branch ("jet_roi_r",&j_roi_r);
    // Cells
    tree->Branch ("jet_index_cellLvl",&j_jetRoiIndex_cellLvl);
    tree->Branch ("jet_cell_index",&j_jetRoiCellIndex);
    tree->Branch ("jet_cell_caloGain",&j_cellGain);
    tree->Branch ("jet_cell_layer",&j_cellLayer);
    tree->Branch ("jet_cell_region",&j_cellRegion);
    tree->Branch ("jet_cell_energy",&j_cellEnergy);
    tree->Branch ("jet_cell_eta",&j_cellEta);
    tree->Branch ("jet_cell_phi",&j_cellPhi);
    tree->Branch ("jet_cell_deta",&j_cellDEta);
    tree->Branch ("jet_cell_dphi",&j_cellDPhi);
    tree->Branch ("jet_cell_distJetDPhi",&j_cellToJetDPhi);
    tree->Branch ("jet_cell_distJetDEta",&j_cellToJetDEta);  
    // Channel
    tree->Branch ("jet_index_chLvl",&j_jetRoiIndex_chLvl);
    tree->Branch ("jet_channel_index",&j_jetRoiChannelIndex);
    tree->Branch ("jet_channel_chInfo",&j_channelChInfo);
    tree->Branch ("jet_channel_digits",&j_channelDigits);
    // tree->Branch ("jet_channel_larDigits",&j_larSamples); // xxxxxxxxxxxxxxxxx excluir
    // tree->Branch ("jet_channel_tileDigits",&j_tileSamples); //xxxxxxxxxxxxxxxxx excluir
    tree->Branch ("jet_channel_energy",&j_channelEnergy);
    tree->Branch ("jet_channel_time",&j_channelTime);
    if (!pr_noBadCells) tree->Branch ("jet_channel_bad",&j_channelBad);
    tree->Branch ("jet_channel_hash",&j_channelHashMap); 
    // Raw Channel
    tree->Branch ("jet_rawChannel_chInfo",&j_rawChannelChInfo);
    tree->Branch ("jet_rawChannel_amplitude",&j_rawChannelAmplitude);
    tree->Branch ("jet_rawChannel_time",&j_rawChannelTime);
    tree->Branch ("jet_rawChannel_pedProv",&j_rawChannelPedProv);
    tree->Branch ("jet_rawChannel_qual",&j_rawChannelQuality);
    tree->Branch ("jet_rawChannel_index",&j_jetRoiRawChannelIndex);
    tree->Branch ("jet_index_rawChLvl",&j_jetRoiIndex_rawChLvl);
    tree->Branch("jet_std_rings", &j_std_rings);
  }

  // ## Particle Truth ##
  if (pr_isMC){
    if (pr_doTruthPartDump){
      tree->Branch("mc_part_energy",&mc_part_energy);
      tree->Branch("mc_part_pt",&mc_part_pt);
      tree->Branch("mc_part_m",&mc_part_m);
      tree->Branch("mc_part_eta",&mc_part_eta);
      tree->Branch("mc_part_phi",&mc_part_phi);
      tree->Branch("mc_part_pdgId",&mc_part_pdgId);
      tree->Branch("mc_part_status", &mc_part_status);
      tree->Branch("mc_part_barcode", &mc_part_barcode);
    }
    // ## Vertex Truth ##
    if (pr_doTruthEventDump){
      tree->Branch("mc_vert_x", &mc_vert_x);
      tree->Branch("mc_vert_y", &mc_vert_y);
      tree->Branch("mc_vert_z", &mc_vert_z);
      tree->Branch("mc_vert_time", &mc_vert_time);
      tree->Branch("mc_vert_perp", &mc_vert_perp);
      tree->Branch("mc_vert_eta", &mc_vert_eta);
      tree->Branch("mc_vert_phi", &mc_vert_phi);
      tree->Branch("mc_vert_barcode", &mc_vert_barcode);
      tree->Branch("mc_vert_id", &mc_vert_id);
    }
  }
  
  // ## Photons ##
  if (pr_doPhotonDump){
    tree->Branch("ph_energy",&ph_energy);
    tree->Branch("ph_pt",&ph_pt);  
    tree->Branch("ph_eta",&ph_eta);
    tree->Branch("ph_phi",&ph_phi);
    tree->Branch("ph_m",&ph_m);
  }  

  // ## Electrons ##
  tree->Branch("el_index",&el_index);
  tree->Branch("el_Pt",&el_Pt);
  tree->Branch("el_et",&el_et);
  tree->Branch("el_Eta",&el_Eta);
  tree->Branch("el_Phi",&el_Phi);
  tree->Branch("el_m",&el_m);
  tree->Branch("el_eoverp",&el_eoverp);
  tree->Branch("el_f1",&el_f1);
  tree->Branch("el_f3",&el_f3);
  tree->Branch("el_eratio",&el_eratio);
  tree->Branch("el_weta1",&el_weta1);
  tree->Branch("el_weta2",&el_weta2);
  tree->Branch("el_fracs1",&el_fracs1);
  tree->Branch("el_wtots1",&el_wtots1);
  tree->Branch("el_e277",&el_e277);
  tree->Branch("el_reta",&el_reta);
  tree->Branch("el_rphi",&el_rphi);
  tree->Branch("el_deltae",&el_deltae);
  tree->Branch("el_rhad",&el_rhad);
  tree->Branch("el_rhad1",&el_rhad1);

  // ## Tag and Probe ##
  // electrons
  // tree->Branch("tp_electronPt",&tp_electronPt);
  // tree->Branch("tp_electronEt",&tp_electronEt);
  // tree->Branch("tp_electronEta",&tp_electronEta);
  // tree->Branch("tp_electronPhi",&tp_electronPhi);
  // tree->Branch("tp_probeIndex",&tp_probeIndex);
  // tree->Branch("tp_tagIndex",&tp_tagIndex);
  // tree->Branch("tp_isTag",&tp_isTag);
  // tree->Branch("tp_isProbe",&tp_isProbe);
  // zee
  tree->Branch("zee_M", &zee_M);
  tree->Branch("zee_E", &zee_E);
  tree->Branch("zee_pt", &zee_pt);
  tree->Branch("zee_px", &zee_px);
  tree->Branch("zee_py", &zee_py);
  tree->Branch("zee_pz", &zee_pz);
  tree->Branch("zee_T", &zee_T);
  // tree->Branch("zee_x", &zee_x);
  // tree->Branch("zee_y", &zee_y);
  // tree->Branch("zee_z", &zee_z);
  tree->Branch("zee_deltaR", &zee_deltaR);

  // ## my Struct obj
  // tree->Branch("myStructObj", &myStruct);

}


void EventReaderAlg::clear(){
  // ## Event info
  e_runNumber       = 9999;
  e_eventNumber     = 9999;
  e_bcid            = 9999;
  // e_lumiBlock       = 9999; 
  e_inTimePileup    = -999;
  e_outOfTimePileUp = -999;
  // ############

  ATH_MSG_DEBUG("Clear Clusters..");
  // ## Cluster (TOPOCLUSTER or SUPERCLUSTER)
  c_clusterIndexCounter = 0;
  c_clusterIndex->clear();
  c_electronIndex_clusterLvl->clear();
  c_clusterEnergy->clear();
  c_clusterTime->clear();
  c_clusterPt->clear();
  c_clusterEta->clear();
  c_clusterPhi->clear();
  // c_clusterEta_calc->clear();
  // c_clusterPhi_calc->clear();
  // c_clusterIndex->clear();
  // cluster shower shapes
  // c_clustere237->clear();
  // c_clustere277->clear();
  // c_clusterfracs1->clear();
  // c_clusterweta2->clear();
  // c_clusterwstot->clear();
  // c_clusterehad1->clear();
  // Cluster cell
  ATH_MSG_DEBUG("Clear Clusters cell..");
  c_cellIndexCounter = 0;
  c_clusterIndex_cellLvl->clear();
  c_clusterCellIndex->clear();
  c_cellGain->clear(); 
  c_cellLayer->clear();
  c_cellRegion->clear();
  c_cellEnergy->clear();
  c_cellEta->clear();
  c_cellPhi->clear();
  c_cellDEta->clear();
  c_cellDPhi->clear();
  c_cellToClusterDPhi->clear();
  c_cellToClusterDEta->clear();  
  //**** Testing variables
  if (pr_testCode){
    c_cellEta_rep->clear();
    c_cellPhi_rep->clear();
    c_cellEnergy_rep->clear();
  }
  c_nCellsInPS->clear();
  c_nCellsInEMB1->clear();
  c_nCellsInEMB2->clear();
  c_nCellsInEMB3->clear();
  //****   
  // Cluster channel
  ATH_MSG_DEBUG("Clear Clusters channel..");
  c_clusterIndex_chLvl->clear();
  c_clusterChannelIndex->clear();
  c_channelHashMap->clear(); // 
  c_channelChannelIdMap->clear();
  c_channelDigits->clear();
  c_channelEnergy->clear();
  c_channelTime->clear();
  if (!pr_noBadCells) c_channelBad->clear();
  c_channelChInfo->clear();
  if (pr_getLArCalibConstants){
    c_channelEffectiveSigma->clear();
    c_channelNoise->clear();
    c_channelDSPThreshold->clear();
    c_channelOFCTimeOffset->clear();
    c_channelADC2MEV0->clear();
    c_channelADC2MEV1->clear();
    c_channelOFCa->clear();
    c_channelOFCb->clear();
    c_channelShapeDer->clear();
    c_channelShape->clear();
  }
  // Cluster raw channel
  ATH_MSG_DEBUG("Clear Clusters rawch..");
  c_rawChannelIdMap->clear();
  c_rawChannelChInfo->clear();
  c_rawChannelAmplitude->clear();
  c_rawChannelTime->clear();
  c_rawChannelPedProv->clear();
  c_rawChannelQuality->clear();
  c_clusterRawChannelIndex->clear();
  c_clusterIndex_rawChLvl->clear();
  c_rawChannelDSPThreshold->clear();
  // c_rawChannelNoise->clear();
  // ############

  // ## Cluster EMB2_711
  c711_clusterIndexCounter = 0;
  c711_clusterIndex->clear();
  c711_electronIndex_clusterLvl->clear();
  c711_clusterEnergy->clear();
  c711_clusterPt->clear();
  c711_clusterTime->clear();
  c711_clusterEta->clear();
  c711_clusterPhi->clear();
  // Cluster cell
  ATH_MSG_DEBUG("Clear Clusters cell..");
  c711_cellIndexCounter = 0;
  c711_clusterIndex_cellLvl->clear();
  c711_clusterCellIndex->clear();
  c711_cellGain->clear(); 
  c711_cellLayer->clear();
  c711_cellRegion->clear();
  c711_cellEnergy->clear();
  c711_cellEta->clear();
  c711_cellPhi->clear();
  c711_cellDEta->clear();
  c711_cellDPhi->clear();
  c711_cellToClusterDPhi->clear();
  c711_cellToClusterDEta->clear();  
  //**** Testing variables
  c711_nCellsInPS->clear();
  c711_nCellsInEMB1->clear();
  c711_nCellsInEMB2->clear();
  c711_nCellsInEMB3->clear();
  //****   
  // Cluster channel
  ATH_MSG_DEBUG("Clear Clusters channel..");
  c711_clusterIndex_chLvl->clear();
  c711_clusterChannelIndex->clear();
  c711_channelHashMap->clear(); // 
  c711_channelChannelIdMap->clear();
  c711_channelDigits->clear();
  c711_channelEnergy->clear();
  c711_channelTime->clear();
  if (!pr_noBadCells) c711_channelBad->clear();
  c711_channelChInfo->clear();
  if (pr_getLArCalibConstants){
    c711_channelEffectiveSigma->clear();
    c711_channelNoise->clear();
    c711_channelDSPThreshold->clear();
    c711_channelOFCTimeOffset->clear();
    c711_channelADC2MEV0->clear();
    c711_channelADC2MEV1->clear();
    c711_channelOFCa->clear();
    c711_channelOFCb->clear();
    c711_channelShapeDer->clear();
    c711_channelShape->clear();
    
  }
  // Cluster raw channel
  ATH_MSG_DEBUG("Clear Clusters rawch..");
  c711_rawChannelIdMap->clear();
  c711_rawChannelChInfo->clear();
  c711_rawChannelAmplitude->clear();
  c711_rawChannelTime->clear();
  c711_rawChannelPedProv->clear();
  c711_rawChannelQuality->clear();
  c711_clusterRawChannelIndex->clear();
  c711_clusterIndex_rawChLvl->clear();
  c711_rawChannelDSPThreshold->clear();
  

  // ############
  // ## Jet
  if (pr_doJetRoiDump){
    ATH_MSG_DEBUG("Clear Jet..");
    j_jetIndex->clear();
    j_jetPt->clear();
    j_jetEta->clear();
    j_jetPhi->clear();
    j_roi_r = pr_roiRadius;  
    // Cell
    j_jetRoiIndex_cellLvl->clear();
    j_jetRoiCellIndex->clear();
    j_cellGain->clear();
    j_cellLayer->clear();
    j_cellRegion->clear();
    //j_cellEnergy->clear();
    j_cellEta->clear();
    j_cellPhi->clear();
    j_cellDEta->clear();
    j_cellDPhi->clear();
    j_cellToJetDPhi->clear();
    j_cellToJetDEta->clear();
    // Channel
    j_jetRoiIndex_chLvl->clear(); // jet ROI index, for each channel index. (they have the same number of inputs)
    j_jetRoiChannelIndex->clear(); // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    j_channelChInfo->clear(); // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    j_channelDigits->clear();  // samples from LAr and Tile cells/channels
    // j_larSamples->clear(); //xxxxxxxxxxx excluir
    // j_tileSamples->clear(); //xxxxxxxxxxx excluir
    j_channelEnergy->clear(); // energy of cell or readout channel inside jet ROI (MeV)
    j_channelTime->clear(); // time of channel inside cluster
    j_channelHashMap->clear(); // 
    if (!pr_noBadCells) j_channelBad->clear(); // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    // Raw channel
    j_rawChannelChInfo->clear(); // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    j_rawChannelAmplitude->clear(); // raw channel energy (adc)
    j_rawChannelTime->clear(); // raw channel time
    j_rawChannelPedProv->clear(); // raw channel estimated pedestal (Tile) or LAr provenance (??)
    j_rawChannelQuality->clear(); // raw channel quality
    j_jetRoiRawChannelIndex->clear(); // raw channel index
    j_jetRoiIndex_rawChLvl->clear(); // jet index at raw channel level
    j_std_rings->clear(); // jet index at raw channel level
  }

  // ## Particle Truth ##
  if (pr_isMC){
    if (pr_doTruthPartDump){
      ATH_MSG_DEBUG("particle truth..");
      // ATH_MSG_DEBUG("Particle Truth");
      mc_part_energy->clear();
      mc_part_pt->clear();
      mc_part_m->clear();
      mc_part_eta->clear();
      mc_part_phi->clear();
      mc_part_pdgId->clear();
      mc_part_status->clear();
      mc_part_barcode->clear();
    }
    // ## Vertex Truth ##
    if (pr_doTruthEventDump){
      mc_vert_x->clear();
      mc_vert_y->clear();
      mc_vert_z->clear();
      mc_vert_time->clear();
      mc_vert_perp->clear();
      mc_vert_eta->clear();
      mc_vert_phi->clear();
      mc_vert_barcode->clear();
      mc_vert_id->clear();
    }
  }
  // ## Photons ##
  if (pr_doPhotonDump){
    ATH_MSG_DEBUG("Clear Photons");
    ph_energy->clear();
    ph_eta->clear();
    ph_phi->clear();
    ph_pt->clear();
    ph_m->clear();
  }

  // ## Electrons ##
  ATH_MSG_DEBUG("Clear: Electrons");
  el_index->clear();
  el_Pt->clear();
  el_et->clear();
  el_Eta->clear();
  el_Phi->clear();
  el_m->clear();
  el_eoverp->clear();
  
  // offline shower shapes 
  el_f1->clear();
  el_f3->clear();
  el_eratio->clear();
  el_weta1->clear();
  el_weta2->clear();
  el_fracs1->clear();
  el_wtots1->clear();
  el_e277->clear();
  el_reta->clear();
  el_rphi->clear();
  el_deltae->clear();
  el_rhad->clear();
  el_rhad1->clear();

  // ## Tag and Probe ##
  ATH_MSG_DEBUG("Clear: T&P");
  electronsSelectedByTrack->clear(); // container not dumped
  // probeElectrons->clear(); // container not dumped
  tAndPElectrons->clear(); // container not dumped
  // tp_electronPt->clear();
  // tp_electronEt->clear();
  // tp_electronEta->clear();
  // tp_electronPhi->clear();
  // tp_probeIndex->clear();
  // tp_tagIndex->clear();
  // tp_isTag->clear();
  // tp_isProbe->clear();
  //zee
  zee_M->clear();
  zee_E->clear();
  zee_pt->clear();
  zee_px->clear();
  zee_py->clear();
  zee_pz->clear();
  zee_T->clear();
  // zee_x->clear();
  // zee_y->clear();
  // zee_z->clear();
  zee_deltaR->clear();


  // ## MyStruc obj
  // ATH_MSG_DEBUG ("Clear: MyStruc obj");
  // myStruct->clear();
  
}

StatusCode EventReaderAlg::finalize() {
  ATH_MSG_DEBUG ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}


// athena.py EventReader_jobOptions.py > reader.log 2>&1; code reader.log
// athena.py EventReader_jobOptions.py > reader_test.log 2>&1; code reader_test.log
// athena.py EventReader_jobOptions.py reader_minBias_manyEvents.log 2>&1
// cd ../build/;make -j;source x86*/setup*;cd ../run/
// cp ../source/EventReader/share/EventReader_jobOptions.py ../run/EventReader_jobOptions.py
// /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/validPlots.py
// /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/saveCaloDict.py