#ifndef EVENTREADER_EVENTREADERALG_H
#define EVENTREADER_EVENTREADERALG_H

#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "AthenaBaseComps/AthAlgorithm.h"
// #include "AthenaBaseComps/AthReentrantAlgorithm.h"

#include "AthenaPoolUtilities/AthenaAttributeList.h"
#include "LArElecCalib/ILArPedestal.h"
#include "LArRawConditions/LArADC2MeV.h"
#include "LArRawConditions/LArDSPThresholdsComplete.h"
#include "LArElecCalib/ILArOFC.h"
#include "LArElecCalib/ILArShape.h" 
#include "CaloConditions/CaloNoise.h"
#include "LArCOOLConditions/LArDSPThresholdsFlat.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"

#include "xAODEgamma/Egamma.h"
#include "xAODEgamma/EgammaxAODHelpers.h"
#include "xAODEgamma/PhotonContainer.h" //xAODEgamma = online and xAODegamma = offline
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
#include "EgammaAnalysisInterfaces/IAsgElectronIsEMSelector.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
// #include "EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h"
// #include "TrigDecisionTool/TrigDecisionTool.h"

#include "LArRawEvent/LArDigitContainer.h"
#include "LArRawEvent/LArDigit.h"
#include "LArRawEvent/LArRawChannelContainer.h"
#include "LArRawEvent/LArRawChannel.h"
#include "TileEvent/TileDigitsContainer.h"
#include "TileEvent/TileDigits.h"
#include "TileEvent/TileDigitsCollection.h"
#include "TileEvent/TileRawChannelContainer.h"
#include "TileEvent/TileRawChannelCollection.h"
#include "TileEvent/TileRawChannel.h"
#include "TileEvent/TileCell.h"

#include "Identifier/Identifier.h"
#include "Identifier/HWIdentifier.h"
#include "LArIdentifier/LArOnlineID.h"
// #include "LArCabling/LArCablingService.h" 
#include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"
#include "TileConditions/TileCablingSvc.h"
#include "CaloIdentifier/CaloCell_ID.h"
// #include "CaloIdentifier/TileHWID.h"
// #include "CaloIdentifier/TileID.h"

#include <TH1.h>
#include <TTree.h>

#include <bitset>

#include "CaloDetDescr/CaloDetDescrManager.h"
// #include "CaloUtils/CaloCellList.h"
// #include "CaloGeoHelpers/CaloSampling.h"
// #include "CaloEvent/CaloCell.h"
// #include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
// #include "xAODTrigRinger/TrigRingerRingsContainer.h"
// #include <xAODCaloRings/versions/RingSet_v1.h>
// #include <xAODCaloRings/RingSetContainer.h>
// #include <xAODEgamma/versions/Electron_v1.h>
// #include <xAODEgamma/ElectronContainer.h>
// #include "xAODTruth/TruthParticle.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "xAODTruth/xAODTruthHelpers.h"
// #include <xAODCaloRings/versions/CaloRings_v1.h>
// #include <xAODCaloRings/CaloRingsContainer.h>

// typedef struct _objectInfo{
//   float electron_pt;
//   float cluster_pt;
//   float cluster_eta;
//   float cluster_phi;
//   float cluster_et;
//   // std::vector <float> cell_digits;
//   // std::vector <float> channel_energy;
// } ObjectInfo;


// using namespace std;
// class LArDigitContainer;
class LArOnOffIdMapping;
class TileCablingSvc;
// class TileOnOffIdMapping;
class CaloCellContainer;
class CaloCellList;


// class EventReaderAlg: public ::AthReentrantAlgorithm 
class EventReaderAlg: public ::AthAlgorithm 
{ 
// The structure here is built to separate the implementation from de header file. 
// So, the implementation goes in another file, a *.cxx one. 
 public: 
    EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ); // changed here
    // using AthReentrantAlgorithm::AthReentrantAlgorithm;

    virtual ~EventReaderAlg();  // changed here

    virtual StatusCode        initialize() override;     //once, before any input is loaded
    virtual StatusCode        execute() override;        //Access via EventInfo
    // virtual StatusCode        execute(const EventContext& ctx) const override;        //Access via EventContext pointer  // changed here (from virtual to non virtual )
    virtual StatusCode        finalize() override;       //once, after all events processed
    void                      clear();
    void                      bookBranches(TTree *tree);
    int                       getCaloRegionIndex(const CaloCell* cell); //customized representative indexes of calo region
    double                    fixPhi(double phi);
    double                    deltaPhi(double phi1 , double phi2);
    double                    deltaR( double eta, double phi);
    bool                      isEtaOutsideLArCrack(float absEta);
    virtual StatusCode        dumpEventInfo(const xAOD::EventInfo *ei);
    virtual StatusCode        dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling); // dump the entire cluster container
    virtual StatusCode        dumpClusterCells(const xAOD::CaloCluster *cl, int clusIndex); // dump only the cluster and its cells
    virtual StatusCode        dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg, const LArOnOffIdMapping* larCabling );
    virtual StatusCode        buildRingsJets(const xAOD::JetContainer *jets, const std::string& jetAlg);
    int                       setLayerID(const int samp);
    virtual StatusCode        dumpOfflineSS(const xAOD::Electron *electron);
    virtual StatusCode        dumpTruthParticle();
    virtual StatusCode        dumpTruthEvent();
    virtual StatusCode        dumpPhotons(const xAOD::PhotonContainer* photonCnt);
    virtual StatusCode        dumpElectrons(const xAOD::Electron* electron);
    virtual StatusCode        dumpZeeCut(const xAOD::EventInfo *ei);
    bool                      isTagElectron(const xAOD::Electron* electron);
    bool                      isGoodProbeElectron(const xAOD::Electron* el);
    bool                      trackSelectionElectrons(const xAOD::Electron *electron, const xAOD::VertexContainer *primVertexCnt); // for Xtalk studies
    bool                      eOverPElectron(const xAOD::Electron* electron);
    int                       getNumberCellsInClusterSampling(int CaloSamplingIndex, std::vector < int > * cellLayerVector);
    void                      findHottestCellInClusterSampling(const xAOD::CaloCluster* cl, CaloCell* &hotCell , CaloSampling::CaloSample samp);
    virtual StatusCode        buildFixedSizeClusterInSampling(std::vector<const CaloCell*> &cellCollectionROI , CaloCell* hotCell, CaloSampling::CaloSample samp, double etaSize, double phiSize);
    virtual StatusCode        dumpCellsInFixedSizeROI(const xAOD::CaloCluster *cl, int clusIndex);
    virtual StatusCode        initializeCalibConstantsTools();

    virtual StatusCode        testCode(const xAOD::CaloCluster *cl);

  private: 
    Gaudi::Property<std::string> pr_clusterName {this, "clusterName" , "CaloCalTopoClusters" ,  "Name of the cluster container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_jetName {this, "jetName" , "AntiKt4EMPFlowJets" ,  "Name of the jet container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_tileDigName {this, "tileDigName" , "TileDigitsCnt" ,  "Name of the Tile digits container that will have its values dumped."};
    Gaudi::Property<std::string> pr_larDigName {this, "larDigName" , "LArDigitContainer_MC" ,  "Name of the LAr digits container that will have its values dumped."};    
    Gaudi::Property<std::string> pr_tileRawName {this, "tileRawName" , "TileRawChannelCnt" ,  "Name of the Tile raw channel container that will have its values dumped."};
    Gaudi::Property<std::string> pr_larRawName {this, "larRawName" , "LArRawChannels" ,  "Name of the LAr raw channel container that will have its values dumped."};
    Gaudi::Property<std::string> pr_offTagTightness {this, "offTagTightness", "LHMedium"}; /*! Define the PID for tag electron */
    Gaudi::Property<std::string> pr_offProbeTightness {this, "offProbeTightness", "Loose"}; /*! define the Pid of Probe from the user */

    Gaudi::Property<bool> pr_doTile {this, "doTile", true, "Dump the tile cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_noBadCells {this, "noBadCells", true, "If True, skip the cells tagged as badCells/channels."};
    Gaudi::Property<bool> pr_doLAr {this, "doLAr", true, "Dump the LAr cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_printCellsClus {this, "printCellsClus", false, "Print out the cluster cells basic info during the dump."}; 
    Gaudi::Property<bool> pr_printCellsJet {this, "printCellsJet", false, "Print out the jet ROI cells basic info during the dump of Jet ROI."};
    Gaudi::Property<bool> pr_testCode {this, "testCode", false, "Execute testing code function."};
    Gaudi::Property<bool> pr_isMC {this, "isMC", false, "Switch the dumper to MC sample mode."};
    Gaudi::Property<bool> pr_doTruthEventDump {this, "doTruthEventDump", false, "Dump the Truth Event variables."};
    Gaudi::Property<bool> pr_doTagAndProbe {this, "doTagAndProbe", true, "First, the electrons are selected by track criteria, then, perform Tag and Probe selection for Zee."};
    Gaudi::Property<bool> pr_doElecSelectByTrackOnly {this, "doElecSelectByTrackOnly", false, "Perform electron selection by track only, witout tag and probe."};
    Gaudi::Property<bool> pr_doJetRoiDump {this, "doJetRoiDump", true, "Perform a jet roi cell dump based on Jet Container name pr_jetName."};
    Gaudi::Property<bool> pr_doClusterDump {this, "doClusterDump", false, "Perform a cluster cell dump based on Cluster Container name pr_clusterName"};
    Gaudi::Property<bool> pr_doPhotonDump {this, "doPhotonDump", false, "Perform a photon particle dump based on offline Photons Container."};
    Gaudi::Property<bool> pr_doTruthPartDump {this, "doTruthPartDump", false, "Perform a truth particle dump."};
    Gaudi::Property<bool> pr_getAssociatedTopoCluster {this, "getAssociatedTopoCluster", true, "Get the topo cluster associated to a super cluster, which was linked to an Electron."};
    Gaudi::Property<bool> pr_getLArCalibConstants {this, "getLArCalibConstants", false, "Get the LAr calorimeter calibration constants, related to cells energy and time (online and offline)."};
  
    Gaudi::Property<float> pr_roiRadius {this, "roiRadius" , 0.5 ,  "Radius of the jets region of interest."};
    Gaudi::Property<float> pr_etMinProbe {this, "etMinProbe", 15 ,"Min electron Pt value for Zee probe selection loose (GeV)."}; // Et or pT ?
    Gaudi::Property<float> pr_etMinTag {this, "etMinTag", 15 ,"Min Et value for the electrons in Zee tag selection (GeV)."}; // Et?
    Gaudi::Property<float> pr_etMaxTag {this, "etMaxTag", 180 ,"Max Et value for the electrons in Zee tag selection (GeV)."}; // Et or pT ?
    Gaudi::Property<float> pr_minZeeMassTP {this, "minZeeMassTP", 66, "Minimum value of Zee mass for checking the TP pairs (GeV)."};
    Gaudi::Property<float> pr_maxZeeMassTP {this, "maxZeeMassTP", 116, "Maximum value of Zee mass for checking the TP pairs (GeV)."};
    Gaudi::Property<float> pr_d0TagSig {this, "d0TagSig", 5, "d_0 transverse impact parameter significance."};
    Gaudi::Property<float> pr_z0Tag {this, "z0Tag", 0.5, "z0 longitudinal impact parameter (mm)"};// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TrackingCPEOYE2015

  //##################
  // Services and Keys
  //##################

    ServiceHandle<ITHistSvc> m_ntsvc;

    // CaloNoise Conditions data object.
    // Typical values are '"electronicNoise', 'pileupNoise', or 'totalNoise' (default)
    SG::ReadCondHandleKey<CaloNoise> m_noiseCDOKey;//{this,"CaloNoiseKey","totalNoise","SG Key of CaloNoise data object"};
    const CaloNoise* noiseCDO;

    SG::ReadCondHandleKey<AthenaAttributeList> m_run2DSPThresholdsKey{this, "Run2DSPThresholdsKey","", "SG Key for thresholds to compute time and quality, run 2"};
    std::unique_ptr<LArDSPThresholdsFlat> run2DSPThresh;

    //Conditions input:
    SG::ReadCondHandleKey<ILArPedestal> m_pedestalKey{this,"PedestalKey","LArPedestal","SG Key of Pedestal conditions object"};
    SG::ReadCondHandleKey<LArADC2MeV>   m_adc2MeVKey{this,"ADC2MeVKey","LArADC2MeV","SG Key of ADC2MeV conditions object"};
    SG::ReadCondHandleKey<ILArOFC>      m_ofcKey{this,"OFCKey","LArOFC","SG Key of OFC conditions object"};
    SG::ReadCondHandleKey<ILArShape>    m_shapeKey{this,"ShapeKey","LArShape","SG Key of Shape conditions object"};
    
    const ILArPedestal* peds;
    const LArADC2MeV*   adc2MeVs;
    const ILArOFC*      ofcs;
    const ILArShape*    shapes;

    // LAr Tools
    const LArOnlineID* m_onlineLArID; //from detector store LAr    
    // SG::ReadCondHandleKey<LArOnOffIdMapping> m_larCablingKey;
    SG::ReadCondHandleKey<LArOnOffIdMapping> m_larCablingKey{this,"LArOnOffMapKey","LArOnOffIdMap"," SG Key of LArOnOffIdMapping object."};
    const LArOnOffIdMapping* larCabling;
    // ToolHandleArray<IAsgElectronIsEMSelector> m_electronIsEMTool { this, "ElectronIsEMSelector", {}};/*! Offline isEM Selectors */
    // ToolHandleArray<IAsgElectronLikelihoodTool> m_electronLHTool { this, "ElectronLikelihoodTool", {}};/*! Offline LH Selectors */
    // SG::ReadCondHandleKey<LArOnOffIdMapping> m_cablingKey{this,"CablingKey","LArOnOffIdMap","SG Key of LArOnOffIdMapping object"};
    // const LArOnOffIdMapping* larCabling=*larCablingHdl;

    // TileCal Tools    
    ServiceHandle<TileCablingSvc> m_tileCablingSvc { this, "TileCablingSvc", "TileCablingSvc", "Tile cabling service"}; //Name of Tile cabling service    
    const TileHWID* m_tileHWID;//Atlas detector Identifier for Tile Calorimeter online (hardware) identifiers
    const TileID* m_tileID; //Atlas detector Identifier class for Tile Calorimeter offline identifiers
    const TileCablingService* m_tileCabling;
    const CaloCell_ID* m_calocell_id;
    
  //#############################
  // Global Variables in general
  //#############################

    TTree *m_Tree;
    // Sampling regions
    std::vector < CaloSampling::CaloSample > sampRegions{CaloSampling::PreSamplerB, CaloSampling::EMB1, CaloSampling::EMB2, CaloSampling::EMB3, CaloSampling::PreSamplerE, CaloSampling::EME1, CaloSampling::EME2, CaloSampling::EME3, CaloSampling::HEC0, CaloSampling::HEC1, CaloSampling::HEC2, CaloSampling::HEC3, CaloSampling::TileBar0, CaloSampling::TileBar1, CaloSampling::TileBar2, CaloSampling::TileGap1, CaloSampling::TileGap2, CaloSampling::TileGap3, CaloSampling::TileExt0, CaloSampling::TileExt1, CaloSampling::TileExt2, CaloSampling::FCAL0, CaloSampling::FCAL1, CaloSampling::FCAL2};
    // std::vector < CaloSampling::CaloSample > sampRegions{CaloSampling::EMB2};

  //##################
  // Dumped Variables
  //##################

    // ## EventInfo ##
    unsigned int        e_runNumber         = 9999; //< Run number
    unsigned int        e_bcid              = 9999; //< BCID number
    unsigned long long  e_eventNumber       = 9999; //< Event number
    // unsigned int        e_lumiBlock         = 9999; // lumiblock number (BUGGED !)
    float               e_inTimePileup      = -999;  //< avg_mu
    float               e_outOfTimePileUp   = -999; // OOT pileup


    // ## Cluster (TOPOCLUSTER) ##
    int c_clusterIndexCounter = 0; // cluster index counter, for the entire event
    std::vector < int > *c_clusterIndex = nullptr; // cluster index, for each cluster in the same event. (they have the same number of inputs)
    std::vector < int > *c_electronIndex_clusterLvl = nullptr; // electron index, for each cluster in the same event.
    std::vector < double > *c_clusterEnergy = nullptr; //energy of the cluster
    std::vector < double > *c_clusterTime = nullptr; //timing of the cluster
    std::vector < double > *c_clusterEta = nullptr; // clus. baricenter eta
    std::vector < double > *c_clusterPhi = nullptr; // clus. baricenter phi
    // std::vector < double > *c_clusterEta_calc = nullptr; // clus. baricenter eta calculated
    // std::vector < double > *c_clusterPhi_calc = nullptr; // clus. baricenter phi calculated
    std::vector < double > *c_clusterPt = nullptr; // clus. baricenter eta
    // cluster shower shapes
    // std::vector < float > *c_clustere237    = nullptr;
    // std::vector < float > *c_clustere277    = nullptr;
    // std::vector < float > *c_clusterfracs1  = nullptr;
    // std::vector < float > *c_clusterweta2   = nullptr;
    // std::vector < float > *c_clusterwstot   = nullptr;
    // std::vector < float > *c_clusterehad1   = nullptr;
    // Cell
    int c_cellIndexCounter = 0; // cell index counter inside a cluster, for the entire event
    std::vector < int > *c_clusterIndex_cellLvl = nullptr; // cluster index, for each cell index. (they have the same number of inputs)
    std::vector < int > *c_clusterCellIndex = nullptr; // cell index inside a cluster
    std::vector < int > *c_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < int > *c_cellLayer = nullptr; // layer index of cell signal, from caloDDE.
    std::vector < int > *c_cellRegion = nullptr; // region of calorimeter system (custom index from 'getCaloRegionIndex')
    std::vector < double > *c_cellEnergy = nullptr; // cell inside cluster energy
    std::vector < double > *c_cellEta = nullptr; // cell inside cluster baricenter eta
    std::vector < double > *c_cellPhi = nullptr; // cell inside cluster baricenter phi
    std::vector < double > *c_cellDEta = nullptr; // cell inside cluster delta_eta (granularity)
    std::vector < double > *c_cellDPhi = nullptr; // cell inside cluster delta_phi (granularity)
    std::vector < double > *c_cellToClusterDPhi = nullptr; // cell inside cluster delta_phi distance to cluster baricenter.
    std::vector < double > *c_cellToClusterDEta = nullptr; // cell inside cluster delta_eta distance to cluster baricenter.
    //**** Testing variables
    std::vector < double > *c_cellEta_rep = nullptr; // cell inside cluster baricenter eta
    std::vector < double > *c_cellPhi_rep = nullptr; // cell inside cluster baricenter phi
    std::vector < double > *c_cellEnergy_rep = nullptr; // cell inside cluster energy
    std::vector < int > *c_nCellsInPS   = nullptr; // count how many cells are in a chosen cluster sampling layer, for each cluster
    std::vector < int > *c_nCellsInEMB1 = nullptr; 
    std::vector < int > *c_nCellsInEMB2 = nullptr; 
    std::vector < int > *c_nCellsInEMB3 = nullptr; 
    std::vector < int > *c_clusSizeEnum   = nullptr; 
    // std::vector < int > *c_cellLayerEgamma711 = nullptr; // for a DataVector<xAOD::CaloCluster_v1>_egamma711Clusters
    //****
    // Channel
    std::vector < int >                   *c_clusterIndex_chLvl = nullptr; // cluster index, for each channel index. (they have the same number of inputs)
    std::vector < float >                 *c_clusterChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > >   *c_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    std::vector < std::vector < float > > *c_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < double >                *c_channelEnergy = nullptr; // energy of cell or readout channel inside cluster (MeV)
    std::vector < double >                *c_channelTime = nullptr; // time of channel inside cluster (VERIFICAR A UNIDADE !!!!!!!!!!!!)
    std::vector < bool >                  *c_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    std::vector <  unsigned int  >        *c_channelHashMap = nullptr; //cell map of ALL cells inside a cluster. id 0x2d214a140000000. is a 64-bit number that represent the cell.
    std::vector <  unsigned int  >        *c_channelChannelIdMap = nullptr;
    std::vector < float >                 *c_channelEffectiveSigma  = nullptr;
    std::vector < float >                 *c_channelNoise  = nullptr;
    std::vector < float >                 *c_channelDSPThreshold  = nullptr; // get from Athena POOL Utilities
    std::vector < std::vector < double > > *c_channelOFCa = nullptr;
    std::vector < std::vector < double > > *c_channelOFCb = nullptr;
    std::vector < std::vector < double > > *c_channelShape = nullptr; // shape
    std::vector < std::vector < double > > *c_channelShapeDer = nullptr; // shape derivative
    std::vector < double >                *c_channelOFCTimeOffset = nullptr; 
    std::vector < float >                 *c_channelADC2MEV0 = nullptr;
    std::vector < float >                 *c_channelADC2MEV1 = nullptr;
    // Raw channel
    std::vector < std::vector < int > > *c_rawChannelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
    std::vector <  unsigned int  >      *c_rawChannelIdMap = nullptr;
    std::vector < float >               *c_rawChannelAmplitude = nullptr; // raw channel energy (adc)
    std::vector < float >               *c_rawChannelTime = nullptr; // raw channel time (VERIFICAR A UNIDADE !!!!!!!!!!!!!)
    std::vector < float >               *c_rawChannelPedProv = nullptr; // raw channel estimated pedestal (Tile) or LAr provenance (??)
    std::vector < float >               *c_rawChannelQuality = nullptr; // raw channel quality
    std::vector < float >               *c_clusterRawChannelIndex = nullptr; // raw channel index
    std::vector < int >                 *c_clusterIndex_rawChLvl = nullptr; // cluster index at raw channel level
    std::vector < float >               *c_rawChannelDSPThreshold  = nullptr; // get from Athena POOL Utilities
    // std::vector < float >               *c_rawChannelNoise  = nullptr;


    // ## Cluster (EMB2 EGAMMA CALO CLUSTER 7_11) ##
    int c711_clusterIndexCounter = 0; // cluster index counter, for the entire event
    std::vector < int >     *c711_clusterIndex = nullptr; // cluster index, for each cluster in the same event. (they have the same number of inputs)
    std::vector < int >     *c711_electronIndex_clusterLvl = nullptr; // electron index, for each cluster in the same event.
    std::vector < double >  *c711_clusterEnergy = nullptr; //energy of the cluster
    std::vector < double >  *c711_clusterTime = nullptr; //timing of the cluster
    std::vector < double >  *c711_clusterEta = nullptr; // clus. baricenter eta
    std::vector < double >  *c711_clusterPhi = nullptr; // clus. baricenter phi
    std::vector < double >  *c711_clusterPt = nullptr; // clus. baricenter eta

    // Cell
    int c711_cellIndexCounter = 0; // cell index counter inside a cluster, for the entire event
    std::vector < int >     *c711_clusterIndex_cellLvl = nullptr; // cluster index, for each cell index. (they have the same number of inputs)
    std::vector < int >     *c711_clusterCellIndex = nullptr; // cell index inside a cluster
    std::vector < int >     *c711_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < int >     *c711_cellLayer = nullptr; // layer index of cell signal, from caloDDE.
    std::vector < int >     *c711_cellRegion = nullptr; // region of calorimeter system (custom index from 'getCaloRegionIndex')
    std::vector < double >  *c711_cellEnergy = nullptr; // cell inside cluster energy
    std::vector < double >  *c711_cellEta = nullptr; // cell inside cluster baricenter eta
    std::vector < double >  *c711_cellPhi = nullptr; // cell inside cluster baricenter phi
    std::vector < double >  *c711_cellDEta = nullptr; // cell inside cluster delta_eta (granularity)
    std::vector < double >  *c711_cellDPhi = nullptr; // cell inside cluster delta_phi (granularity)
    std::vector < double >  *c711_cellToClusterDPhi = nullptr; // cell inside cluster delta_phi distance to cluster baricenter.
    std::vector < double >  *c711_cellToClusterDEta = nullptr; // cell inside cluster delta_eta distance to cluster baricenter.
    // Channel
    std::vector < int >                   *c711_clusterIndex_chLvl = nullptr; // cluster index, for each channel index. (they have the same number of inputs)
    std::vector < float >                 *c711_clusterChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > >   *c711_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    std::vector < std::vector < float > > *c711_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < double >                *c711_channelEnergy = nullptr; // energy of cell or readout channel inside cluster (MeV)
    std::vector < double >                *c711_channelTime = nullptr; // time of channel inside cluster (VERIFICAR A UNIDADE !!!!!!!!!!!!)
    std::vector < bool >                  *c711_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    std::vector <  unsigned int  >        *c711_channelHashMap = nullptr; //cell map of ALL cells inside a cluster. id 0x2d214a140000000. is a 64-bit number that represent the cell.
    std::vector <  unsigned int  >        *c711_channelChannelIdMap = nullptr;
    std::vector < float >                 *c711_channelEffectiveSigma  = nullptr;
    std::vector < float >                 *c711_channelNoise  = nullptr;
    std::vector < float >                 *c711_channelDSPThreshold  = nullptr; // get from Athena POOL Utilities
    std::vector < std::vector < double > > *c711_channelOFCa = nullptr; 
    std::vector < std::vector < double > > *c711_channelOFCb = nullptr; 
    std::vector < std::vector < double > > *c711_channelShape = nullptr; // shape
    std::vector < std::vector < double > > *c711_channelShapeDer = nullptr; // shape derivative
    std::vector < double >                *c711_channelOFCTimeOffset = nullptr; 
    std::vector < float >                 *c711_channelADC2MEV0 = nullptr;
    std::vector < float >                 *c711_channelADC2MEV1 = nullptr;
    // ***** Testing Variables
    std::vector < int > *c711_nCellsInPS   = nullptr; // count how many cells are in a chosen cluster sampling layer, for each cluster
    std::vector < int > *c711_nCellsInEMB1 = nullptr; 
    std::vector < int > *c711_nCellsInEMB2 = nullptr; 
    std::vector < int > *c711_nCellsInEMB3 = nullptr; 
    std::vector < int > *c711_clusSizeEnum   = nullptr; 
    // *****
    // Raw channel
    std::vector < std::vector < int > > *c711_rawChannelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
    std::vector <  unsigned int  >      *c711_rawChannelIdMap = nullptr;
    std::vector < float >               *c711_rawChannelAmplitude = nullptr; // raw channel energy (adc)
    std::vector < float >               *c711_rawChannelTime = nullptr; // raw channel time (VERIFICAR A UNIDADE !!!!!!!!!!!!!)
    std::vector < float >               *c711_rawChannelPedProv = nullptr; // raw channel estimated pedestal (Tile) or LAr provenance (??)
    std::vector < float >               *c711_rawChannelQuality = nullptr; // raw channel quality
    std::vector < float >               *c711_clusterRawChannelIndex = nullptr; // raw channel index
    std::vector < int >                 *c711_clusterIndex_rawChLvl = nullptr; // cluster index at raw channel level
    std::vector < float >               *c711_rawChannelDSPThreshold  = nullptr; // get from  Athena POOL utilities
    // std::vector < float >               *c711_rawChannelNoise  = nullptr;

    // ################
    // ##  Jets  ##
    std::vector < int >   *j_jetIndex = nullptr; // jet index, for each cluster in the same event.
    std::vector < float > *j_jetPt = nullptr; // transverse momentum of jets 
    std::vector < float > *j_jetEta = nullptr; // vector to store jets eta 
    std::vector < float > *j_jetPhi = nullptr; // vector to store jets phi
    float j_roi_r;
    // Cell
    std::vector < int > *j_jetRoiIndex_cellLvl = nullptr; // jet index, for each cell index. (they have the same number of inputs)
    std::vector < int > *j_jetRoiCellIndex = nullptr; // cell index inside a jet ROI
    std::vector < int > *j_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < int > *j_cellLayer = nullptr; // layer index of cell signal, from caloDDE (sampling).
    std::vector < int > *j_cellRegion = nullptr; // region of calorimeter system (custom index from 'getCaloRegionIndex')
    std::vector < double > *j_cellEnergy = nullptr; // energy of a cell inside jet ROI
    std::vector < double > *j_cellEta = nullptr; // cell inside jet ROI baricenter eta
    std::vector < double > *j_cellPhi = nullptr; // cell inside jet ROI baricenter phi
    std::vector < double > *j_cellDEta = nullptr; // cell inside jet ROI delta_eta (granularity)
    std::vector < double > *j_cellDPhi = nullptr; // cell inside jet ROI delta_phi (granularity)
    std::vector < double > *j_cellToJetDPhi = nullptr; // cell inside jet ROI delta_phi distance to jet eta/phi.
    std::vector < double > *j_cellToJetDEta = nullptr; // cell inside jet ROI delta_eta distance to jet eta/phi.
    // Channel
    std::vector < int > *j_jetRoiIndex_chLvl = nullptr; // jet ROI index, for each channel index. (they have the same number of inputs)
    std::vector < float > *j_jetRoiChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > > *j_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    std::vector < std::vector < float > > *j_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < std::vector < short > > *j_larSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < std::vector < float > > *j_tileSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < double > *j_channelEnergy = nullptr; // energy of cell or readout channel inside jet ROI (MeV)
    std::vector < double > *j_channelTime = nullptr; // time of channel inside cluster (VERIFICAR A UNIDADE !!!!!!!!!!!!)
    std::vector < bool > *j_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    std::vector <  unsigned int  > *j_channelHashMap = nullptr; //cell map of ALL cells inside a jet ROI.
    // Raw channel
    std::vector < std::vector < int > > *j_rawChannelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
    std::vector < float > *j_rawChannelAmplitude = nullptr; // raw channel energy (adc)
    std::vector < float > *j_rawChannelTime = nullptr; // raw channel time (VERIFICAR A UNIDADE !!!!!!!!!!!!!)
    std::vector < float > *j_rawChannelPedProv = nullptr; // raw channel estimated pedestal (Tile) or LAr provenance (??)
    std::vector < float > *j_rawChannelQuality = nullptr; // raw channel quality
    std::vector < float > *j_jetRoiRawChannelIndex = nullptr; // raw channel index
    std::vector < int > *j_jetRoiIndex_rawChLvl = nullptr; // jet index at raw channel level
    std::vector < std::vector < float > > *j_std_rings = nullptr; // jet index at raw channel level
    
    // ## Particle Truth ##
    std::vector < float > *mc_part_energy   = nullptr;
    std::vector < float > *mc_part_pt       = nullptr;
    std::vector < float > *mc_part_m        = nullptr;
    std::vector < float > *mc_part_eta      = nullptr;
    std::vector < float > *mc_part_phi      = nullptr;    
    std::vector < int >   *mc_part_pdgId    = nullptr;
    std::vector < int >   *mc_part_status   = nullptr;
    std::vector < int >   *mc_part_barcode  = nullptr;

    // ## Vertex Truth ##
    std::vector < float > *mc_vert_x     = nullptr;
    std::vector < float > *mc_vert_y     = nullptr;
    std::vector < float > *mc_vert_z     = nullptr;
    std::vector < float > *mc_vert_time  = nullptr;
    std::vector < float > *mc_vert_perp  = nullptr;
    std::vector < float > *mc_vert_eta   = nullptr;
    std::vector < float > *mc_vert_phi   = nullptr;
    std::vector < int > *mc_vert_barcode = nullptr;
    std::vector < int > *mc_vert_id      = nullptr;
  
    // ## Photons Reco ##
    std::vector < float > *ph_eta     = nullptr;
    std::vector < float > *ph_phi     = nullptr;
    std::vector < float > *ph_pt      = nullptr;
    std::vector < float > *ph_energy  = nullptr;
    std::vector < float > *ph_m       = nullptr;
    // std::vector < float > *ph_xxx       = nullptr;
    // std::vector < int > *ph_origin  = nullptr;
    // std::vector < int > *ph_type    = nullptr;

    // ## Electrons ##
    std::vector < int >   *el_index   = nullptr;
    std::vector < float > *el_Pt      = nullptr;
    std::vector < float > *el_et      = nullptr;
    std::vector < float > *el_Eta     = nullptr;
    std::vector < float > *el_Phi     = nullptr;
    std::vector < float > *el_m       = nullptr;
    std::vector < float > *el_eoverp  = nullptr;
      // offline shower shapes
    std::vector < float > *el_f1  = nullptr;
    std::vector < float > *el_f3  = nullptr;
    std::vector < float > *el_eratio = nullptr;
    std::vector < float > *el_weta1 = nullptr;  
    std::vector < float > *el_weta2 = nullptr;  
    std::vector < float > *el_fracs1 = nullptr; 
    std::vector < float > *el_wtots1 = nullptr; 
    std::vector < float > *el_e277 = nullptr; 
    std::vector < float > *el_reta = nullptr; 
    std::vector < float > *el_rphi = nullptr; 
    std::vector < float > *el_deltae = nullptr; 
    std::vector < float > *el_rhad = nullptr;  
    std::vector < float > *el_rhad1 = nullptr; 

    // ## Tag and probe ##
    xAOD::ElectronContainer* electronsSelectedByTrack = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    // xAOD::ElectronContainer* probeElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    xAOD::ElectronContainer* tAndPElectrons = new xAOD::ElectronContainer(SG::VIEW_ELEMENTS);
    std::vector < float > *tp_electronPt = nullptr;
    std::vector < float > *tp_electronEt = nullptr;
    std::vector < float > *tp_electronEta = nullptr;
    std::vector < float > *tp_electronPhi = nullptr;
    // indexes
    std::vector < int >   *tp_probeIndex = nullptr; // contains the electron indexes associated to each electron (tags). The vector position represent the Tags, the number on it, the associated Probe.
    std::vector < int >   *tp_tagIndex = nullptr; // index for each electron in the container
    std::vector < bool >  *tp_isTag = nullptr;
    std::vector < bool >  *tp_isProbe = nullptr;
    // Zee
    std::vector < double >  *zee_M = nullptr;
    std::vector < double >  *zee_E = nullptr;
    std::vector < double >  *zee_pt = nullptr;
    std::vector < double >  *zee_px = nullptr;
    std::vector < double >  *zee_py = nullptr;
    std::vector < double >  *zee_pz = nullptr;
    std::vector < double >  *zee_T = nullptr;
    // std::vector < double >  *zee_x = nullptr;
    // std::vector < double >  *zee_y = nullptr;
    // std::vector < double >  *zee_z = nullptr;
    std::vector < double >  *zee_deltaR = nullptr;

   

    //## MyObj
    // std::vector < ObjectInfo > *myStruct = new std::vector < ObjectInfo>;

// protected:
//   SG::ReadCondHandleKey<CaloDetDescrManager> m_caloMgrKey{this,"CaloDetDescrManager", "CaloDetDescrManager"}; 
//   SG::ReadHandleKey<CaloCellContainer> m_cellsContName {this, "CellsContainerName", "AllCalo","Key to obtain the cell container"};

};


#endif 
