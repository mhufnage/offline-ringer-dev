## This file goes INSIDE a package directory.
## Its responsible to point to all files within the project, in order to compile them.
## automatically generated CMakeLists.txt file

# Declare the package
# ATLAS_SUBDIR( EventReader )
atlas_subdir( EventReader )

#find_package( COMPONENTS )
find_package( ROOT COMPONENTS MathCore RIO Core Tree Hist)

# Add binary
# ATLAS_ADD_LIBRARY ( EventReaderLib EventReader/EventReaderAlg.h Root/EventReaderAlg.cxx #Root/components/*.cxx
# 		  PUBLIC_HEADERS EventReader
#           LINK_LIBRARIES xAODEventInfo
#                  )

atlas_add_component ( EventReader EventReader/EventReaderAlg.h Root/EventReaderAlg.cxx Root/components/*.cxx
       PUBLIC_HEADERS EventReader
       INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS}
       LINK_LIBRARIES ${ROOT_INCLUDE_DIRS} ${Boost_INCLUDE_DIRS} AthenaBaseComps AthenaPoolUtilities CaloDetDescrLib xAODEventInfo xAODCaloEvent xAODTruth xAODEgamma CaloEvent LArIdentifier LArRecEvent LArCablingLib LArCOOLConditions LArDigitizationLib LArElecCalib CaloUtilsLib AthContainers Identifier LArRawEvent xAODJet TileEvent TileIdentifier TileConditionsLib xAODTracking EgammaAnalysisInterfacesLib CaloConditions 
       #xAODTrigEgamma
)

# Install python modules, joboptions, and share content
atlas_install_python_modules( python/*.py )
atlas_install_joboptions( share/*.py )
