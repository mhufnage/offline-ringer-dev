#ascii
import ROOT
import numpy as np
# import pandas as pd
# from ROOT import TH1F, TTree, TChain, TFile, gROOT
from glob import glob
from time import time
# from histHelper import histLayerRegionFixBin, histLayerDynBin, histTH1F, histTH1D, histTH2F, plotTH1F, plotTH2F  #custom
from functionsHelper import getRegionString, getSamplingString, getCaloGainString ,stdVecToArray, stdVecOfStdVecToArrayOfList, indexConversion, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, save_pickle, load_pickle #custom
import math
import argparse
import logging
import os, sys

ROOT.gROOT.SetBatch(True)
# ROOT.gROOT.SetStyle("ATLAS")
################################

############################
#### PARSE ARGUMENTS   #####
############################
# Usage example: 
# python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/EventReader/share/createXTalkDataset.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/minbias_getOFCandRamp_new.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias_getOFCandRamp_new' --fileAlias='dataMinBias_getOFCandRamp_part' --events=-1 --splitEvents=30

# parse arguments good tutorial found at https://levelup.gitconnected.com/the-easy-guide-to-python-command-line-arguments-96b4607baea1
parser = argparse.ArgumentParser(description='Script to read the dumped file from EventReader, and generate the validation plots as TH1F in *.root files.')
parser.add_argument("--fileAlias", type=str, default='test_', help='name alias to generated *.root files. ( <fileAlias>_001.npz )')
parser.add_argument("--events", type=int, default=-1, help='amount of events to be processed. Default is all the events pointed by the file directory.')
parser.add_argument("--splitEvents", type=int, default=-1, help='amount of FILES to save the whole collection of events. Default is all the events in same file.')
parser.add_argument("--outputFilesPath", type=str, default='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts', help='path for the output *.npz files. It may create folders in this dir.')
parser.add_argument("--inputFilesPath", type=str, default='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root', help='path for the file(s). It accepts the glob (*) notation.')
# parser.add_argument("--outputFolder", type=str, default='singlePi0', help='name alias the output histograms, related to dumped data.')

args = parser.parse_args()

# n_process = args.n_process
parse_aliasName         = args.fileAlias
parse_events            = args.events
parse_splitEvents       = args.splitEvents
outputFilesPath         = args.outputFilesPath
inputFilesPath          = args.inputFilesPath

########################################
#######  PATHS AND FILENAMES    ########
########################################
inputFileName   = glob('{}'.format(inputFilesPath)) # ntuple_singlePi0_Truth000001
outputDir       = '{}/'.format(outputFilesPath) #lxplus
outputDirHist   = outputDir+'histograms/'
fDictLayerName  = 'dictCaloByLayer.json'
# inputFileName = '/home/mhufnagel/cernbox/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuple.root' #local

if not(os.path.isdir(outputDir)):
    os.mkdir(outputDir)
    print("Path {} created.".format(outputDir))

# xTalkFileName       = parse_aliasName+'xTalkStudiesValidation.root'
# histConfigDictName  = parse_aliasName+"histConfigDict.json"

############################
##   LOAD DUMPED DATA     ##
############################

sTree = ROOT.TChain("dumpedData",'')
# logging.info("input files: \n")
for file in inputFileName:
    fileNameInputLog = file+" added to the TChain..."
    print(fileNameInputLog)
    # logging.info(fileNameInputLog)    
    sTree.Add(file+"/dumpedData")

if parse_events == -1:
    nEvts = sTree.GetEntries()
else:
    nEvts = parse_events

    
sTree.GetEntry(0) #get first entry to load constants

##################################################################################################################
####### Configuration ######
############################
# jetROI  = -1

if parse_splitEvents == -1:
    # eventsPerFile       = 'all'
    eventsPerFile       = [nEvts]
    outputFileNames     = parse_aliasName+('{}.npz'.format(0))
    print(outputFileNames)
    
elif parse_splitEvents > 0:
    eventsPerFile       = parse_splitEvents*[int(nEvts/parse_splitEvents)]
    eventsPerFile[0]    = eventsPerFile[0] + (nEvts % parse_splitEvents)

    outputFileNames     = len(eventsPerFile)*['']
    for k, ev in enumerate(eventsPerFile):
        outputFileNames[k] = parse_aliasName+('{}'.format(k)).zfill(int(len(str(len(eventsPerFile))))) + '.npz' # fill with leading zeros
    print(outputFileNames)      

    print("Events per output file: ", eventsPerFile)
else:
    sys.exit("Invalid value to splitEvents parameter. Stopping execution.")

try:
    dataBadCh   = getattr(sTree,'cluster_channel_bad')
    # jetROI  = np.float32(getattr(sTree, 'jet_roi_r'))
except:
    print("Data does not have bad_channel info. dataHasBadCh=False")
    dataHasBadCh  = False # if the data dumped has the flag 'noBadCells=True', this flag has to be False. Otherwise, its True.
    pass
evLim               = nEvts

#### Main Switches ####
isMC                = False
    

# ****** DICTIONARIES ******
dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
maxOfLayers         = len(dictCaloLayer['Layer']) 

# Number of regions in each sampling layer (eta x phi region)
regionsPerLayer = []
for reg in dictCaloLayer['granularityEtaLayer']:
    regionsPerLayer.append(len(reg))

evN         = 0
fileNumber  = 0
start = time()

datasetDict = {"dataset": []}

fileCounter = 0
evtCounter  = 0
for evN in range(0,evLim):

    if evN >= evLim:
        break
    sTree.GetEntry(evN)
    if (evN % 1000) == 0:
        evtLogInfo = "Event %d / %d"%(evN, evLim)
        timePerEvStart = time()

        print(evtLogInfo)
        # logging.info(evtLogInfo)
    
    ei_runNumber    = int(getattr(sTree, "RunNumber"))
    ei_eventNumber  = int(getattr(sTree, "EventNumber"))
    ei_bcid         = int(getattr(sTree, "BCID"))
    # ei_lumibNumber  = int(getattr(sTree, "Lumiblock"))
    ei_avgMu        = getattr(sTree, "avg_mu_inTimePU")
    ei_avgMuOOT     = getattr(sTree, "avg_mu_OOTimePU")
    
    ###### DEBUG #######
    # python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/EventReader/share/createXTalkDataset.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/test_minbias_hwid.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/offline-ringer-dev/crosstalk/run/dataMinBias_debug' --fileAlias='dataMinBiasHwid_debug' --events=-1 --splitEvents=-1 > debug.log 2>&1
    # if ei_eventNumber != 380449669:
    #     continue    
    #***************************

    # print(ei_eventNumber, type(ei_eventNumber),int(ei_eventNumber), np.int64(ei_eventNumber))

    electronPt                  = getattr(sTree,'el_Pt')
    electronIndex               = getattr(sTree,'el_index')
    electronIndex_clusterLvl    = getattr(sTree,'c_electronIndex_clusterLvl')
    electronEta                 = getattr(sTree,'el_Eta')
    electronPhi                 = getattr(sTree,'el_Phi')
    electronEoverP              = getattr(sTree,'el_eoverp')
    electronSSf1                = getattr(sTree,'el_f1')
    electronSSf3                = getattr(sTree,'el_f3')
    electronSSeratio            = getattr(sTree,'el_eratio')
    electronSSweta1             = getattr(sTree,'el_weta1')
    electronSSweta2             = getattr(sTree,'el_weta2')
    electronSSfracs1            = getattr(sTree,'el_fracs1')
    electronSSwtots1            = getattr(sTree,'el_wtots1')
    electronSSe277              = getattr(sTree,'el_e277')
    electronSSreta              = getattr(sTree,'el_reta')
    electronSSrphi              = getattr(sTree,'el_rphi')
    electronSSdeltae            = getattr(sTree,'el_deltae')
    electronSSrhad              = getattr(sTree,'el_rhad')
    electronSSrhad1             = getattr(sTree,'el_rhad1')

    ### TopoCluster or SuperCluster
    clusterIndex                 = getattr(sTree,'cluster_index')
    clusterPt                    = getattr(sTree,'cluster_pt')
    clusterEta                   = getattr(sTree,'cluster_eta')
    clusterPhi                   = getattr(sTree,'cluster_phi')
    # clusterEtaCalc               = getattr(sTree,'cluster_eta_calc')
    # clusterPhiCalc               = getattr(sTree,'cluster_phi_calc')
    # Cluster Cells 
    clusterCellIndex             = getattr(sTree,'cluster_cell_index') 
    clusterIndexCellLvl          = getattr(sTree,'cluster_index_cellLvl')
    clusterCellCaloGain          = getattr(sTree,'cluster_cell_caloGain')
    clusterCellLayer             = getattr(sTree,'cluster_cell_layer')
    clusterCellRegion            = getattr(sTree,'cluster_cell_region')
    clusterCellEta               = getattr(sTree,'cluster_cell_eta')
    clusterCellPhi               = getattr(sTree,'cluster_cell_phi') 
    clusterCellDEta              = getattr(sTree,'cluster_cell_deta')
    clusterCellDPhi              = getattr(sTree,'cluster_cell_dphi')
    clusterCellsDistDEta         = getattr(sTree,'cluster_cellsDist_deta')
    clusterCellsDistDPhi         = getattr(sTree,'cluster_cellsDist_dphi')
    # Cluster Channel 
    clusterIndexChLvl            = getattr(sTree,'cluster_index_chLvl')
    clusterChannelIndex          = getattr(sTree,'cluster_channel_index')
    clusterChannelHash           = getattr(sTree,'cluster_channel_hash')
    clusterChannelId             = getattr(sTree,'cluster_channel_id')
    clusterChannelDigits         = getattr(sTree,'cluster_channel_digits')
    clusterChannelEnergy         = getattr(sTree,'cluster_channel_energy')
    clusterChannelTime           = getattr(sTree,'cluster_channel_time')
    if dataHasBadCh: clusterChannelBad            = getattr(sTree,'cluster_channel_bad')
    clusterChannelChInfo         = getattr(sTree,'cluster_channel_chInfo')
    clusterChannelEffSigma       = getattr(sTree,'cluster_channel_effSigma')
    clusterChannelNoise          = getattr(sTree,'cluster_channel_noise')
    clusterChannelDSPThr         = getattr(sTree,'cluster_channel_DSPThreshold')
    clusterChannelOFCTimeOffset  = getattr(sTree,'cluster_channel_OFCTimeOffset')
    clusterChannelADC2MeV0       = getattr(sTree,'cluster_channel_ADC2MeV0')
    clusterChannelADC2MeV1       = getattr(sTree,'cluster_channel_ADC2MeV1')
    clusterChannelOFCa           = getattr(sTree,'cluster_channel_OFCa')
    clusterChannelOFCb           = getattr(sTree,'cluster_channel_OFCb')
    clusterChannelShape          = getattr(sTree,'cluster_channel_shape')
    clusterChannelShapeDer       = getattr(sTree,'cluster_channel_shapeDer')
    # Cluster raw channel 
    clusterIndexRawChLvl         = getattr(sTree,'cluster_index_rawChLvl')
    clusterRawChannelIndex       = getattr(sTree,'cluster_rawChannel_index')
    clusterRawChannelAmplitude   = getattr(sTree,'cluster_rawChannel_amplitude')
    clusterRawChannelTime        = getattr(sTree,'cluster_rawChannel_time')
    clusterRawChannelPedProv     = getattr(sTree,'cluster_rawChannel_pedProv')
    clusterRawChannelQual        = getattr(sTree,'cluster_rawChannel_qual')
    clusterRawChannelChInfo      = getattr(sTree,'cluster_rawChannel_chInfo')
    clusterRawChannelId          = getattr(sTree,'cluster_rawChannel_id')
    clusterRawChannelDSPThr      = getattr(sTree,'cluster_rawChannel_DSPThreshold')

    ### Fixed Window Clusters (7x11 EMB2)
    cluster711Index                 = getattr(sTree,'cluster711_index')
    cluster711Pt                    = getattr(sTree,'cluster711_pt')
    cluster711Eta                   = getattr(sTree,'cluster711_eta')
    cluster711Phi                   = getattr(sTree,'cluster711_phi')
    # Cluster Cells 
    cluster711CellIndex             = getattr(sTree,'cluster711_cell_index') 
    cluster711IndexCellLvl          = getattr(sTree,'cluster711_index_cellLvl')
    cluster711CellCaloGain          = getattr(sTree,'cluster711_cell_caloGain')
    cluster711CellLayer             = getattr(sTree,'cluster711_cell_layer')
    cluster711CellRegion            = getattr(sTree,'cluster711_cell_region')
    cluster711CellEta               = getattr(sTree,'cluster711_cell_eta')
    cluster711CellPhi               = getattr(sTree,'cluster711_cell_phi') 
    cluster711CellDEta              = getattr(sTree,'cluster711_cell_deta')
    cluster711CellDPhi              = getattr(sTree,'cluster711_cell_dphi')
    cluster711CellsDistDEta         = getattr(sTree,'cluster711_cellsDist_deta')
    cluster711CellsDistDPhi         = getattr(sTree,'cluster711_cellsDist_dphi')
    # Cluster Channel 
    cluster711IndexChLvl            = getattr(sTree,'cluster711_index_chLvl')
    cluster711ChannelIndex          = getattr(sTree,'cluster711_channel_index')
    cluster711ChannelHash           = getattr(sTree,'cluster711_channel_hash')
    cluster711ChannelId             = getattr(sTree,'cluster711_channel_id')
    cluster711ChannelDigits         = getattr(sTree,'cluster711_channel_digits')
    cluster711ChannelEnergy         = getattr(sTree,'cluster711_channel_energy')
    cluster711ChannelTime           = getattr(sTree,'cluster711_channel_time')
    if dataHasBadCh: cluster711ChannelBad            = getattr(sTree,'cluster711_channel_bad')
    cluster711ChannelChInfo         = getattr(sTree,'cluster711_channel_chInfo')
    cluster711ChannelEffSigma       = getattr(sTree,'cluster711_channel_effSigma')
    cluster711ChannelNoise          = getattr(sTree,'cluster711_channel_noise')
    cluster711ChannelDSPThr         = getattr(sTree,'cluster711_channel_DSPThreshold')
    cluster711ChannelOFCTimeOffset  = getattr(sTree,'cluster711_channel_OFCTimeOffset')
    cluster711ChannelADC2MeV0       = getattr(sTree,'cluster711_channel_ADC2MeV0')
    cluster711ChannelADC2MeV1       = getattr(sTree,'cluster711_channel_ADC2MeV1')
    cluster711ChannelOFCa           = getattr(sTree,'cluster711_channel_OFCa')
    cluster711ChannelOFCb           = getattr(sTree,'cluster711_channel_OFCb')
    cluster711ChannelShape          = getattr(sTree,'cluster711_channel_shape')
    cluster711ChannelShapeDer       = getattr(sTree,'cluster711_channel_shapeDer')

    # Cluster raw channel 
    cluster711IndexRawChLvl         = getattr(sTree,'cluster711_index_rawChLvl')
    cluster711RawChannelIndex       = getattr(sTree,'cluster711_rawChannel_index')
    cluster711RawChannelAmplitude   = getattr(sTree,'cluster711_rawChannel_amplitude')
    cluster711RawChannelTime        = getattr(sTree,'cluster711_rawChannel_time')
    cluster711RawChannelPedProv     = getattr(sTree,'cluster711_rawChannel_pedProv')
    cluster711RawChannelQual        = getattr(sTree,'cluster711_rawChannel_qual')
    cluster711RawChannelChInfo      = getattr(sTree,'cluster711_rawChannel_chInfo')
    cluster711RawChannelId          = getattr(sTree,'cluster711_rawChannel_id')
    cluster711RawChannelDSPThr      = getattr(sTree,'cluster711_rawChannel_DSPThreshold')

    # if isMC:
    #     mcTruthEnergy  = getattr(sTree,'mc_energy')
    #     mcTruthPt      = getattr(sTree,'mc_pt')
    #     mcTruthM       = getattr(sTree,'mc_m')
    #     mcTruthEta     = getattr(sTree,'mc_eta')
    #     mcTruthPhi     = getattr(sTree,'mc_phi')
    #     mcTruthPdgId   = getattr(sTree,'mc_pdgId')

    # photonEnergy  = getattr(sTree,'ph_energy')
    # photonPt      = getattr(sTree,'ph_pt')
    # photonEta     = getattr(sTree,'ph_eta')
    # photonPhi     = getattr(sTree,'ph_phi')
    # photonM       = getattr(sTree,'ph_m')

    #**********************************************
    # Format into python list/np array
    #**********************************************
    electronPtArray                 = stdVecToArray(electronPt)
    electronIndexArray              = stdVecToArray(electronIndex)
    electronIndex_clusterLvlArray   = stdVecToArray(electronIndex_clusterLvl)
    electronEtaArray                = stdVecToArray(electronEta)
    electronPhiArray                = stdVecToArray(electronPhi)
    electronEoverPArray             = stdVecToArray(electronEoverP)
    electronSSf1Array               = stdVecToArray(electronSSf1)
    electronSSf3Array               = stdVecToArray(electronSSf3)
    electronSSeratioArray           = stdVecToArray(electronSSeratio)
    electronSSweta1Array            = stdVecToArray(electronSSweta1)
    electronSSweta2Array            = stdVecToArray(electronSSweta2)
    electronSSfracs1Array           = stdVecToArray(electronSSfracs1)
    electronSSwtots1Array           = stdVecToArray(electronSSwtots1)
    electronSSe277Array             = stdVecToArray(electronSSe277)
    electronSSretaArray             = stdVecToArray(electronSSreta)
    electronSSrphiArray             = stdVecToArray(electronSSrphi)
    electronSSdeltaeArray           = stdVecToArray(electronSSdeltae)
    electronSSrhadArray             = stdVecToArray(electronSSrhad)
    electronSSrhad1Array            = stdVecToArray(electronSSrhad1)

    clusterIndexArray               = stdVecToArray(clusterIndex)
    clusterPtArray                  = stdVecToArray(clusterPt)
    clusterEtaArray                 = stdVecToArray(clusterEta)
    clusterPhiArray                 = stdVecToArray(clusterPhi)

    clusterCellIndexArray           = stdVecToArray(clusterCellIndex)
    clusterIndexCellLvlArray        = stdVecToArray(clusterIndexCellLvl)
    clusterCellCaloGainArray        = stdVecToArray(clusterCellCaloGain)
    clusterCellLayerArray           = stdVecToArray(clusterCellLayer)
    clusterCellRegionArray          = stdVecToArray(clusterCellRegion)
    clusterCellEtaArray             = stdVecToArray(clusterCellEta)
    clusterCellPhiArray             = stdVecToArray(clusterCellPhi)
    clusterCellDEtaArray            = stdVecToArray(clusterCellDEta)
    clusterCellDPhiArray            = stdVecToArray(clusterCellDPhi)
    clusterCellsDistDEtaArray       = stdVecToArray(clusterCellsDistDEta)
    clusterCellsDistDPhiArray       = stdVecToArray(clusterCellsDistDPhi)
    
    clusterIndexChLvlArray          = stdVecToArray(clusterIndexChLvl)
    clusterChannelIndexArray        = stdVecToArray(clusterChannelIndex)
    clusterChannelHashArray         = stdVecToArray(clusterChannelHash)
    clusterChannelIdArray           = stdVecToArray(clusterChannelId)
    clusterChannelDigitsArray       = stdVecOfStdVecToArrayOfList(clusterChannelDigits, convertToInt=True)
    clusterChannelEnergyArray       = stdVecToArray(clusterChannelEnergy)
    clusterChannelTimeArray         = stdVecToArray(clusterChannelTime)
    clusterChannelChInfoArray       = stdVecOfStdVecToArrayOfList(clusterChannelChInfo)
    if dataHasBadCh: clusterChannelBadArray      = stdVecToArray(clusterChannelBad)
    clusterChannelEffSigmaArray         = stdVecToArray(clusterChannelEffSigma)
    clusterChannelNoiseArray            = stdVecToArray(clusterChannelNoise)
    clusterChannelDSPThrArray           = stdVecToArray(clusterChannelDSPThr)
    clusterChannelOFCTimeOffsetArray    = stdVecToArray(clusterChannelOFCTimeOffset)
    clusterChannelADC2MeV0Array         = stdVecToArray(clusterChannelADC2MeV0)
    clusterChannelADC2MeV1Array         = stdVecToArray(clusterChannelADC2MeV1)
    clusterChannelOFCaArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCa)
    clusterChannelOFCbArray             = stdVecOfStdVecToArrayOfList(clusterChannelOFCb)
    clusterChannelShapeArray            = stdVecOfStdVecToArrayOfList(clusterChannelShape)
    clusterChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(clusterChannelShapeDer)

    clusterIndexRawChLvlArray       = stdVecToArray(clusterIndexRawChLvl)
    clusterRawChannelIndexArray     = stdVecToArray(clusterRawChannelIndex)
    clusterRawChannelAmplitudeArray = stdVecToArray(clusterRawChannelAmplitude)
    clusterRawChannelTimeArray      = stdVecToArray(clusterRawChannelTime)
    clusterRawChannelPedProvArray   = stdVecToArray(clusterRawChannelPedProv)
    clusterRawChannelQualArray      = stdVecToArray(clusterRawChannelQual)
    clusterRawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(clusterRawChannelChInfo)
    clusterRawChannelIdArray        = stdVecToArray(clusterRawChannelId)
    clusterRawChannelDSPThrArray    = stdVecToArray(clusterRawChannelDSPThr)

    cluster711IndexArray               = stdVecToArray(cluster711Index)
    cluster711PtArray                  = stdVecToArray(cluster711Pt)
    cluster711EtaArray                 = stdVecToArray(cluster711Eta)
    cluster711PhiArray                 = stdVecToArray(cluster711Phi)

    cluster711CellIndexArray           = stdVecToArray(cluster711CellIndex)
    cluster711IndexCellLvlArray        = stdVecToArray(cluster711IndexCellLvl)
    cluster711CellCaloGainArray        = stdVecToArray(cluster711CellCaloGain)
    cluster711CellLayerArray           = stdVecToArray(cluster711CellLayer)
    cluster711CellRegionArray          = stdVecToArray(cluster711CellRegion)
    cluster711CellEtaArray             = stdVecToArray(cluster711CellEta)
    cluster711CellPhiArray             = stdVecToArray(cluster711CellPhi)
    cluster711CellDEtaArray            = stdVecToArray(cluster711CellDEta)
    cluster711CellDPhiArray            = stdVecToArray(cluster711CellDPhi)
    cluster711CellsDistDEtaArray       = stdVecToArray(cluster711CellsDistDEta)
    cluster711CellsDistDPhiArray       = stdVecToArray(cluster711CellsDistDPhi)
    
    cluster711IndexChLvlArray          = stdVecToArray(cluster711IndexChLvl)
    cluster711ChannelIndexArray        = stdVecToArray(cluster711ChannelIndex)
    cluster711ChannelHashArray         = stdVecToArray(cluster711ChannelHash)
    cluster711ChannelIdArray           = stdVecToArray(cluster711ChannelId)
    cluster711ChannelDigitsArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelDigits, convertToInt=True)
    cluster711ChannelEnergyArray       = stdVecToArray(cluster711ChannelEnergy)
    cluster711ChannelTimeArray         = stdVecToArray(cluster711ChannelTime)
    cluster711ChannelChInfoArray       = stdVecOfStdVecToArrayOfList(cluster711ChannelChInfo)
    if dataHasBadCh: cluster711ChannelBadArray      = stdVecToArray(cluster711ChannelBad)
    cluster711ChannelEffSigmaArray     = stdVecToArray(cluster711ChannelEffSigma)
    cluster711ChannelNoiseArray        = stdVecToArray(cluster711ChannelNoise)
    cluster711ChannelDSPThrArray       = stdVecToArray(cluster711ChannelDSPThr)
    cluster711ChannelOFCTimeOffsetArray    = stdVecToArray(cluster711ChannelOFCTimeOffset)
    cluster711ChannelADC2MeV0Array         = stdVecToArray(cluster711ChannelADC2MeV0)
    cluster711ChannelADC2MeV1Array         = stdVecToArray(cluster711ChannelADC2MeV1)
    cluster711ChannelOFCaArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCa)
    cluster711ChannelOFCbArray             = stdVecOfStdVecToArrayOfList(cluster711ChannelOFCb)
    cluster711ChannelShapeArray            = stdVecOfStdVecToArrayOfList(cluster711ChannelShape)
    cluster711ChannelShapeDerArray         = stdVecOfStdVecToArrayOfList(cluster711ChannelShapeDer)

    cluster711IndexRawChLvlArray       = stdVecToArray(cluster711IndexRawChLvl)
    cluster711RawChannelIndexArray     = stdVecToArray(cluster711RawChannelIndex)
    cluster711RawChannelAmplitudeArray = stdVecToArray(cluster711RawChannelAmplitude)
    cluster711RawChannelTimeArray      = stdVecToArray(cluster711RawChannelTime)
    cluster711RawChannelPedProvArray   = stdVecToArray(cluster711RawChannelPedProv)
    cluster711RawChannelQualArray      = stdVecToArray(cluster711RawChannelQual)
    cluster711RawChannelChInfoArray    = stdVecOfStdVecToArrayOfList(cluster711RawChannelChInfo)
    cluster711RawChannelIdArray        = stdVecToArray(cluster711RawChannelId)
    cluster711RawChannelDSPThrArray    = stdVecToArray(cluster711RawChannelDSPThr)

    timePerEvRead = time()

#**********************************************
#  XTALKS STUDIES: DATASET DICT
#**********************************************

# ----------------------- 0 EventInfo -------------------------------------
    eventInfoDict = {
        "runNumber":        [],
        "eventNumber":      [],
        "BCID":             [],
        "Lumiblock":        [],
        "avgMu":            [],
        "avgMuOOT":         [],
        "electrons":        {}
    }
    eventInfoDict["runNumber"].append(ei_runNumber)
    eventInfoDict["eventNumber"].append(ei_eventNumber)
    eventInfoDict["BCID"].append(ei_bcid)
    # eventInfoDict["Lumiblock"].append(ei_lumibNumber)
    eventInfoDict["avgMu"].append(ei_avgMu)
    eventInfoDict["avgMuOOT"].append(ei_avgMuOOT)
    
    # ----------------------- I Electron -------------------------------------
    if len(electronIndexArray) == 0: # skip events without electrons
        evtCounter+=1
        continue
    for elec in range(0, len(electronIndexArray)):
        electronDict    = {
        "el_{}".format(elec):{
            "index":      [],
            "pt":         [],
            "eta":        [],
            "phi":        [],
            "eoverp":     [],
            #shower shapes
            "f1":         [],
            "f3":         [],
            "eratio":     [],
            "weta1" :     [],
            "weta2" :     [],
            "fracs1":     [],
            "wtots1":     [],
            "e277"  :     [],
            "reta"  :     [],
            "rphi"  :     [],
            "deltae":     [],
            "rhad"  :     [],
            "rhad1" :     [],

            "clusters":{},
            "711_roi": {} 
            }
        } # electron-end

        electronDict["el_{}".format(elec)]["index"].append(electronIndexArray[elec])
        electronDict["el_{}".format(elec)]["pt"].append(electronPtArray[elec])
        electronDict["el_{}".format(elec)]["eta"].append(electronEtaArray[elec])
        electronDict["el_{}".format(elec)]["phi"].append(electronPhiArray[elec])
        electronDict["el_{}".format(elec)]["eoverp"].append(electronEoverPArray[elec])
        electronDict["el_{}".format(elec)]["f1"].append(electronSSf1Array[elec])
        electronDict["el_{}".format(elec)]["f3"].append(electronSSf3Array[elec])
        electronDict["el_{}".format(elec)]["eratio"].append(electronSSeratioArray[elec])
        electronDict["el_{}".format(elec)]["weta1"].append(electronSSweta1Array[elec])
        electronDict["el_{}".format(elec)]["weta2"].append(electronSSweta2Array[elec])
        electronDict["el_{}".format(elec)]["fracs1"].append(electronSSfracs1Array[elec])
        electronDict["el_{}".format(elec)]["wtots1"].append(electronSSwtots1Array[elec])
        electronDict["el_{}".format(elec)]["e277"].append(electronSSe277Array[elec])
        electronDict["el_{}".format(elec)]["reta"].append(electronSSretaArray[elec])
        electronDict["el_{}".format(elec)]["rphi"].append(electronSSrphiArray[elec])
        electronDict["el_{}".format(elec)]["deltae"].append(electronSSdeltaeArray[elec])
        electronDict["el_{}".format(elec)]["rhad"].append(electronSSrhadArray[elec])
        electronDict["el_{}".format(elec)]["rhad1"].append(electronSSrhad1Array[elec])

    # --------------------------------------------------------------------------
    #                       TopoCluster or SuperCluster
    # --------------------------------------------------------------------------

    # ----------------------- II Calo Objects (TopoCluster or SuperCluster) ----------------------
        # find array indexes for the cluster collection, from each electron.
        eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(clusterIndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            # fill clusters
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["index"].append(clusterIndexArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["pt"].append(clusterPtArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["eta"].append(clusterEtaArray[cl])
            clusterDict["cl_{}".format(clusterIndexArray[cl])]["phi"].append(clusterPhiArray[cl])

    # ----------------------- III CaloCell and Channels ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( clusterIndexCellLvlArray    == clusterIndexArray[cl] )[0]
            clusIndexCh     = np.where( clusterIndexChLvlArray      == clusterIndexArray[cl] )[0]
            clusIndexRawCh  = np.where( clusterIndexRawChLvlArray   == clusterIndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString( clusterCellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString( clusterCellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString( clusterCellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"].append( clusterCellIndexArray[clusIndexCell].tolist() )
            cellDict["sampling"].append( cellSamplingString )
            cellDict["caloRegion"].append( cellRegionString )
            cellDict["eta"].append( clusterCellEtaArray[clusIndexCell].tolist() )
            cellDict["phi"].append( clusterCellPhiArray[clusIndexCell].tolist() )
            cellDict["deta"].append( clusterCellDEtaArray[clusIndexCell].tolist() )
            cellDict["dphi"].append( clusterCellDPhiArray[clusIndexCell].tolist() )
            # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            cellDict["gain"].append( cellCaloGainString )

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )
            # print("debug el_{}, cl_{} clusterCellIndexArray[clusIndexCell]: ".format(elec,cl),clusterCellIndexArray[clusIndexCell])
            # print("debug el_{}, cl_{} clusterCellCaloGainArray[clusIndexCell]: ".format(elec,cl), clusterCellCaloGainArray[clusIndexCell] )
            # print("debug el_{} cl_{} clusterCellDEtaArray[clusIndexCell]: ".format(elec,cl),clusterCellDEtaArray[clusIndexCell])

            # -- Channels --
            channelDict     = {
                "index":        [],
                "caloRegion":   [], # from cell level
                "sampling":     [], # from cell level
                "energy":       [],
                "time":         [],
                "digits":       [],
                "eta":          [], # from cell level
                "phi":          [], # from cell level
                "gain":         [], # from cell level
                "deta":         [], # from cell level
                "dphi":         [], # from cell level
                "chInfo":       [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":       [],
                "channelId":    [],
                "effSigma":     [],
                "noise":        [],
                "DSPThreshold": [],
                "OFCTimeOffset":[],
                "ADC2MeV0":     [],
                "ADC2MeV1":     [],
                "OFCa":         [],
                "OFCb":         [],
                "Shape":        [],
                "ShapeDer":     []
            }
            roundedChIndex  = [round(x,1) for x in clusterChannelIndexArray[clusIndexCh]]
        
            channelDict["index"].append( roundedChIndex )            
            channelDict["energy"].append(clusterChannelEnergyArray[clusIndexCh].tolist())
            channelDict["time"].append(clusterChannelTimeArray[clusIndexCh].tolist())
            channelDict["digits"].append(clusterChannelDigitsArray[clusIndexCh].tolist())
            channelDict["chInfo"].append(clusterChannelChInfoArray[clusIndexCh].tolist())
            channelDict["hashId"].append(clusterChannelHashArray[clusIndexCh].tolist())
            channelDict["channelId"].append(clusterChannelIdArray[clusIndexCh].tolist())
            channelDict["effSigma"].append(clusterChannelEffSigmaArray[clusIndexCh].tolist())
            channelDict["noise"].append(clusterChannelNoiseArray[clusIndexCh].tolist())
            channelDict["DSPThreshold"].append(clusterChannelDSPThrArray[clusIndexCh].tolist())
            channelDict["OFCTimeOffset"].append(clusterChannelOFCTimeOffsetArray[clusIndexCh].tolist())
            channelDict["ADC2MeV0"].append(clusterChannelADC2MeV0Array[clusIndexCh].tolist())
            channelDict["ADC2MeV1"].append(clusterChannelADC2MeV1Array[clusIndexCh].tolist())
            channelDict["OFCa"].append(clusterChannelOFCaArray[clusIndexCh].tolist())
            channelDict["OFCb"].append(clusterChannelOFCbArray[clusIndexCh].tolist())
            channelDict["Shape"].append(clusterChannelShapeArray[clusIndexCh].tolist())
            channelDict["ShapeDer"].append(clusterChannelShapeDerArray[clusIndexCh].tolist())

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"].append( channelRegionString )
            channelDict["sampling"].append( channelSamplingString )
            channelDict["gain"].append( channelCaloGainString )
            channelDict["eta"].append (channelEtaLink.tolist() )
            channelDict["phi"].append (channelPhiLink.tolist() )
            channelDict["deta"].append(channelDEtaLink.tolist() )
            channelDict["dphi"].append(channelDPhiLink.tolist() )
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":        [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "caloRegion":   [],
                "sampling":     [],
                "amplitude":    [],
                "time":         [],
                "pedProv":      [],
                "quality":      [],
                "chInfo":       [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":    [],
                "DSPThreshold": []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in clusterRawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"].append( roundedRawChIndex )
            rawChannelDict["amplitude"].append(clusterRawChannelAmplitudeArray[clusIndexRawCh].tolist())
            rawChannelDict["time"].append(clusterRawChannelTimeArray[clusIndexRawCh].tolist())
            rawChannelDict["pedProv"].append(clusterRawChannelPedProvArray[clusIndexRawCh].tolist())
            rawChannelDict["quality"].append(clusterRawChannelQualArray[clusIndexRawCh].tolist())
            rawChannelDict["chInfo"].append(clusterRawChannelChInfoArray[clusIndexRawCh].tolist())
            rawChannelDict["channelId"].append(clusterRawChannelIdArray[clusIndexRawCh].tolist())
            rawChannelDict["DSPThreshold"].append(clusterRawChannelDSPThrArray[clusIndexRawCh].tolist())
            
            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellRegionArray[clusIndexCell])
            rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellLayerArray[clusIndexCell] )
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"], clusterCellDPhiArray[clusIndexCell])
            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"].append( rawChannelRegionString )
            rawChannelDict["sampling"].append( rawChannelSamplingString )
            rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
            rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
            rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
            rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

    # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(clusterIndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["clusters"].update( clusterDict ) 

    # --------------------------------------------------------------------------
    #                       ROI 7_11 EMB2 Reference
    # --------------------------------------------------------------------------

    # ----------------------- II-1 Calo Objects (711 ROI) ----------------------
        # find array indexes for the cluster collection, from each electron.
        # eIndexClus = np.where( electronIndex_clusterLvlArray == electronIndexArray[elec] )[0]

        # with the array indexes, find the the cluster indexes, linked to the selected electron
        # ( the cluster index is used to reach the cell level indexes. )
        # for cIndex in clusterIndexArray[eIndexClus]: # for each cluster
        for cl in eIndexClus:
    
            # layerIndex = np.where(clusterCellLayerArray[cIndexCell] == 2)[0] # get EMB2 cells
            clusterDict = {
                "cl_{}".format(cluster711IndexArray[cl]):{
                    "index":        [],
                    "pt":           [],
                    "eta":          [],
                    "phi":          [],
                    # cells
                    "cells":         {}, # structure
                    "channels":      {}, # structure
                    "rawChannels":   {} # structure
                }
            }
            # fill clusters
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["index"].append(cluster711IndexArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["pt"].append (cluster711PtArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["eta"].append(cluster711EtaArray[cl])
            clusterDict["cl_{}".format(cluster711IndexArray[cl])]["phi"].append(cluster711PhiArray[cl])

        # ----------------------- III-1 CaloCell and Channels (711) ------------------
            # get cells, channels and rawChannels linked to each cluster
            clusIndexCell   = np.where( cluster711IndexCellLvlArray    == cluster711IndexArray[cl] )[0]
            clusIndexCh     = np.where( cluster711IndexChLvlArray      == cluster711IndexArray[cl] )[0]
            clusIndexRawCh  = np.where( cluster711IndexRawChLvlArray   == cluster711IndexArray[cl] )[0]
            # print("clusterCellIndexArray:", clusterCellIndexArray)
            # print("clusterRawChannelIndexArray",clusterRawChannelIndexArray)

            # -- Cells --
            # OBS.: it was chosen to remove most of the information in this part of dict (cell level), because
            # it is more convenient to work with the xtalk and cell data in the same level, in this case, in channel.
            # 28 Sept, it was added again to perform some tests.
            cellDict    = {
                "index":        [],
                "caloRegion":   [],
                "sampling":     [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "gain":         []
            }
            cellRegionString    = getRegionString   ( cluster711CellRegionArray[clusIndexCell] , dictCaloLayer["Region"])
            cellSamplingString  = getSamplingString ( cluster711CellLayerArray[clusIndexCell] , dictCaloLayer["Layer"] )
            cellCaloGainString  = getCaloGainString ( cluster711CellCaloGainArray[clusIndexCell] , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            cellDict["index"].append        ( cluster711CellIndexArray[clusIndexCell].tolist() )
            cellDict["sampling"].append     ( cellSamplingString )
            cellDict["caloRegion"].append   ( cellRegionString )
            cellDict["eta"].append          ( cluster711CellEtaArray[clusIndexCell].tolist() )
            cellDict["phi"].append          ( cluster711CellPhiArray[clusIndexCell].tolist() )
            cellDict["deta"].append         ( cluster711CellDEtaArray[clusIndexCell].tolist() )
            cellDict["dphi"].append         ( cluster711CellDPhiArray[clusIndexCell].tolist() )
            # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
            cellDict["gain"].append         ( cellCaloGainString )

            # print(clusterCellCaloGainArray[clusIndexCell], cellDict["gain"] )

            # -- Channels --
            channelDict     = {
                "index":        [],
                "caloRegion":   [], # from cell level
                "sampling":     [], # from cell level
                "energy":       [],
                "time":         [],
                "digits":       [],
                "eta":          [], # from cell level
                "phi":          [], # from cell level
                "gain":         [], # from cell level
                "deta":         [], # from cell level
                "dphi":         [], # from cell level
                "chInfo":       [],  # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "hashId":       [],
                "channelId":    [],
                "effSigma":     [],
                "noise":        [],
                "DSPThreshold": [],
                "OFCTimeOffset":[],
                "ADC2MeV0":     [],
                "ADC2MeV1":     [],
                "OFCa":         [],
                "OFCb":         [],
                "Shape":        [],
                "ShapeDer":     []
            }
            roundedChIndex  = [round(x,1) for x in cluster711ChannelIndexArray[clusIndexCh]]

            # print(cluster711ChannelEnergyArray,cluster711ChannelChInfoArray)
        
            channelDict["index"].append ( roundedChIndex )            
            channelDict["energy"].append(cluster711ChannelEnergyArray[clusIndexCh].tolist())
            channelDict["time"].append  (cluster711ChannelTimeArray[clusIndexCh].tolist())
            channelDict["digits"].append(cluster711ChannelDigitsArray[clusIndexCh].tolist())
            channelDict["chInfo"].append(cluster711ChannelChInfoArray[clusIndexCh].tolist())
            channelDict["hashId"].append(cluster711ChannelHashArray[clusIndexCh].tolist())
            channelDict["channelId"].append(cluster711ChannelIdArray[clusIndexCh].tolist())
            channelDict["effSigma"].append(cluster711ChannelEffSigmaArray[clusIndexCh].tolist())
            channelDict["noise"].append(cluster711ChannelNoiseArray[clusIndexCh].tolist())
            channelDict["DSPThreshold"].append(cluster711ChannelDSPThrArray[clusIndexCh].tolist())
            channelDict["OFCTimeOffset"].append(cluster711ChannelOFCTimeOffsetArray[clusIndexCh].tolist())
            channelDict["ADC2MeV0"].append(cluster711ChannelADC2MeV0Array[clusIndexCh].tolist())
            channelDict["ADC2MeV1"].append(cluster711ChannelADC2MeV1Array[clusIndexCh].tolist())
            channelDict["OFCa"].append(cluster711ChannelOFCaArray[clusIndexCh].tolist())
            channelDict["OFCb"].append(cluster711ChannelOFCbArray[clusIndexCh].tolist())
            channelDict["Shape"].append(cluster711ChannelShapeArray[clusIndexCh].tolist())
            channelDict["ShapeDer"].append(cluster711ChannelShapeDerArray[clusIndexCh].tolist())

            # Data decompression
            channelRegionLink       = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellRegionArray[clusIndexCell])
            channelSamplingLink     = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellLayerArray[clusIndexCell] )
            channelGainLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellCaloGainArray[clusIndexCell] )
            channelEtaLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellEtaArray[clusIndexCell] )
            channelPhiLink          = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellPhiArray[clusIndexCell] )
            channelDEtaLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDEtaArray[clusIndexCell] )
            channelDPhiLink         = indexConversion( channelDict["index"] , cellDict["index"], cluster711CellDPhiArray[clusIndexCell] )

            channelRegionString     = getRegionString( channelRegionLink , dictCaloLayer["Region"] )
            channelSamplingString   = getSamplingString( channelSamplingLink , dictCaloLayer["Layer"])
            channelCaloGainString   = getCaloGainString( channelGainLink , dictCaloLayer["CaloGainNumber"], dictCaloLayer["CaloGainName"] )

            channelDict["caloRegion"].append(channelRegionString )
            channelDict["sampling"].append  (channelSamplingString )
            channelDict["gain"].append      (channelCaloGainString)  #( cellCaloGainString ) ## is that organized with indexes sequences for channel?
            channelDict["eta"].append       (channelEtaLink.tolist() )
            channelDict["phi"].append       (channelPhiLink.tolist() )
            channelDict["deta"].append      (channelDEtaLink.tolist() )
            channelDict["dphi"].append      (channelDPhiLink.tolist() )
            
            # # cellDict["gain"].append( clusterCellCaloGainArray[clusIndexCell] )
        
            # -- RawChannels --
            rawChannelDict  = {
                "index":        [],
                "eta":          [],
                "phi":          [],
                "deta":         [],
                "dphi":         [],
                "caloRegion":   [],
                "sampling":     [],
                "amplitude":    [],
                "time":         [],
                "pedProv":      [],
                "quality":      [],
                "chInfo":       [], # both LAr (barrel or EC, pos or neg side, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
                "channelId":    [],
                "DSPThreshold": []
            }
            # must be treated because these indexes has floating point to represent tile PMT1 (+0.1) and PMT2 (+0.2), but when loading them, it is not precise (small bug to be fixed)
            roundedRawChIndex = [round(x,1) for x in cluster711RawChannelIndexArray[clusIndexRawCh]]

            rawChannelDict["index"].append      ( roundedRawChIndex )
            rawChannelDict["amplitude"].append  (cluster711RawChannelAmplitudeArray[clusIndexRawCh].tolist())
            rawChannelDict["time"].append       (cluster711RawChannelTimeArray[clusIndexRawCh].tolist())
            rawChannelDict["pedProv"].append    (cluster711RawChannelPedProvArray[clusIndexRawCh].tolist())
            rawChannelDict["quality"].append    (cluster711RawChannelQualArray[clusIndexRawCh].tolist())
            rawChannelDict["chInfo"].append     (cluster711RawChannelChInfoArray[clusIndexRawCh].tolist())
            rawChannelDict["channelId"].append  (cluster711RawChannelIdArray[clusIndexRawCh].tolist())
            rawChannelDict["DSPThreshold"].append(cluster711RawChannelDSPThrArray[clusIndexRawCh].tolist())


            #  Index conversion: it works like data decompression from root files, that were indexed to use less
            # disk when dumped from ESD format. Now, information that were saved for only one data level (cell, channel, etc),
            # can be propagated to the others to built a dataset which is easier to handle, independently of each data level.
            # Here, the cluster711Cell variables in cell level, will be converted to their respective cells in the rawChannel level.
            rawChannelRegionLink        = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellRegionArray[clusIndexCell])
            rawChannelSamplingLink      = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellLayerArray[clusIndexCell] )
            rawChannelEtaLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellEtaArray[clusIndexCell])
            rawChannelDEtaLink          = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellDEtaArray[clusIndexCell])
            rawChannelPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"]  , cluster711CellPhiArray[clusIndexCell])
            rawChannelDPhiLink           = indexConversion( rawChannelDict["index"] , cellDict["index"] , cluster711CellDPhiArray[clusIndexCell])

            rawChannelRegionString      = getRegionString( rawChannelRegionLink , dictCaloLayer["Region"] )
            rawChannelSamplingString    = getSamplingString( rawChannelSamplingLink , dictCaloLayer["Layer"])            

            rawChannelDict["caloRegion"].append( rawChannelRegionString )
            rawChannelDict["sampling"].append( rawChannelSamplingString )
            rawChannelDict["eta"].append( rawChannelEtaLink.tolist() )
            rawChannelDict["phi"].append( rawChannelPhiLink.tolist() )
            rawChannelDict["deta"].append( rawChannelDEtaLink.tolist() )
            rawChannelDict["dphi"].append( rawChannelDPhiLink.tolist() )

            # print(np.shape(rawChannelDict["phi"]), rawChannelDict["phi"])
            # print(np.shape(cellDict["eta"]), cellDict["eta"] )

            # df = pd.concat({k: pd.DataFrame.from_dict(v, orient='index') for k, v in rawChannelDict.items()}, axis=1)
            # print(df)
            # print(pd.concat({k: pd.DataFrame.from_dict(v) for k, v in rawChannelDict.items()}))

            # ----------------------- III-end ------------------------------------
            # fill clusters with cells, channels and raw channels
            clusName    = str("cl_{}".format(cluster711IndexArray[cl]))
            clusterDict[clusName]["cells"].update( cellDict )
            clusterDict[clusName]["channels"].update( channelDict )
            clusterDict[clusName]["rawChannels"].update( rawChannelDict )

            # saveAsJsonFile(clusterDict, 'file001.json')
            # lDatasetDict = loadJsonFile('file001.json')
            # print(lDatasetDict)

            # fill electron with cluster(s)
            electronDict["el_{}".format(elec)]["711_roi"].update( clusterDict ) 

        # fill event with particles
        eventInfoDict["electrons"].update( electronDict )

    # fill dataset with events
    datasetDict["dataset"].append(eventInfoDict)

    ####### DEBUG ########
    # print(eventInfoDict)
    #*********************
    #---------------------

    # print(evtCounter,fileCounter ,eventsPerFile[fileCounter])  # debug reasons
    # for k, outFileName in enumerate(outputFileNames):
    if (evtCounter >= eventsPerFile[fileCounter]) and not(parse_splitEvents == -1):
        # ***************
        #   SAVING FILE
        # ***************
        # np.savez(outputDir+parse_aliasName+'dataMinBias003.npz',**datasetDict)
        np.savez(outputDir+outputFileNames[fileCounter],**datasetDict)
        print(outputFileNames[fileCounter]+" saved.")
        fileCounter+=1
        evtCounter = 0
        datasetDict = {"dataset": []}
        continue

    evtCounter+=1

# save the rest of events, in case of split
if datasetDict["dataset"]:
    if not(parse_splitEvents == -1):
        np.savez(outputDir+outputFileNames[fileCounter],**datasetDict)


timePerEvAnalysis = time()
# print(datasetDict)
# print(dictCaloLayer)

###################################################################        
# # ***************
# #   SAVING FILE (Full events)
# # ***************
# # np.savez(outputDir+parse_aliasName+'dataMinBias003.npz',**datasetDict)
if (parse_splitEvents == -1):
    np.savez(outputDir+outputFileNames,**datasetDict)

# To LOAD: (python3)
# import numpy as np
# a = np.load('file001.npz', allow_pickle=True)
# Example in acessing the lower elements:
#   a['dataset'][0]['electrons']['el_0']['clusters']['cl_0']['channels']['digits']

end = time()
timeLog = "Total time for %d events: %.3f minutes."%(evLim,(end-start)/60)
print(timeLog)

ROOT.gROOT.SetBatch(False)
