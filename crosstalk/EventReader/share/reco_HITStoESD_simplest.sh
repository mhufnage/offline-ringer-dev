#!/bin/sh
shopt -s expand_aliases
source $HOME/.bashrc
############## Batch Mode on LXPLUS ###############
setupATLAS;
asetup Athena,22.0.44;
# # This import is needed to get components (tools, algs)
# # See https://indico.cern.ch/event/871612/contributions/3677824/attachments/1963726/3264714/UseCompFactory.pdf
# from AthenaConfiguration.ComponentFactory import CompFactory

for i in {000002..000050}
    do
        # HIT TO RDO
        echo "HITStoRDO..."
        echo "Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_$i.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._$i.pool.root.1 >Reco_tf_HITS0.log 2>&1 "
        Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_$i.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._$i.pool.root.1 >Reco_tf_HITStoRDO.log 2>&1

        # RDO TO ESD
        echo "RDOtoESD..."
        echo "Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_$i.pool.root --outputESDFile=ESD_pi0_$i.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO0.log 2>&1"  
        Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_$i.pool.root --outputESDFile=ESD_pi0_$i.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO0.log 2>&1

        rm RDO_pi0_$i.pool.root
    done


############## < END > ###############
# HITS to RDO
# Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_00245x.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._00245*.pool.root.1 >Reco_tf_HITS0.log 2>&1 

# Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_002451.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._002451.pool.root.1 >Reco_tf_HITS1.log 2>&1 

# Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_002452.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._002452.pool.root.1 >Reco_tf_HITS2.log 2>&1 

# Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_002453.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._002453.pool.root.1 >Reco_tf_HITS3.log 2>&1 

# RDO to ESD
# Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_002450.pool.root --outputESDFile=ESD_pi0_002450.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO0.log 2>&1 

# Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_002451.pool.root --outputESDFile=ESD_pi0_002451.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO1.log 2>&1 

# Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_002452.pool.root --outputESDFile=ESD_pi0_002452.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO2.log 2>&1 

# Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_00245x.pool.root --outputESDFile=ESD_pi0_002453.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO3.log 2>&1 





# Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_00245x.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._00245*.pool.root.1 >Reco_tf_HITS0.log 2>&1 

# Reco_tf.py  --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --inputRDOFile=RDO_pi0_00245x.pool.root --outputESDFile=ESD_pi0_002453.pool.root --preExec=rec.UserAlgs="['jobRDOtoESDOptions.py']" --postInclude=saveESD.py >Reco_tf_RDO3.log 2>&1 
