# A rodar... 25/07/2022 (crash)
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0 --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];'"
# A testar local... 26/07/2022 (ok)
# setupATLAS
# asetup Athena,21.0.30
# Reco_tf.py  --inputBSFile=/eos/user/m/mhufnage/ALP_project/RAW/user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167/data17_13TeV.00329542.physics_MinBias.daq.RAW._lb0239._SFO-4._0001.data --outputESDFile=ESD_minBias_.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --AddCaloDigi=True --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"];         StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];"

# A testar grid... 26/07/2022 (crash StreamESD)
pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0 --trf 'Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=5 --AddCaloDigi=True --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];"'
## &&&&&&&&&&&&
# A testar grid... 27/07/2022
setupATLAS
asetup Athena,21.0.30
lsetup rucio
lsetup panda
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v3 --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];'"
# pathena --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v4 --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec=\"RAWtoESD:StreamESD.ItemList+=['TileDigitsContainer#TileDigitsCnt']; StreamESD.ItemList+=['LArDigitContainer#*']; StreamESD.ItemList+=['LArRawChannelContainer#LArRawChannels']; StreamESD.ItemList+=['TileRawChannelContainer#TileRawChannelCnt'];\""
pathena --trf "Reco_tf.py  --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\", \"LArDigitContainer#FREE\", \"LArDigitContainer#LArDigitContainer_Thinned\", \"LArRawChannelContainer#LArRawChannels\", \"TileRawChannelContainer#TileRawChannelCnt\"]' " --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v4
rucio list-dids user.mhufnage:*

## &&&&&&&&&&&&



# pathena --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root" --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0


pathena --trf "Reco_tf.py --CA --steering doRAWtoALL --maxEvents=-1 --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --preInclude egammaConfig.egammaOnlyFromRawFlags.egammaOnlyFromRaw --preExec='from AthenaConfiguration.AllConfigFlags import ConfigFlags;'" --inDS=mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775 --outDS=user.eegidiop.mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775.v0

# pathena --trf "Reco_tf.py --CA --steering doRAWtoALL --maxEvents=-1 --inputHITSFile=%IN --outputAODFile=%OUT.AOD.pool.root --preInclude egammaConfig.egammaOnlyFromRawFlags.egammaOnlyFromRaw --preExec='from AthenaConfiguration.AllConfigFlags import ConfigFlags;'" --inDS=mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775 --outDS=user.mhufnage.mc21_13p6TeV.801278.Py8EG_A14NNPDF23LO_perf_JF17.simul.HITS.e8400_e7400_s3775.v0

# pathena --trf "Reco_tf.py --AMIConfig=r9738 --AMITag=r9738 --maxEvents=-1 --AddCaloDigi=True --inputBSFile=%IN --outputESDFile=%OUT.ESD.pool.root --postExec='RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"]; StreamESD.ItemList+=[\"LArDigitContainer#*\"]; StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"]; StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];' --inDS=data17_13TeV.00329542.physics_MinBias.daq.RAW --outDS=user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW.v0"

# Reco_tf.py  \
#         --AMIConfig=r9738 \
#         --AMITag=r9738 \
#         --AddCaloDigi True \
#         --maxEvents $nEvents \
#         --inputBSFile=$file \
#         --outputESDFile=ESD_minBias_$indexOutput.pool.root \
#         --outputAODFile=AOD_minBias_$indexOutput.pool.root \
#         --postExec="RAWtoESD:StreamESD.ItemList+=[\"TileDigitsContainer#TileDigitsCnt\"];\
#         StreamESD.ItemList+=[\"LArDigitContainer#*\"];\
#         StreamESD.ItemList+=[\"LArRawChannelContainer#LArRawChannels\"];\
#         StreamESD.ItemList+=[\"TileRawChannelContainer#TileRawChannelCnt\"];\
#         "\
#         >Reco_tf_$indexOutput.log 2>&1