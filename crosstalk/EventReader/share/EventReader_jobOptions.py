###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
isMC = True
# doMC = True
dumpCells = False
# doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from EventReader.EventReaderConf import EventReaderAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

Geometry = "ATLAS-R2-2016-01-00-01"
globalflags.DetGeo.set_Value_and_Lock('atlas')
#--------------------------------------------------------
if not(isMC):
    globalflags.DataSource.set_Value_and_Lock('data')
#--------------------------------------------------------
DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion


##### Conditions and Geometry Includes
include( "CaloConditions/CaloConditions_jobOptions.py" )
include( "CaloIdCnv/CaloIdCnv_joboptions.py" )
include( "TileIdCnv/TileIdCnv_jobOptions.py" )
include( "TileConditions/TileConditions_jobOptions.py" )
include( "LArDetDescr/LArDetDescr_joboptions.py" )
    # include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py") # obsolete?? yes

from CaloTools.CaloNoiseCondAlg import CaloNoiseCondAlg
CaloNoiseCondAlg ('totalNoise')
CaloNoiseCondAlg ('electronicNoise')
CaloNoiseCondAlg ('pileupNoise')

##### Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()



##### Algorithm setup and execution

job += EventReaderAlg( "EventReader" )

job.EventReader.clusterName                 = "CaloCalTopoClusters"#"LArClusterEM7_11Nocorr"#"LArClusterEM7_11Nocorr"#"egammaClusters"#
job.EventReader.jetName                     = "AntiKt4EMPFlowJets"
job.EventReader.roiRadius                   = 0.2
job.EventReader.tileDigName                 = "TileDigitsCnt"
if isMC:        
    job.EventReader.larDigName              = "LArDigitContainer_MC"
else:       
    job.EventReader.larDigName              = 'FREE' #'LArDigitContainer_EMClust'#'LArDigitContainer_Thinned' #
job.EventReader.tileRawName                 = "TileRawChannelCnt"
job.EventReader.larRawName                  = "LArRawChannels"
# job.EventReader.doTile                      = True
job.EventReader.doClusterDump               = False # Dump only a cluster container. (override the electron cluster)
job.EventReader.noBadCells                  = True
# job.EventReader.doLAr                       = True
job.EventReader.printCellsClus              = False # Debugging
job.EventReader.printCellsJet               = False # Debugging
job.EventReader.testCode                    = False # Debugging
# Electrons 
job.EventReader.doTagAndProbe               = True  #select by tag and probe method, electron pairs (start the chain of selection: track + T&P)
job.EventReader.doElecSelectByTrackOnly     = True  #select only single electrons which pass track criteria (only track)
job.EventReader.getAssociatedTopoCluster    = True #Get the topo cluster associated to a super cluster, which was linked to an Electron
##
job.EventReader.OutputLevel                 = INFO #DEBUG # 
job.EventReader.isMC                        = isMC  # set to True in case of MC sample.

##### Condition Database Access  (based on: https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArROD/python/LArRawChannelBuilderDefault.py)
obj = "AthenaAttributeList"
db = 'LAR_ONL'
if globalflags.DataSource() == 'data':
    from IOVDbSvc.CondDB import conddb

    if conddb.GetInstance() == 'COMP200': 
        fld='/LAR/Configuration/DSPThreshold/Thresholds'
        # job.EventReader.Run1DSPThresholdsKey='LArDSPThresholds' ## NOT IMPLEMENTED FOR RUN1 DATA YET ! 
        obj='LArDSPThresholdsComplete'
    else:
        fld="/LAR/Configuration/DSPThresholdFlat/Thresholds"
        job.EventReader.Run2DSPThresholdsKey=fld
    conddb.addFolder (db, fld, className=obj)
else:   
    from AtlasGeoModel.CommonGMJobProperties import CommonGeometryFlags
    if CommonGeometryFlags.Run() == "RUN1": # back to flat threshold (NOT IMPLEMENTED YET)
        job.EventReader.useDB = False
        job.EventReader.Run2DSPThresholdsKey=''
    else:
        fld="/LAR/NoiseOfl/DSPThresholds"
        job.EventReader.Run2DSPThresholdsKey=fld
        db ='LAR_OFL'
    conddb.addFolder(db, fld, className=obj)

# if job.EventReader.isMC==False:
#     globalflags.DataSource.set_Value_and_Lock('data') 


from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD/ESD_pi0.pool.root"

# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_RDO_LarDigits/ESD_fromRDO-HITS_PI0_001783.pool.root"
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMItags/ESD_pi0.pool.root" #AMI TAG reco
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMIdumpCells/ESD_pi0.pool.root"
# inputFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_moreEvents/ESD_pi0.pool.root"

# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_002450.pool.root'
# inputFile = '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'#MC

inputDataPath   = '/afs/cern.ch/work/e/eegidiop/EventReader/offline-ringer-dev/crosstalk/data/mc21.900050.PG_single_photon_egammaET.recon.ESD.e8366_e7400_s3775_r13614_r13643/'
inputFile = [inputDataPath+'ESD_Zee_0.pool.root']
# inputDataPath   = '/eos/user/m/mhufnage/ALP_project/ESD/user.eegidiop.data17_13TeV.00329542.physics_MinBias.daq.RAW.v3_EXT0/'
# inputFile   =   [
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000001.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000002.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000003.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000004.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000005.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000006.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000007.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000008.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000009.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000010.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000028.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000054.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000200.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000201.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000202.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000203.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000204.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000205.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000206.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000207.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000208.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000209.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000210.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000211.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000212.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000213.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000214.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000215.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000217.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000218.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000219.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000220.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000221.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000222.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000223.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000224.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000225.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000226.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000227.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000228.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000229.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000230.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000231.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000232.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000233.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000234.ESD.pool.root',
#                 inputDataPath+'user.eegidiop.29913676.EXT0._000235.ESD.pool.root' 
#                 ] 
ServiceMgr.EventSelector.InputCollections = inputFile
ServiceMgr += CfgMgr.THistSvc()

# Create output file
hsvc = ServiceMgr.THistSvc
# jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
# hsvc.Output += [ "rec DATAFILE='run329542_minBias/ESD_1_dumped.root' OPT='RECREATE'" ]
hsvc.Output += [ "rec DATAFILE='minbias_getDSPThrs.root' OPT='RECREATE'" ]
theApp.EvtMax = -1

MessageSvc.defaultLimit = 9999999  # all messages

# athena.py EventReader_jobOptions.py  > reader_test_DATA.log 2>&1; code reader_test_DATA.log
